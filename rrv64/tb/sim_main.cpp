// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#define Vtop Vtestbench
#include <verilated.h>          // Defines common routines
#include <stdio.h>
#include "Vtestbench.h"               // From Verilating "top.v"
#include "cache_cal.h"

#ifdef SPIKE_DEBUG    
#include <stdlib.h>
#include <vector>
#include <string>
#include <memory>
#include<iostream>
#include <dlfcn.h>


#include "sim.h"
#include "disasm.h"


#define CSRNUM 9
#define SPIKE_OFFSET 6 // spike has a few instrs start from addr 0x0, overlook them
#define STEP 0
#define SPIKE_STEP_NUM 1000000
#endif

// If "verilator --trace" is used, include the tracing class
#if VM_TRACE
    # include <verilated_vcd_c.h>
#endif

#ifdef U_TEST
    # include"test_files.h"
#else
    # define TEST_NUM 1
    char test_files[TEST_NUM][100]={"../test_program/benchmarks/dhrystone.bin"};
#endif
Vtop *top;                      // Instantiation of module

vluint64_t main_time = 0;       // Current simulation time
// This is a 64-bit integer to reduce wrap over issues and
// allow modulus.  This is in units of the timeprecision
// used in Verilog (or from --timescale-override)

enum passfail { BLANK, PASSED, FAILED_TIMEOUT, FAILED_FUNC};

passfail judge_pass(unsigned long pc);
passfail run(   
                Vtop *top, 
                VerilatedVcdC** tfp, 
                long local_time
#ifdef SPIKE_DEBUG                
                , 
                std::vector<state_t> reg_record, 
                std::vector<insn_t>  inst_record
#endif
);
passfail loop_run(int loop);

#ifdef SPIKE_DEBUG  
int spikeaaa(int argc, char** argv, int step_num,
		std::vector<state_t> &reg_record, std::vector<insn_t> &inst_record);
int cosim_spike(std::vector<state_t> &reg_record, std::vector<insn_t>  &inst_record, int step_num_sum);
int cosim_compare(std::vector<state_t> &reg_record, std::vector<insn_t>  &inst_record, Vtop *top, int &step_num);
#endif

double sc_time_stamp ()         // Called by $time in Verilog
{
    return main_time;           // converts to double, to match
                                // what SystemC does
}

int main(int argc, char** argv) 
{
    int ret = 0;
    Verilated::commandArgs(argc, argv);   // Remember args
    int loop;// = 29;
    passfail judge[TEST_NUM] = {BLANK};
    for(loop = 0; loop < TEST_NUM; loop++)
    {
        printf("%3d: ", loop);
        judge[loop] = loop_run(loop); 

        if(judge[loop] == BLANK)
        {
            printf("Unknown error, exiting...\n");
            break;
        }
        if (judge[loop] == FAILED_FUNC)
        {
            printf("Functional error, exiting...\n");
            break;
        }
    }

#ifdef U_TEST
    FILE *fp;
    if((fp = fopen("../logs/utestlog", "wb+")) == NULL)
    {
        printf("log file open failed\n");
    }
    printf("\nSummary, failed entries, also in logs/utestlog:\n");
    fprintf(fp, "Summary, failed entries, also in logs/utestlog:\n");
    printf("\nFailed Timeout:\n");
    fprintf(fp, "Failed Timeout:\n");
    for(loop = 0; loop < TEST_NUM; loop++)
    {
        if (judge[loop] == FAILED_TIMEOUT)
        {
            printf("%3d:  %s\n", loop, test_files[loop]);
            fprintf(fp, "%3d:  %s\n", loop, test_files[loop]);
            ret = 1;
        }
    }
    printf("\nFailed Functional:\n");
    fprintf(fp, "\nfailed Functional:\n");
    for(loop = 0; loop < TEST_NUM; loop++)
    {
        if (judge[loop] == FAILED_FUNC)
        {
            printf("%3d:  %s\n", loop, test_files[loop]);
            fprintf(fp, "%3d:  %s\n", loop, test_files[loop]);
            ret = 1;
        }
    }
    printf("\nFailed Others:\n");
    fprintf(fp, "\nFailed Others:\n");
    for(loop = 0; loop < TEST_NUM; loop++)
    {
        if (judge[loop] == BLANK)
        {
            printf("%3d:  %s\n", loop, test_files[loop]);
            fprintf(fp, "%3d:  %s\n", loop, test_files[loop]);
            ret = 1;
        }
    }
    fclose(fp);
#endif
    return ret;
}

passfail run(   
                Vtop *top, 
                VerilatedVcdC** tfp, 
                long local_time
#ifdef SPIKE_DEBUG 
                ,                
                std::vector<state_t> reg_record, 
                std::vector<insn_t>  inst_record
#endif
)
{
#ifdef SPIKE_DEBUG 
    int step_num = SPIKE_OFFSET;
#endif

    passfail judge = BLANK;
    top->rst = 1;           // Set some inputs
    top->clk = 0;

    cache_calculate* cache_cal = new cache_calculate;

    while (!Verilated::gotFinish() && (judge == BLANK)) 
    {
        if (main_time > 10) 
        {
            top->rst = 0;    // Deassert reset
        }

        top->clk = !top->clk;  // Toggle clock
#ifdef U_TEST
        judge = judge_pass(top->if_pc);
        if(judge == BLANK) // continue
        {
        }
        else if(judge == PASSED) //pass
        {
            printf("Passed   ");
            break;
        }
        else if(judge == FAILED_FUNC)// failed
        {
            printf("Failed, Functional error   ");
            break;
        }
        if(local_time > 10000)
        {
            printf("Failed, Timeout error   ");
            judge = FAILED_TIMEOUT;
            break;
        }
#endif            

        top->eval();            // Evaluate model, Verilator firstly looks for changes in clock signals, then evaluates related sequential always blocks

        main_time++;            // Time passes
        local_time++;

#ifdef SPIKE_DEBUG
    if(cosim_compare(reg_record, inst_record, top, step_num) == 1)
    {
        judge = FAILED_FUNC;
    }
#endif

    // if(cache_cal->run(top->rd_en, top->wr_en, top->debug_o_wb_pc) )
    // {
    //     // cache_cal->report();
    //     // getchar();
    // }

#if VM_TRACE
        // Dump trace data for this cycle
        for(int loop_m = 0; loop_m < 1; loop_m++)
        {
            if (tfp[loop_m]) tfp[loop_m]->dump(main_time);
        }
#endif
    }
printf("\nhere!\n");
    // cache_cal->report();
    delete cache_cal;
    return judge;
}

passfail judge_pass(unsigned long pc)
{
    if(pc == 0x80000058)
        return PASSED; //pass
    else if (pc == 0x80000044)
        return FAILED_FUNC; //fail
    else
        return BLANK;
}

passfail loop_run(int loop)
{
    top = new Vtop;             // Create instance
    long local_time = 0;
    passfail judge = BLANK;

#ifdef SPIKE_DEBUG 
    std::vector<state_t> reg_record;
    std::vector<insn_t>  inst_record;
    int step_num_sum = SPIKE_STEP_NUM;
#endif

#if VM_TRACE
    // If verilator was invoked with --trace argument,
    // and if at run time passed the +trace argument, turn on tracing
    VerilatedVcdC** tfp = new VerilatedVcdC*[1];
    int loop_i;
    for(loop_i=0;loop_i<1;loop_i++)
    {
        tfp[loop_i] = new VerilatedVcdC;
        top->trace(tfp[loop_i], 99);  // Trace 99 levels of hierarchy
    }
    //VerilatedVcdC* tfp = NULL;
    const char* flag = Verilated::commandArgsPlusMatch("trace");
    if (flag && 0==strcmp(flag, "+trace")) {
        Verilated::traceEverOn(true);  // Verilator must compute traced signals
#ifndef U_TEST
        VL_PRINTF("Enabling waves into logs/vlt_dump.vcd...\n");
#endif
       // tfp = new VerilatedVcdC;
        //top->trace(tfp, 99);  // Trace 99 levels of hierarchy
        Verilated::mkdir("../logs");
        for(loop_i=0;loop_i<1;loop_i++)
        {
            char a[40];
            sprintf(a,"%s%d%s","../logs/vlt_dump", loop_i, ".vcd");

            tfp[loop_i]->open(a);  // Open the dump file
        }
    }
#endif

    
#ifdef U_TEST
    char str1[100] = "../test_program/isa_2/isa_verilator/";
    strcat(str1,test_files[loop]);
#else
    char str1[100] = "../test_program/benchmarks/dhrystone.bin";
    //strcpy(str1,test_files[loop]);
#endif
    printf("%s\n",str1);
    top->image_f = str1;

#ifdef SPIKE_DEBUG
    cosim_spike(reg_record, inst_record, step_num_sum);
#endif

    judge = run(top, tfp, local_time
#ifdef SPIKE_DEBUG
    , reg_record , inst_record
#endif
    );  
    printf(" %s\n",test_files[loop]);

    top->final();               // Done simulating

    delete top;
#if VM_TRACE
    // Close trace if opened
    for(int count = 0; count < 1; count++)
    {
        if (tfp[count]) { tfp[count]->close(); tfp[count] = NULL; }
    }
#endif
    return judge;
}

#ifdef SPIKE_DEBUG  
int cosim_spike(std::vector<state_t> &reg_record, std::vector<insn_t>  &inst_record, int step_num_sum)
{
    int ret = 0;
    char** command = new char*[4];
    char str0[] = "./spike";
    char str1[] = "--isa=RV64IMAFDC";
    char str2[] = "-d";
    // char str3[] = "pk";
    char str4[] = "../test_program/benchmarks/dhrystone.riscv";

    command[0] = str0;
    command[1] = str1;
    command[2] = str2;
    // command[3] = str3;
    command[3] = str4;
    int command_num = 4;


#ifdef SPIKE_DEBUG
	ret = spikeaaa(command_num, command, step_num_sum, reg_record, inst_record);

	std::cout<<"spike finished!"<<std::endl;
    getchar();
#endif
    return ret;
}

int cosim_compare(std::vector<state_t> &reg_record, std::vector<insn_t>  &inst_record, Vtop *top, int &step_num)
{
    static long pc_last   = 0;
    int err = 0;
    int processor_start = 0;

    char csrNames[CSRNUM][50]=
    {
		"misa",
        "mhartid",
        "mstatus",
        "mtvec",
        "mscratch",
        "mepc",
        "mcause",
        "mtval",
        "mcycle"
    };
    long unsigned int csrValues[CSRNUM];
    for (int i = 0; i < CSRNUM; i++)
    {
        csrValues[i] = top->debug_o_wb_csr[i];
    }

    disassembler_t* disassembler = new disassembler_t(32);

  	if( (pc_last != top->debug_o_wb_pc) && top->rst == 0 && pc_last != 0)
    {
        int r;
        unsigned long temp_gpr = 0;
        
        
        fprintf(stderr, "\n\n%-4s: 0x%08x\n", "pc", top->debug_o_wb_pc);//pc from processor


        //processor

        // fprintf(stderr, "processor: 0x%016" PRIx64 " (0x%016" PRIx64 ") %s\n",
        //          pc_last, inst_last, disassembler->disassemble(inst_last).c_str());
        fprintf(stderr, "processor: 0x%016" PRIx64 "\n", top->debug_o_wb_pc);

        for ( r = 0; r < NXPR; ++r)
        {
            temp_gpr = (long)(top->debug_o_wb_gpr[2*r]) + ((long)(top->debug_o_wb_gpr[2*r+1]) << 32);
            fprintf(stderr, "%-4s: 0x%016" PRIx64 "  ", xpr_name[r], temp_gpr);
            if ((r + 1) % 4 == 0)
            {
                fprintf(stderr, "\n");
            }
        }


        for ( r = 0; r < CSRNUM; ++r)
        {
            fprintf(stderr, "%-4s: 0x%016" PRIx64 "  ", csrNames[r], csrValues[r]);
            if ((r + 1) % 4 == 0)
            {
                fprintf(stderr, "\n");
            }
        }
        fprintf(stderr, "\n");
        // printf("privilege level %d (0=user, 1=superviser, 2=hyperviser, 3=machine)\n", top->priv_lvl_ver);


        // spike simulator
        // disasm from spike
        uint64_t bits = inst_record[step_num-1].bits() & ((1ULL << (8 * insn_length(inst_record[step_num-1].bits()))) - 1);
        fprintf(stderr, "\nsimulator: 0x%016" PRIx64 " (0x%016" PRIx64 ") %s\n",
                    /* pc_last*/(reg_record.begin()+step_num-1)->pc, bits, disassembler->disassemble(inst_record[step_num-1]).c_str());


        for ( r = 0; r < NXPR; ++r)
        {
            // regfile_t.h:   template <class T, size_t N, bool zero_reg> class regfile_t
            fprintf(stderr, "%-4s: 0x%016" PRIx64 "  ", xpr_name[r], (reg_record.begin()+step_num)->XPR[r] );
            if ((r + 1) % 4 == 0)
            {
                fprintf(stderr, "\n");
            }
        }

        // if( top->debug_o_wb_pc != (reg_record.begin()+step_num-1)->pc)  // only compare pc
        // {
        if(top->debug_o_wb_pc > 0x8000003c) // reg reset finished
        {
            for ( r = 0; r < NXPR; ++r)
            {
                // processor inconsistent against spike sim
                temp_gpr = (long)(top->debug_o_wb_gpr[2*r]) + ((long)(top->debug_o_wb_gpr[2*r+1]) << 32);
                if(unsigned(temp_gpr) != unsigned((reg_record.begin()+step_num)->XPR[r])
                )
                {
                    fprintf(stderr, "inconsistency found, reg:%-4s\nprocessor:0x%016llx\nspike    :0x%016llx\n",
                            xpr_name[r], temp_gpr, unsigned((reg_record.begin()+step_num)->XPR[r]) );
                    //err = 1;
                    getchar();
                }
                if(unsigned(top->debug_o_wb_pc) != unsigned((reg_record.begin()+step_num-1)->pc))  // add comparing pc
                {
                    fprintf(stderr, "inconsistency found, reg:pc\nprocessor:0x%016llx\nspike    :0x%016llx\n",
                            unsigned(top->debug_o_wb_pc), unsigned((reg_record.begin()+step_num-1)->pc) );
                    //err = 1;
                    getchar();
                }
            }
            printf("step %d\n", step_num);
        }
        // }

        pc_last = top->debug_o_wb_pc;
        step_num++;
        if(step_num >= SPIKE_STEP_NUM) // spike sim max
        {
            printf("spike sim ended at %d instr, exit...\n", step_num);
            err = 1;
        }
    }

    if(pc_last == 0)
    {
        pc_last = top->debug_o_wb_pc;
    }

    delete disassembler;
#if STEP
    getchar();
#endif
    return err;
}

#endif
