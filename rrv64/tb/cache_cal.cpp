// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#include "cache_cal.h"
#include <stdio.h>

cache_calculate::cache_calculate()
{
    cycle_count  = 0;
    pc_last      = 0;

    rd_en_last = false;
    wr_en_last = false;

    read_access  = 0;
    write_access = 0;
    read_miss    = 0;
    write_miss   = 0;
}

cache_calculate::~cache_calculate()
{}

int cache_calculate::read_access_add(int inc)
{
    this->read_access += inc;
    return this->read_access;
}

int cache_calculate::write_access_add(int inc)
{
    this->write_access += inc;
    return this->write_access;
}

int cache_calculate::read_miss_add(int inc)
{
    this->read_miss += inc;
    return this->read_miss;
}

int cache_calculate::write_miss_add(int inc)
{
    this->write_miss += inc;
    return this->write_miss;
}

int cache_calculate::cycle_count_add(int inc)
{
    this->cycle_count += inc;
    return this->cycle_count;
}

int cache_calculate::cycle_count_set(int cycle_count_new)
{
    this->cycle_count = cycle_count_new;
    return this->cycle_count;
}

unsigned long cache_calculate::pc_last_update(unsigned long pc_new)
{
    this->pc_last = pc_new;
    return this->pc_last;
}

bool cache_calculate::pc_compare(unsigned long pc_new)
{
    return (this->pc_last == pc_new);
}

bool cache_calculate::rd_en_last_read()
{
    return this->rd_en_last;
}
bool cache_calculate::rd_en_last_set(bool rd_en_new)
{
    this->rd_en_last = rd_en_new;
    return this->rd_en_last;
}
bool cache_calculate::wr_en_last_read()
{
    return this->wr_en_last;
}
bool cache_calculate::wr_en_last_set(bool wr_en_new)
{
    this->wr_en_last = wr_en_new;
    return this->wr_en_last;
}

int cache_calculate::run(bool rd_en_new, bool wr_en_new, unsigned long pc_new)
{
    int ret = 0;
    // printf("%d, %d, 0x%lx\n", rd_en_new, wr_en_new, pc_new);
    // if(rd_en_new || wr_en_new) // memory read/write
    // {
    printf("rd_en_new %d, wr_en_new %d, pc_new 0x%lx, pc_old 0x%lx, cycle_count %d\n", 
    this->rd_en_last_read(), this->wr_en_last_read(), pc_new, this->pc_last, this->cycle_count_read());

    if(this->pc_compare(pc_new))  // the same inst
    {
        this->cycle_count_add(1);
        ret = 0;
    }
    else                          // new inst
    {
        // last_inst
        ret = 1;
        if(this->rd_en_last_read())      // last ma inst is rd
        {
            if(this->cycle_count_read() > 0)
            {
                this->read_access_add(1);
            }
            if(this->cycle_count_read() > 3)       // last rd inst miss
            {
                this->read_miss_add(1);
            }
            this->rd_en_last_set(false);
        }
        else if(this->wr_en_last_read()) // last ma inst is wr
        {
            if(this->cycle_count_read() > 0)
            {
                this->write_access_add(1);
            }
            if(this->cycle_count_read() > 3)       // last wr inst miss
            {
                this->write_miss_add(1);
            }
            this->wr_en_last_set(false);
        }
        else
        {
            ret = 0;
        }
        
        // new_inst
        this->pc_last_update(pc_new);
        this->cycle_count_set(0); // recount cycle for new ma inst
        // printf("cycle_count set to 0 %d\n", this->cycle_count_read());
        // getchar();
        this->rd_en_last_set(false);
        this->wr_en_last_set(false);
        if(rd_en_new)
        {
            this->rd_en_last_set(true);
        }
        else if(wr_en_new)
        {
            this->wr_en_last_set(true);
        }

        this->report();
        getchar();
        
    }
    // }
    return ret;
}

void cache_calculate::report()
{
    printf("read  access count : %d\n", this->read_access_add(0));
    printf("read  miss count   : %d\n", this->read_miss_add(0));
    printf("read  miss rate    : %f%\n\n", (float)this->read_miss_add(0)/(float)this->read_access_add(0)*100.0);
    printf("write access count : %d\n", this->write_access_add(0));
    printf("write miss count   : %d\n", this->write_miss_add(0));
    printf("write miss rate    : %f%\n\n", (float)this->write_miss_add(0)/(float)this->write_access_add(0)*100.0);
    printf("total access count : %d\n", this->read_access_add(0) + this->write_access_add(0));
    printf("total miss count   : %d\n", this->read_miss_add(0) + this->write_miss_add(0));
    printf("total miss rate    : %f%\n\n", (float)(this->read_miss_add(0) + this->write_miss_add(0))/(float)(this->read_access_add(0) + this->write_access_add(0))*100.0);    
}

int cache_calculate::cycle_count_read()
{
    return this->cycle_count_add(0);
}
