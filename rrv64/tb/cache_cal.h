// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#ifndef _SIM_MAIN_H_
#define _SIM_MAIN_H_

class cache_calculate
{
    public:
        cache_calculate();
        ~cache_calculate();
        int run(bool rd_en_new, bool wr_en_new, unsigned long pc_new);
        void report();

        int cycle_count_read();
        

    private:
        int read_access_add(int inc);
        int write_access_add(int inc);
        int read_miss_add(int inc);
        int write_miss_add(int inc);
        
        int cycle_count_add(int inc);
        int cycle_count_set(int cycle_count_new);

        unsigned long pc_last_update(unsigned long pc_new);
        bool pc_compare(unsigned long pc_new);

        bool rd_en_last_read();
        bool rd_en_last_set(bool rd_en_new);
        bool wr_en_last_read();
        bool wr_en_last_set(bool wr_en_new);

        int cycle_count;
        unsigned long pc_last;

        bool rd_en_last;
        bool wr_en_last;

        int read_access;
        int write_access;
        int read_miss;
        int write_miss;
};

#endif
