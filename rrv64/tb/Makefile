# Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

RTL_ROOT          = ../rtl
RTL_CORE64_PATH   = $(RTL_ROOT)/rrv64
RTL_LIB_PATH      = $(RTL_ROOT)/lib
RTL_COMMON_PATH   = $(RTL_ROOT)/common

TB_ROOT           = ../tb
TB_RRV64          = $(TB_ROOT)/rrv64
TB_MEM_MODEL      = $(TB_ROOT)/perfect_mem_model

TOP_NAME          = Vtestbench

# vpath %.vh $(RTL_ROOT)/common

SIM_FILES         = $(wildcard $(TB_RRV64)/*.sv)  $(wildcard $(TB_MEM_MODEL)/*.sv) 
LIB_FILES         = $(RTL_COMMON_PATH)/soc_cfg.sv \
                    $(RTL_COMMON_PATH)/soc_func.sv \
                    $(RTL_COMMON_PATH)/soc_typedef.sv \
                    $(RTL_CORE64_PATH)/rrv64_param_pkg.sv \
                    $(RTL_CORE64_PATH)/rrv64_typedef_pkg.sv \
                    $(RTL_CORE64_PATH)/rrv64_func_pkg.sv \
                    $(RTL_CORE64_PATH)/rrv64_decode_func_pkg.sv \
                    $(RTL_CORE64_PATH)/rrv_cfg.sv \
                    $(RTL_CORE64_PATH)/rrv_typedef.sv \
                    $(RTL_COMMON_PATH)/soc_intf_typedef.sv \
                    \
                    $(RTL_LIB_PATH)/xilinx_sram.sv \
                    $(RTL_LIB_PATH)/sd_hold_valid.sv \
                    $(wildcard $(RTL_LIB_PATH)/clock/*.sv) \
                    $(wildcard $(RTL_LIB_PATH)/modules/*.sv) 
                    


# $(wildcard $(RTL_LIB_PATH)/*.sv)  $(wildcard $(RTL_LIB_PATH)/clock/*.sv)  $(wildcard $(RTL_LIB_PATH)/op_ip/misc/*.v)  $(wildcard $(RTL_LIB_PATH)/modules/*.sv)
RTL_FILES         = $(wildcard $(RTL_CORE32_PATH)/*.sv) \
                    \
                    $(RTL_CORE64_PATH)/rrv64.sv \
                    $(RTL_CORE64_PATH)/rrv64_alu.sv \
                    $(RTL_CORE64_PATH)/rrv64_bpu.sv \
                    $(RTL_CORE64_PATH)/rrv64_cache_noc.sv \
                    $(RTL_CORE64_PATH)/rrv64_clk_gating.sv \
                    $(RTL_CORE64_PATH)/rrv64_csr.sv \
                    $(RTL_CORE64_PATH)/rrv64_dcache_bypass.sv \
                    $(RTL_CORE64_PATH)/rrv64_dcache_ram.sv \
										$(RTL_CORE64_PATH)/rrv64_dcache_mesi.sv \
										$(RTL_CORE64_PATH)/rrv64_dcache.sv \
                    $(RTL_CORE64_PATH)/rrv64_decode.sv \
                    $(RTL_CORE64_PATH)/rrv64_edeleg_checker.sv \
                    $(RTL_CORE64_PATH)/rrv64_execute.sv \
                    $(RTL_CORE64_PATH)/rrv64_fetch.sv \
                    $(RTL_CORE64_PATH)/rrv64_icache.sv \
                    $(RTL_CORE64_PATH)/rrv64_icache_sysbus.sv \
                    $(RTL_CORE64_PATH)/rrv64_icache_top.sv \
                    $(RTL_CORE64_PATH)/rrv64_ideleg_checker.sv \
                    $(RTL_CORE64_PATH)/rrv64_inst_buffer.sv \
                    $(RTL_CORE64_PATH)/rrv64_inst_trace_buffer.sv \
                    $(RTL_CORE64_PATH)/rrv64_int_regfile.sv \
                    $(RTL_CORE64_PATH)/rrv64_mem_access.sv \
                    $(RTL_CORE64_PATH)/rrv64_mem_breakpoint.sv \
                    $(RTL_CORE64_PATH)/rrv64_napot_addr.sv \
                    $(RTL_CORE64_PATH)/rrv64_ring_if_arbiter.sv \
                    $(RTL_CORE64_PATH)/rrv64_pc_breakpoint.sv \
                    $(RTL_CORE64_PATH)/rrv64_perm_checker.sv \
                    $(RTL_CORE64_PATH)/rrv64_pmp_checker.sv \
                    $(RTL_CORE64_PATH)/rrv64_pmp_match.sv \
                    $(RTL_CORE64_PATH)/rrv64_ptw.sv \
                    $(RTL_CORE64_PATH)/rrv64_ptw_core.sv \
                    $(RTL_CORE64_PATH)/rrv64_regfile.sv \
                    $(RTL_CORE64_PATH)/rrv64_rvc_inst_build.sv \
                    $(RTL_CORE64_PATH)/rrv64_stall.sv \
			$(RTL_CORE64_PATH)/rrv64_div.sv \
			$(RTL_CORE64_PATH)/rrv64_mul.sv \
			$(RTL_CORE64_PATH)/rrv64_divi.sv \
			$(RTL_CORE64_PATH)/rrv64_mult_slow.sv \
			$(RTL_CORE64_PATH)/rrv64_mult_fast.sv \
                    $(RTL_CORE64_PATH)/rrv64_tlb.sv 

VERILATOR_FILES =   testbench.sv $(LIB_FILES)  \
                    $(SIM_FILES) $(RTL_FILES) --exe $(TB_ROOT)/sim_main.cpp $(TB_ROOT)/cache_cal.cpp

ifeq ($(JOBS),)
  JOBS := $(shell grep -c ^processor /proc/cpuinfo 2>/dev/null)
  ifeq ($(JOBS),)
    JOBS := 1
  endif
endif

.PHONY: default, vcs, vcs_run_test, vcs_run, vcs_run_gui, vcs_run_test_gui, clean
default:

	-mkdir obj_dir

	verilator -I$(RTL_COMMON_PATH) -I"$(LIB_FILES)" -I$(TB_MEM_MODEL)  -I$(TB_RRV64)  -Wno-fatal  --cc    $(VERILATOR_FILES) -LDFLAGS "-ldl -lpthread" --top-module "testbench"
	
	make -j$(JOBS) -C obj_dir -f $(TOP_NAME).mk $(TOP_NAME)

	make -j$(JOBS) -C ./test_program/benchmarks 

	cd obj_dir/ && ./$(TOP_NAME) +trace 

vcs:
	-mkdir obj_dir
	make -j$(JOBS) -C ./test_program/benchmarks
	make -j$(JOBS) -C ./test_program/isa XLEN=64
	vcs -sverilog testbench.sv $(LIB_FILES) $(SIM_FILES) $(RTL_FILES) +incdir+../rtl/common +incdir+/opt/cad/synopsys/installs/syn/Q-2019.12-SP5-1/dw/sim_ver -full64 -debug_access+all
	chmod -R 775 obj_dir

TEST_CASES=$(wildcard ./test_program/isa/rv64u*-p-*.bin)
vcs_run_test:
	-rm -rf ./sim_test_report.txt
	touch ./sim_test_report.txt
	$(foreach case, $(TEST_CASES), echo "$(case)" >> sim_test_report.txt; ./simv +backdoor_load_image=$(case) +report_file=./sim_test_report.txt +case_name=$(case);)

vcs_run_test_gui:
	-rm -rf ./sim_test_report.txt
	touch ./sim_test_report.txt
	./simv -gui  +backdoor_load_image=./test_program/isa/rv64ui-p-and.bin +report_file=./sim_test_report.txt +case_name=rv64ui-p-and

BENCHMARK_CASES=$(wildcard /home/ywei/rrv64/tb/test_program/embench-iot/bd/src/*/*.bin)
vcs_run_benchmark:
	-rm -rf ./sim_test_report.txt
	touch ./sim_test_report.txt
	$(foreach case, $(BENCHMARK_CASES), echo "$(case)" >> sim_test_report.txt; ./simv +backdoor_load_image=$(case) +report_file=./sim_test_report.txt +case_name=$(case);)


vcs_run:
	./simv +backdoor_load_image=./test_program/benchmarks/dhrystone.bin

vcs_run_gui:
	./simv -gui +backdoor_load_image=./test_program/benchmarks/dhrystone.bin

vcs_cosim:
	make -j$(JOBS) -C ./cosim
	vcs -sverilog testbench.sv $(LIB_FILES) $(SIM_FILES) $(RTL_FILES) ./cosim/main.so +incdir+../rtl/common +incdir+/opt/cad/synopsys/installs/syn/Q-2019.12-SP5-1/dw/sim_ver -full64 -debug_access+all +define+COSIM
	./simv +backdoor_load_image=./test_program/benchmarks/_dhrystone.bin +define+COSIM

verilator_run_test:
	make -j$(JOBS) -C ./test_program/isa_2/isa_verilator XLEN=64
	-mkdir obj_dir
	verilator --trace -DU_TEST -I$(RTL_COMMON_PATH) -I"$(LIB_FILES)" -I$(TB_MEM_MODEL)  -I$(TB_ORV64) -I$(DW_LIB_PATH) -Wno-fatal  --cc    $(VERILATOR_FILES) -CFLAGS "-DU_TEST" -LDFLAGS "-ldl -lpthread" --top-module "testbench"
	make -j$(JOBS) -C obj_dir -f $(TOP_NAME).mk $(TOP_NAME)


verilator_cosim:
	-mkdir obj_dir
	make -j$(JOBS) -C ./cosim
	verilator -I$(RTL_COMMON_PATH) -I"$(LIB_FILES)" -I$(TB_MEM_MODEL)  -I$(TB_RRV64)  -Wno-fatal  --cc    $(VERILATOR_FILES) ../cosim/main.so -LDFLAGS "-ldl -lpthread" --top-module "testbench" -DCOSIM -CFLAGS "-DVERILATOR_COSIM"
	make -j$(JOBS) -C obj_dir -f $(TOP_NAME).mk $(TOP_NAME)
	cd obj_dir/ && ./$(TOP_NAME) +trace 

verilator:
	-mkdir obj_dir
	verilator --trace --unroll-count 256  -I$(RTL_COMMON_PATH) -I"$(LIB_FILES)" -I$(TB_MEM_MODEL)  -I$(TB_RRV64) -Wno-fatal  --cc    $(VERILATOR_FILES) -CFLAGS "-I$(TB_ROOT)/cache_cal.h"  -LDFLAGS "-ldl -lpthread" --top-module "testbench"
	make -j$(JOBS) -C obj_dir -f $(TOP_NAME).mk $(TOP_NAME)

verilator_run: verilator
	-mkdir logs
	make -j$(JOBS) -C ./test_program/benchmarks
	cd obj_dir/ && ./$(TOP_NAME)  +trace 

clean:
	-rm -rf ./*.vpd
	-rm -r ./obj_dir
	-rm -rf ./csrc ./simv.daidir ./simv
	-rm -rf ./logs
	-make -C ./test_program/benchmarks clean
	-make -C ./test_program/isa clean
	-make -C ./cosim clean
