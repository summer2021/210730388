// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

`ifndef VERILATOR
	`timescale   1ns/1ps
`endif
`define SPIKE_DEBUG
module testbench
(
`ifndef VCS
    input  logic             clk,
    input  logic             rst,
	input  string            image_f,
	output logic[39-1:0]     if_pc,

	output logic 			 rd_en,
	output logic   			 wr_en,
	output logic [39-1:0]  	 debug_o_wb_pc
`endif

`ifdef SPIKE_DEBUG
	,
	output logic [32-1:0][64-1:0] 	debug_o_wb_gpr,
	output logic [64-1:0]			debug_o_wb_csr [0:9-1]
`endif
);

`ifdef SPIKE_DEBUG
	assign debug_o_wb_gpr[32-1:1] = top0.dut.rrv64_u.IRF.RF.regfile[32-1:1];
	assign debug_o_wb_gpr[0]      = '0;
	assign debug_o_wb_csr[0] = { 36'b0 ,top0.dut.rrv64_u.CS.misa };
	assign debug_o_wb_csr[1] = { 56'b0, top0.dut.rrv64_u.CS.mhartid };
	assign debug_o_wb_csr[2] = top0.dut.rrv64_u.CS.mstatus;
	assign debug_o_wb_csr[3] = top0.dut.rrv64_u.CS.mtvec;
	assign debug_o_wb_csr[4] = top0.dut.rrv64_u.CS.mscratch;
	assign debug_o_wb_csr[5] = { 25'b0, top0.dut.rrv64_u.CS.mepc };
	assign debug_o_wb_csr[6] = top0.dut.rrv64_u.CS.mcause;
	assign debug_o_wb_csr[7] = top0.dut.rrv64_u.CS.mtval;
	assign debug_o_wb_csr[8] = top0.dut.rrv64_u.CS.mcycle;
`endif


`ifdef VCS

logic clk;
logic rst;


initial begin
	// $dumpfile("rrv64.vcd");
	// $dumpvars(0, top0);
	$vcdpluson();
end

initial begin
	clk = 1'b0;
	// $dumpon;
	forever #5 clk = ~clk;
	// $dumpoff;
end

initial begin
	rst = 1'b1;
	#20;
	rst = 1'b0;
end
`endif

`ifndef VCS
initial begin
	top0.image_f = image_f;
	$display("%t Get image_f %s", $time, image_f);
end

assign debug_o_wb_pc  = top0.dut.rrv64_u.MA.wb_pc;
assign if_pc = top0.dut.rrv64_u.IF.if_pc;
assign rd_en = top0.dut.rrv64_u.DC.l1_common_rd_en;
assign wr_en = top0.dut.rrv64_u.DC.l1_common_wt_en;
`endif

top top0
(
  .tb_clk                   (clk),
  .tb_rst                   (rst)
);

endmodule
