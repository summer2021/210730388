# RRV64

## Overview

RRV64 is a 64-bit RISC-V Core designed for embedded applications. 
It has a 5 stage in-order pipeline and multi-level cache system including 
L1 I/Dcache and L2 I/D cache. RRV64 supports RV64IMAC instruction sets, 
Sv39 Virtual Address format, legal combinations of privilege modes and 
Physcial Memory Protection (PMP). It is capable of running a 
full-featured operating system like Linux. The core is compatible with 
all applicable RISC‑V standards.

## Documentation
The RRV64 user manual can be accessed online at [ReadTheDocs](https://picorio-doc.readthedocs.io/en/latest/index.html). It is also contained in the [doc](https://gitlab.com/picorio/picorio-doc) folder of this repository.

Table of Contents
=================
* [RRV64 RISC-V CPU Overview](#overview)
* [Documentation](#documentation)
* [Table of Contents](#table-of-contents)
    * [Prerequisites](#prerequisites)
    * [Compile & Run simulation](#compile-&-run-simulation)
        * [With VCS](#with-vcs)
        * [With Verilator](#with-verilator)
    * [Build & Run Benchmarks](#build-&-run-benchmark)
        * [Build Dhrystone](#build-dhrystone)
        * [Build Embench-iot](#build-embench-iot)
        * [Build Coremark](#build-coremark)
    * [CI Testsuites](#ci-testsuites)
* [Contributing](#contributing)

## Prerequisites
### 1. [Verilator: SystemVerilog Translator and simulator](https://www.veripool.org/projects/verilator/wiki/Installing)

   The project is developed under the Verilator with version 4.100, git commit SHA `0a9ae154`.
	
### 2. [Gtkwave: Wave viewer](http://gtkwave.sourceforge.net/)

### 3. [RISC-V GNU Compiler Toolchain](https://github.com/riscv/riscv-gnu-toolchain).
   
 * Choose Newlib for installation.
 * The configuration can be: ```./configure --prefix=/opt/riscv --with-arch=rv64ima --with-abi=lp64 --with-cmodel=medany```

4.Set RRV64 PATH ```RRV64```

 * If you place the project at, say, ```/opt/RRV64```

        $ vim ~/.bashrc
      append ```export RRV64=/opt/RRV64``` into .bashrc, then save & exit
        $ source ~/.bashrc


## Compile & Run simulation
### With VCS
To compile RRV64 with VCS:

    $ cd rrv64/tb
    $ make vcs 

And then it will be compiled by `VCS`, to run the simulation:

    $ make vcs_run

The default program to be run is Dhrystone

### With Verilator

[Verilator](https://github.com/verilator/verilator) is an open-source simulator, it provides verilog/systemverilog compilation function similar to VCS.

To compile RRV64 with Verilator:

    $ cd rrv64/tb
    $ make verilator

And then it will be compiled by `Verilator`, to run the simulation:

    $ make verilator_run

The `+trace` flag will make the simulator product a waveform file in `rrv64/tb/logs`, suffixed with `.vcd`, and you can view it by an open-source waveform viewer [Gtkwave](http://gtkwave.sourceforge.net/).

## Build & Run Benchmarks

### Build Dhrystone
Dhrystone the program that runs by default. You can build and execute it as described in the [Compile & Run simulation](#compile-&-run-simulation) section with both [VCS](#with-vcs) and [Verilator](#with-verilator).
### Build Embench-iot
To build embench-iot:

    $ cd rrv64/tb/test_program/embench-iot
    $ ./build_all.py --arch=riscv64 --chip=rrv64 --board=picorio compile

And then you will find the bin file in rrv64/tb/test_program/embench-iot/bd/

To run embench-iot with VCS:
    
    $ cd rrv64/tb
    $ make vcs
    $ ./simv +backdoor_load_image=/path/to/the/bin/file

### Build Coremark
To build Coremark:
    
    $ cd rrv64/tb/test_program/coremark
    $ make PORT_DIR=simple

And then you will find the bin file in rrv64/tb/test_program/coremark

To run embench-iot with VCS:
    
    $ cd rrv64/tb
    $ make vcs
    $ ./simv +backdoor_load_image=/path/to/the/bin/file

## CI Testsuites
We provide GitLab continuous integration configuration file that runs the RISC-V unit-tests and the RISCV benchmarks. 
Once everything is set up and installed, you can run the tests suites as follows: 
* With Verilator:
        
        $ make -C ./tb verilator_run_test
        $ make -C ./tb verilator_run
* With VCS

        $ make -C ./tb vcs
        $ make -C ./tb vcs_run_test
# Contributing
Check out the [contribution guide](CONTRIBUTING.md).

