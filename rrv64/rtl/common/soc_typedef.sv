// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

 /*************************************************************************
 *  - Typedef for SoC
 **************************************************************************/

`ifndef __SOC_TYPEDEF__SV__
`define __SOC_TYPEDEF__SV__

package soc_typedef;
  import soc_cfg::*;

  `define ZERO(n) {(n){1'b0}}
  `define ONE(n) {(n){1'b1}}
  `define ZERO_BITS(t) {($bits(t)){1'b0}}
  `define ONE_BITS(t) {($bits(t)){1'b1}}

  typedef logic [VIR_ADDR_WIDTH-1:0]      vaddr_t; // virtual address
  typedef logic [PHY_ADDR_WIDTH-1:0]      paddr_t; // physical address
  typedef logic [CACHE_LINE_BYTE*8-1:0]   cache_line_t; // cache line data
  typedef logic [CACHE_OFFSET_WIDTH-1:0]  cache_ofs_t; // byte offset inside a cache line

  //==========================================================
  // time
  
  typedef logic [TIMER_WIDTH-1:0]             timer_t;

  //==========================================================
  // virtual memory
  typedef logic [PAGE_OFFSET_WIDTH-1:0]       page_ofs_t; // byte offset inside a page
  typedef logic [VIR_PAGE_NUM_WIDTH-1:0]      vpn_t;
  typedef logic [VIR_PAGE_NUM_PART_WIDTH-1:0] vpn_part_t;

  typedef logic [PHY_PAGE_NUM_WIDTH-1:0]      ppn_t;
  typedef logic [PHY_PAGE_PART_WIDTH-1:0]     ppn_part_t;
  typedef logic [ADDR_SPACE_ID_WIDTH-1:0]     asid_t;

  //==========================================================
  // AXI4
  typedef enum logic[1:0] {
    AXI_RESP_OKAY = 2'b00,
    AXI_RESP_EXOKAY = 2'b01,
    AXI_RESP_SLVERR = 2'b10,
    AXI_RESP_DECERR = 2'b11
  } axi4_resp_t;

  //==========================================================
  // CPU to Cache interface
  typedef cache_line_t                  cpu_data_t;
  typedef logic [CACHE_LINE_BYTE-1:0]   cpu_byte_mask_t;

  typedef enum logic [3:0] {
    REQ_LR              = 4'b1000,
    REQ_SC              = 4'b1001,
    REQ_BARRIER_SET     = 4'b1010,
    REQ_BARRIER_SYNC    = 4'b1011,
    REQ_READ            = 4'b0000,
    REQ_WRITE           = 4'b0010,
    REQ_WRITE_NO_ALLOC  = 4'b0011, // no-allocate write
    REQ_AMO_LR          = 4'b0100, // load-reserved
    REQ_AMO_SC          = 4'b0101, // store-conditional
    REQ_FLUSH_IDX       = 4'b0110,
    REQ_FLUSH_ADDR      = 4'b0111
  } cpu_req_type_t;

  typedef struct packed {
    logic [CPUNOC_TID_MASTERID_SIZE-1:0] cpu_noc_id;
    logic [CPUNOC_TID_SRCID_SIZE-1:0]  src;
    logic [CPUNOC_TID_TID_SIZE-1:0]  tid;
  } cpu_tid_t;

  //==========================================================
  // Cache to mem interface
  `ifndef FPGA_ES1X
  typedef logic [MEM_DATA_WIDTH-1:0]    mem_data_t;
  `endif
  typedef logic [MEM_ADDR_WIDTH-1:0]    mem_addr_t;
  typedef struct packed {
    logic [MEMNOC_TID_MASTERID_SIZE-1:0] bid;
    logic [MEMNOC_TID_TID_SIZE-1:0] tid;
  } mem_tid_t;

  //==========================================================
  typedef logic [RING_ADDR_WIDTH-1:0]       ring_addr_t;
  typedef logic [RING_DATA_WIDTH-1:0]       ring_data_t;
  typedef logic [RING_STRB_WIDTH-1:0]       ring_strb_t;
  typedef logic [RING_MAX_ID_WIDTH+4-1:0]   ring_tid_t; // MSB is the station ID, which is different from station to station

  //==========================================================
  // DMA internal interface
  typedef struct packed {
    logic [6:0] tid; // [6]:~dma_thread, [5:4]:thread_id, [3:0]:tid
    logic [1:0] chk;
    cpu_req_type_t req_type;
    ring_addr_t addr;
    ring_data_t wdata;
    ring_strb_t mask;
  } dma_req_t;

  typedef struct packed {
    logic [6:0] tid;
    ring_data_t rdata;
    logic       is_wr_rsp;
  } dma_rsp_t;

  typedef logic dma_thread_align_t;
  parameter int DMA_RPT_CNT_WIDTH = 5;
  typedef logic [DMA_RPT_CNT_WIDTH-1:0] dma_thread_rpt_cnt_t;

  //! DMA
  typedef enum logic {READ, WRITE} rw_t;
  typedef enum logic [3:0] {DMA_IN_IDLE, DMA_AWRDY, DMA_BRSP, DMA_DBG_AWRDY, DMA_DBG_WR, DMA_ERR_AWRDY, DMA_ERR_BRSP, DMA_DBG_ARRDY, DMA_DBG_RD, DMA_DBG_RRSP, DMA_ERR_ARRDY, DMA_ERR_RRSP} dma_in_state_t;
  // end_addr is next line of last line
  typedef struct packed {logic [RING_ADDR_WIDTH-1 : 0] end_addr; logic [CACHE_LINE_BYTE*8-1:0] data; rw_t rw; logic [RING_MAX_ID_WIDTH+4-1:0] tid;} dma_cmd_t;
  typedef logic dma_thread_cmd_vld_t;
  typedef enum {DMA_OUT_IDLE, DMA_OUT_REQ} dma_out_state_t;
  typedef enum {DMA_PF_ADDR, DMA_DBG_ADDR, DMA_DBG_WDATA, DMA_DBG_RDATA} dma_addr_t;
  typedef enum logic [1:0] {
    DMA_FLUSH_ADDR = 2'b00,
    DMA_FLUSH_IDX  = 2'b01,
    DMA_FLUSH_ALL  = 2'b10
  } dma_flush_type_t;
  typedef enum logic [2:0] {
    DMA_DEBUG_RD_8B   = 3'b000,
    DMA_DEBUG_RD_4B   = 3'b001,
    DMA_DEBUG_WR_8B   = 3'b010,
    DMA_DEBUG_WR_4B   = 3'b011,
    DMA_DEBUG_BARRIER = 3'b100
  } dma_dbg_req_type_t;

  //==========================================================
  // AHB interface
  typedef logic [AHB_ADDR_WIDTH-1:0]      ahb_addr_t;
  typedef logic [AHB_DATA_WIDTH-1:0]      ahb_data_t;
  typedef enum logic [2:0] {
    AHB_SIZE_BYTE     = 3'b000,
    AHB_SIZE_HWORD    = 3'b001,
    AHB_SIZE_WORD     = 3'b010,
    AHB_SIZE_DWORD    = 3'b011,
    AHB_SIZE_4WORD    = 3'b100,
    AHB_SIZE_8WORD    = 3'b101
  } ahb_size_t;
  typedef enum logic [1:0] {
    AHB_TRANS_IDLE    = 2'b00,
    AHB_TRANS_BUSY    = 2'b01,
    AHB_TRANS_NONSEQ  = 2'b10,
    AHB_TRANS_SEQ     = 2'b11
  } ahb_trans_t;

  parameter N_DMA_CHNL = 4;

  //==========================================================
  // MEM-NOC interface
  //typedef logic [MEM_NOC_ADDR_WIDTH-1:0]		mem_noc_addr_t;
  //typedef logic [MEM_NOC_ID_WIDTH-1:0] 		mem_noc_id_t;
  //typedef logic [MEM_NOC_DATA_WIDTH-1:0]		mem_noc_data_t;
  //typedef logic [MEM_NOC_DATA_MASK_WIDTH-1:0]		mem_noc_data_mask_t;
  //typedef logic [MEM_NOC_RESP_STATUS_WIDTH-1:0]	mem_noc_resp_status_t;

  //==========================================================
  // L2 Cache typedef

  // physical address and its break-down
  typedef logic [OFFSET_WIDTH-1:0]        offset_t;
  typedef logic [BANK_ID_WIDTH-1:0]       bank_id_t;
  typedef logic [BANK_INDEX_WIDTH-1:0]    bank_index_t;
  typedef logic [TAG_WIDTH-1:0]           tag_t;
  typedef logic [DRAM_TAG_WIDTH-1:0]      dram_tag_t;
  typedef logic [WAY_ID_WIDTH-1:0]        way_id_t;
  typedef struct packed {
    tag_t         tag;
    bank_index_t  bank_index;
    bank_id_t     bank_id;
    offset_t      offset;
  } cache_paddr_t;

  typedef struct packed {
    logic [4:0] cpu_id;
    logic       is_scalar;  
    logic       is_dcache; 
    logic [4:0] tran_id;
  } tid_t;

  typedef logic [MEM_DATA_WIDTH/8-1:0]      mem_byte_mask_t;
  typedef logic [MEM_OFFSET_WIDTH-1:0]      mem_offset_t;

  typedef logic [N_MSHR-1:0]                mshr_sel_t;
  typedef logic [$clog2(N_MSHR)-1:0]        mshr_id_t;

  typedef logic [LRU_WIDTH-1:0]             lru_t;

  typedef enum logic [1:0] {
    VLS_CFG_2WAY = 2'b01,
    VLS_CFG_4WAY = 2'b10,
    VLS_CFG_6WAY = 2'b11,
    VLS_CFG_8WAY = 2'b00
  } vls_cfg_t;

  typedef struct packed {
    vls_cfg_t cfg; // VLS config state, reset to 0 = VLS_CFG_8WAY
    logic     en; // VLS enable, reset to 1
    tag_t     pbase;
  } vls_ctrl_t;

  typedef enum logic [1:0] {
    PWR_NORMAL  = 2'b00,
    PWR_SLEEP   = 2'b01,
    PWR_DOWN    = 2'b11
  } pwr_state_t;

  typedef struct packed {
    logic en_hpmcounter;
    pwr_state_t [N_BANK-1:0] bank_pwr_state; // reset to 0 = PWR_NORMAL
    pwr_state_t [N_WAY-1:0] way_pwr_state; // reset to 0 = PWR_NORMAL
  } pwr_ctrl_t;

  typedef struct packed {
    pwr_ctrl_t  pwr;
    vls_ctrl_t  vls;
  } ctrl_reg_t;

  //------------------------------------------------------
  // VLS <-> CPU
  //typedef enum logic [1:0] {
  //  REQ_READ  = 2'b00,
  //  REQ_WRITE = 2'b01,
  //  REQ_AMO   = 2'b10,
  //  REQ_FLUSH = 2'b11
  //} req_type_t; // request type,

  typedef struct packed {
    cache_paddr_t         paddr;
    cpu_byte_mask_t       byte_mask; // for both read and write
    cpu_data_t            data;
    cpu_tid_t             tid; 		 // request transaction ID
    cpu_req_type_t        req_type;  // request type
  } cpu_req_t;

  typedef struct packed {
    cpu_tid_t               tid;
    cpu_byte_mask_t         byte_mask;  // read byte mask to disable partial registers to save energy
    cpu_data_t              data;       // read data
  } cpu_resp_t;

  //------------------------------------------------------
  // MSHR definition
  typedef enum logic [2:0] {
    MSHR_WAIT_WRITE_BACK,
    MSHR_WRITE_BACK,
    MSHR_WAIT_ALLOCATE
  } mshr_state_t;

  typedef struct packed {
    logic                 valid;

    tid_t                 tid; // transaction id to the cpu side
    logic                 rw; // read or write miss
    logic                 flush;
    logic                 no_write_alloc;
    tag_t                 old_tag, // WB addr
                          new_tag; // read addr
    bank_index_t          bank_index; // bank index (index - bank_id)
    way_id_t              way_id; // way id
    cpu_data_t            data; // data to write (for write miss)
    cpu_byte_mask_t       byte_mask; // data read/write mask
  } mshr_t;

  typedef struct packed {
    // axi bus status
    logic                   waddr;
    logic                   wdata;
  } mshr_mem_state_t;

  // wdata pipeline registers
  typedef struct packed {
    bank_index_t                            waddr;
    mshr_id_t                               wid;
    //logic                                   flush;
    logic                                   wvalid;
    //logic                                   wlast;
    //logic [$clog2(N_WAY)-1:0]               way_id;
  } wdata_pipe_t;

  typedef struct packed {
    // cache data write pipeline
    wdata_pipe_t                            wdata_pipe;
    mem_offset_t                            wdata_offset;
    //mshr_mem_state_t [N_MSHR-1:0]           mem_state;
    //mem_offset_t rdata_offset;

    // data output pipeline
    cpu_resp_t  rdata_pipe;
    logic       rdata_pipe_valid;
  } mem_fsm_reg_t;

  typedef struct packed {
    mshr_id_t id;
    logic     rw;
    logic     no_write_alloc;
    logic     flush;
    logic     valid;
  } mshr_req_t; // request interface pipeline registers

//==========================================================
// CPU Noc to Cache memory barrier interface
  typedef struct packed {
     logic [MEM_BARRIER_STATUS_WIDTH-1:0] status;
  } mem_barrier_sts_t;

  typedef struct packed {
    mem_barrier_sts_t status;
  } mem_barrier_sts_banks_t;

endpackage

`endif
