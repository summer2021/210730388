// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*************************************************************************
 * Execution stage
 **************************************************************************/

module rrv64_execute
  import rrv64_param_pkg::*;
  import rrv64_typedef_pkg::*;
  import rrv64_func_pkg::*;
  import rrv64_decode_func_pkg::*;
  import soc_typedef::*;
  import soc_intf_typedef::*;
(
  input [7:0]		      core_id,
  // ID -> EX
  input   rrv64_id2ex_t       id2ex_ff,
  input   rrv64_data_t        id2ex_fp_rs1_ff,
  input   rrv64_itb_packet_t  rff_id2ex_itb_data,
  // EX -> BPU
  output rrv64_pred_update_t  ex2bpu_update,

  `ifdef RRV64_SUPPORT_MULDIV
  input   rrv64_id2muldiv_t   id2mul_ff, id2div_ff, id2mul, id2div,
  `endif // RRV64_SUPPORT_MULDIV

  `ifdef RRV64_SUPPORT_FP
  input   rrv64_id2fp_add_t   id2fp_add_s_ff,
  input   rrv64_id2fp_mac_t   id2fp_mac_s_ff,
  input   rrv64_id2fp_div_t   id2fp_div_s_ff,
  input   rrv64_id2fp_sqrt_t  id2fp_sqrt_s_ff,
  input   rrv64_id2fp_misc_t  id2fp_misc_ff,

  `ifdef RRV64_SUPPORT_FP_DOUBLE
  input   rrv64_id2fp_add_t   id2fp_add_d_ff,
  input   rrv64_id2fp_mac_t   id2fp_mac_d_ff,
  input   rrv64_id2fp_div_t   id2fp_div_d_ff,
  input   rrv64_id2fp_sqrt_t  id2fp_sqrt_d_ff,
  `endif // RRV64_SUPPORT_FP_DOUBLE

  `endif // RRV64_SUPPORT_FP

  input   rrv64_id2ex_ctrl_t  id2ex_ctrl_ff,
  input   rrv64_excp_t        id2ex_excp_ff,
  // EX -> MA
  output  rrv64_ex2ma_t       ex2ma_ff,
  output  rrv64_ex2ma_ctrl_t  ex2ma_ctrl_ff,
  output  rrv64_excp_t        ex2ma_excp_ff,
  output  rrv64_itb_packet_t  rff_ex2ma_itb_data,
  // EX -> L1D$
  output  rrv64_ex2dc_t       ex2dc,
  input   rrv64_dc2ma_t       dc2ma,
  input   rrv64_data_t        ma2ex_amo_ld_data,
  // EX -> IF: next pc
  output  rrv64_npc_t         ex2if_npc,
  output  logic         ex2if_kill, ex2id_kill,
  output  logic         branch_solved,
  // EX -> ID: bypass
  output  rrv64_bps_t         ex2id_bps,
  // MA -> EX: bypass
  input   rrv64_bps_t         ma2ex_bps,
  // EX -> ID: scoreboard kill addr
  output  rrv64_reg_addr_t    ex2id_kill_addr,
  // EX -> DTLB
  output logic                       ex2dtlb_flush_req_valid,
  output rrv64_tlb_flush_if_req_t    ex2dtlb_flush_req,
  // EX -> VTLB
//  output logic                       ex2vtlb_flush_req_valid,
//  output rrv64_tlb_flush_if_req_t    ex2vtlb_flush_req,
  // EX -> DA
  output  rrv64_ex2da_t       ex2da,

  // privilege level
  input   rrv64_prv_t         prv,
  input   rrv64_csr_mstatus_t mstatus,
  // pipeline ctrl
  input   logic         ex_stall, ex_kill, if_ready, id_ready, id_valid, id_valid_ff, ma_ready,
  output  logic         ex_valid, ex_valid_ff, ex_ready,
  output  logic         ex_is_stable_stall,
  // performance counter
  output  logic         wait_for_ex,
  output  logic         ex_wait_for_reg,

  output  rrv64_vaddr_t       ex_pc,
  // atomic
  // rst & clk
  input                 rst, clk
);

  rrv64_ex2ma_t       ex2ma;
  rrv64_ex2ma_ctrl_t  ex2ma_ctrl;
  rrv64_excp_t        ex2ma_excp;
  rrv64_itb_packet_t  ex2ma_itb_data;

  logic     hold_ex2dc, hold_ex2dc_ff, ex2dc_en_ff, ex2dc_en;
  logic     is_sfence, do_trap_sfence;
  logic     do_trap_sret;

  rrv64_opcode_t      opcode;
  rrv64_reg_addr_t    rd_addr, rs1_addr, rs2_addr, rs3_addr;
  logic [ 1:0]  funct2;
  logic [ 2:0]  funct3;
  logic [ 4:0]  funct5;
  logic [ 5:0]  funct6;
  logic [ 6:0]  funct7;
  logic [11:0]  funct12;
  logic [ 1:0]  fmt;
  rrv64_data_t        i_imm, s_imm, b_imm, u_imm, j_imm, z_imm;
  rrv64_frm_t         frm;

  rrv64_data_t    dff_amo_rs2;

  //==========================================================
  // ICG {{{

  logic en_amo_addr, en_amo_wdata, en_ex2if_npc;
  logic amo_addr_clkg, amo_wdata_clkg, ex2dc_clkg, ex2if_clkg;
  logic amo_rs2_clkg, en_amo_rs2;

  assign en_amo_addr = ex_valid & id2ex_ctrl_ff.is_amo_load & ma_ready;
  assign en_amo_wdata = ex_valid & id2ex_ctrl_ff.is_amo_op & ma_ready;
  assign en_ex2if_npc = ex2if_npc.valid;
  assign en_amo_rs2 = id2ex_ctrl_ff.is_amo_mv;

  icg amo_addr_clk_u(
    .en       (en_amo_addr),
    .tst_en   (1'b0),
    .clk      (clk), //ICG input
    .clkg     (amo_addr_clkg)
  );

  icg amo_wdata_clk_u(
    .en       (en_amo_wdata),
    .tst_en   (1'b0),
    .clk      (clk), //ICG input
    .clkg     (amo_wdata_clkg)
  );

  icg ex2dc_clk_u(
    .en       (ex2dc_en),
    .tst_en   (1'b0),
    .clk      (clk), //ICG input
    .clkg     (ex2dc_clkg)
  );

  icg ex2if_clk_u(
    .en       (en_ex2if_npc),
    .tst_en   (1'b0),
    .clk      (clk), //ICG input
    .clkg     (ex2if_clkg)
  );

  icg amo_rs2_clk_u(
    .en       (en_amo_rs2),
    .tst_en   (1'b0),
    .clk      (clk), //ICG input
    .clkg     (amo_rs2_clkg)
  );

  // }}}

  //==========================================================
  // MA2EX forwarding path {{{
//  rrv64_data_t final_rs1, final_rs2;

  // MA2EX_FORWARDING begin
  logic        is_reg_not_final;
  logic        rff_ma2ex_hold_valid;
  rrv64_data_t final_rs1, final_rs2;
  rrv64_data_t dff_ma2ex_hold_data;
  logic        ma2ex_final_valid;
  rrv64_data_t ma2ex_bps_data;
  logic        wait_for_rs1, wait_for_rs2;

  assign is_reg_not_final = id_valid_ff & ~(id2ex_ff.is_rs1_final & id2ex_ff.is_rs2_final) & ~id2ex_ctrl_ff.is_amo_op;

  assign ma2ex_final_valid = (ma2ex_bps.valid_addr & ma2ex_bps.valid_data) | rff_ma2ex_hold_valid;
  assign ma2ex_bps_data = rff_ma2ex_hold_valid ? dff_ma2ex_hold_data: ma2ex_bps.rd;

  assign final_rs1 = id2ex_ctrl_ff.is_amo_op ? ma2ex_amo_ld_data: id2ex_ff.is_rs1_final ? (id2mul_ff.en ? id2mul_ff.rs1 : id2div_ff.en ? id2div_ff.rs1 : id2ex_ff.rs1) : ma2ex_bps_data;
  assign final_rs2 = id2ex_ctrl_ff.is_amo_op ? dff_amo_rs2: id2ex_ff.is_rs2_final ? (id2mul_ff.en ? id2mul_ff.rs2 : id2div_ff.en ? id2div_ff.rs2 : id2ex_ff.rs2) : ma2ex_bps_data;

  assign wait_for_rs1 = id_valid_ff & ~id2ex_ff.is_rs1_final & ~ma2ex_final_valid & ~id2ex_ctrl_ff.is_amo_op;
  assign wait_for_rs2 = id_valid_ff & ~id2ex_ff.is_rs2_final & ~ma2ex_final_valid & ~id2ex_ctrl_ff.is_amo_op;

  assign ex_wait_for_reg = (wait_for_rs1 | wait_for_rs2) & ~id2ex_excp_ff.valid & ~ex_kill;

  always_ff @(posedge clk) begin
   if (rst) begin
     rff_ma2ex_hold_valid <= '0;
   end else begin
     if (rff_ma2ex_hold_valid) begin
       rff_ma2ex_hold_valid <= ~(ex_valid & ma_ready);
     end if (is_reg_not_final & ma2ex_bps.valid_addr & ma2ex_bps.valid_data & ~(ex_valid & ma_ready)) begin
       rff_ma2ex_hold_valid <= '1;
       dff_ma2ex_hold_data <= ma2ex_bps.rd;
     end
   end
  end
  // MA2EX_FORWARDING end

//  assign ex_wait_for_reg = '0;
//  assign final_rs1 = id2ex_ctrl_ff.is_amo_op ? ma2ex_amo_ld_data: id2ex_ff.rs1;
//  assign final_rs2 = id2ex_ctrl_ff.is_amo_op ? dff_amo_rs2: id2ex_ff.rs2;


`ifndef SYNTHESIS
`ifndef VERILATOR
  ma2ex_fwd_rs1_mismatch: assert property (@(posedge clk) disable iff (rst === '0) ((id_valid_ff & ~id2ex_ff.is_rs1_final & ma2ex_bps.valid_addr & ma2ex_bps.valid_data & ~id2ex_excp_ff.valid) |-> (ma2ex_bps.rd_addr == rs1_addr))) else `olog_error("RRV_EXECUTE", $sformatf("%m: ma2ex_rd_addr=%h rs1_addr=%h", ma2ex_bps.rd_addr, rs1_addr));
  ma2ex_fwd_rs2_mismatch: assert property (@(posedge clk) disable iff (rst === '0) ((id_valid_ff & ~id2ex_ff.is_rs2_final & ma2ex_bps.valid_addr & ma2ex_bps.valid_data & ~id2ex_excp_ff.valid) |-> (ma2ex_bps.rd_addr == rs2_addr))) else `olog_error("RRV_EXECUTE", $sformatf("%m: ma2ex_rd_addr=%h rs2_addr=%h", ma2ex_bps.rd_addr, rs2_addr));
`endif
`endif

  // }}}

  //==========================================================
  // flop value of rs2 for amo op{{{

  always_ff @(posedge amo_rs2_clkg) begin
    if (en_amo_rs2) begin
      dff_amo_rs2 <= final_rs2;
    end
  end 

  //------------------------------------------------------
  // DTLB sfence

  logic ex2dtlb_flush_req_valid_unmasked;

  assign do_trap_sfence = mstatus.TVM & (prv == RRV64_PRV_S);
  assign is_sfence = (opcode == RRV64_SYSTEM) & (funct7 == 7'h09) & (funct3 == 3'h0) & (rd_addr == 5'h00) & id_valid_ff & ~ex_kill & ~do_trap_sfence;

  always_comb begin
    ex2dtlb_flush_req.req_flush_asid = final_rs2;
    ex2dtlb_flush_req.req_flush_vpn = (final_rs1 >> RRV64_PAGE_OFFSET_WIDTH);
    if ((ex2dtlb_flush_req.req_flush_asid != '0) & (ex2dtlb_flush_req.req_flush_vpn != '0)) begin
      ex2dtlb_flush_req.req_sfence_type = RRV64_SFENCE_ASID_VPN;
    end else if ((ex2dtlb_flush_req.req_flush_asid != '0) & (ex2dtlb_flush_req.req_flush_vpn == '0)) begin
      ex2dtlb_flush_req.req_sfence_type = RRV64_SFENCE_ASID;
    end else if ((ex2dtlb_flush_req.req_flush_asid == '0) & (ex2dtlb_flush_req.req_flush_vpn != '0)) begin
      ex2dtlb_flush_req.req_sfence_type = RRV64_SFENCE_VPN;
    end else begin
      ex2dtlb_flush_req.req_sfence_type = RRV64_SFENCE_ALL;
    end
  end

  assign ex2dtlb_flush_req_valid = is_sfence & (ex_valid & ma_ready);

  //------------------------------------------------------
  // VTLB sfence

//  assign ex2vtlb_flush_req = ex2dtlb_flush_req;
//  assign ex2vtlb_flush_req_valid = ex2dtlb_flush_req_valid;

  //==========================================================
  // TSR check
  assign do_trap_sret = (mstatus.TSR & (prv == RRV64_PRV_S)) | ~((prv == RRV64_PRV_S) | (prv == RRV64_PRV_M));

  //==========================================================
  // imm selection {{{
  rrv64_data_t imm;
  always_comb
    case (id2ex_ctrl_ff.imm_sel)
      RRV64_IMM_SEL_I: imm = i_imm;
      RRV64_IMM_SEL_S: imm = s_imm;
      RRV64_IMM_SEL_B: imm = b_imm;
      RRV64_IMM_SEL_U: imm = u_imm;
      RRV64_IMM_SEL_J: imm = j_imm;
      RRV64_IMM_SEL_Z: imm = z_imm;
      default: imm = 64'b0;
    endcase
  // }}}

  //==========================================================
  // ALU {{{

  // select op1 & op2
  rrv64_data_t    op1, op2;
  always_comb begin
    case (id2ex_ctrl_ff.op1_sel)
      RRV64_OP1_SEL_RS1:    op1 = final_rs1;
      `ifdef RRV64_SUPPORT_FP
      RRV64_OP1_SEL_FP_RS1: op1 = id2ex_fp_rs1_ff;
      RRV64_OP1_SEL_FP_RS1_SEXT: op1 = {{32{id2ex_fp_rs1_ff[31]}}, id2ex_fp_rs1_ff[31:0]};
      `endif // RRV64_SUPPORT_FP
      RRV64_OP1_SEL_ZERO:   op1 = 'b0;
      default:        op1 = 'b0;
    endcase
  end

  always_comb begin
    case (id2ex_ctrl_ff.op2_sel)
      RRV64_OP2_SEL_RS2:      op2 = final_rs2;
      default:          op2 = imm;
    endcase
  end

  rrv64_data_t  alu_out, adder_out;
  rrv64_data_t  final_op1, final_op2;
  logic   cmp_out;

  generate
    for (genvar i=0; i<32; i++) begin
      assign final_op1[i] = op1[i];
      assign final_op2[i] = op2[i];
    end
  endgenerate

  generate
    for (genvar i=32; i<64; i++) begin
      assign final_op1[i] = id2ex_ctrl_ff.is_32 ? op1[31]: op1[i];
      assign final_op2[i] = id2ex_ctrl_ff.is_32 ? op2[31]: op2[i];
    end
  endgenerate

  rrv64_alu ALU(
    .alu_out, .adder_out, .cmp_out,
    .op1(final_op1), .op2(final_op2), .alu_op(id2ex_ctrl_ff.alu_op), .is_32(id2ex_ctrl_ff.is_32)
  );
  // }}}

  //==========================================================
  // PC adder and branch taken logic {{{
  rrv64_data_t  pc_adder_out;
  rrv64_data_t 	pc_adder_ext;
  rrv64_data_t 	next_pc;
  rrv64_vaddr_t jump_pc;
  logic   branch_taken;
  logic   ex2if_npc_valid_pre;
  logic   is_misaligned_if_addr;
  assign  pc_adder_ext = {{(RRV64_XLEN-RRV64_VIR_ADDR_WIDTH){id2ex_ff.pc[RRV64_VIR_ADDR_WIDTH-1]}}, id2ex_ff.pc};
  assign  next_pc = pc_adder_ext + (id2ex_ff.is_rvc ? 'h2: 'h4);
  assign  pc_adder_out = pc_adder_ext + ((id2ex_ctrl_ff.ex_pc_sel == RRV64_EX_PC_SEL_P4) ? (id2ex_ff.is_rvc ? 'h2: 'h4) : imm);

  rrv64_vaddr_t   ex2if_pc_saved_ff;
  logic   hold_ex2if_kill_ff, ex2if_kill_saved_ff, clear_ex2if_npc_valid;
  always_ff @ (posedge clk) begin
    if (rst) begin
      hold_ex2if_kill_ff <= 1'b0;
      clear_ex2if_npc_valid <= 1'b0;
      ex2if_kill_saved_ff <= 1'b0;
    end else begin
      hold_ex2if_kill_ff <= (ex2if_kill & (~if_ready | ~id_ready)); // when IF or ID is not ready, they are not ready to be killed. so need to hold the kill signal till after they are ready to be killed
      if (~clear_ex2if_npc_valid & ex2if_npc.valid & if_ready & id_ready)
        clear_ex2if_npc_valid <= '1; // clear kill signal after its accepted
      else if (clear_ex2if_npc_valid & id_valid & ex_ready)
        clear_ex2if_npc_valid <= '0;
      if (ex2if_npc.valid) begin
        ex2if_kill_saved_ff <= ex2if_kill;
      end
    end
  end

  always_ff @(posedge ex2if_clkg) begin
    if (en_ex2if_npc) begin
      ex2if_pc_saved_ff <= ex2if_npc.pc;
    end
  end

  assign  jump_pc = {alu_out[RRV64_XLEN-1:1], 1'b0};

  logic is_branch, pred_taken, mispred;
  logic need_clear_delay_slot;
  reg delay_slot_kill_once;
  always @(posedge clk) begin
      if(rst) begin
          delay_slot_kill_once <= 0;
      end else if(!(ex_valid & ma_ready) & need_clear_delay_slot) begin
          delay_slot_kill_once <= 1;
      end else if(ex_valid & ma_ready) begin
          delay_slot_kill_once <= 0;
      end
  end
  assign  is_branch = (opcode == RRV64_BRANCH) | (opcode == RRV64_JALR);
  // ID will remain an instruction as a "delay slot", if pred_taken actually taken, need to flush id
  assign  need_clear_delay_slot = (ex_valid & ~clear_ex2if_npc_valid & is_branch) & (pred_taken & branch_taken);
  // assign  branch_taken = (opcode == RRV64_BRANCH) ? cmp_out : 1'b0;
  assign  branch_taken = ((opcode == RRV64_BRANCH) ? cmp_out : 1'b0) | (opcode == RRV64_JALR);
  assign  branch_solved = (opcode == RRV64_BRANCH) & ex_valid & ~clear_ex2if_npc_valid; // clear branch_solved if it's accepted
  assign  ex2if_npc.pc = hold_ex2if_kill_ff ? ex2if_pc_saved_ff: ((opcode == RRV64_JALR) ? jump_pc : (branch_taken ? pc_adder_out : next_pc));
  // assign  ex2if_npc_valid_pre = hold_ex2if_kill_ff | ((((opcode == RRV64_JALR) | branch_taken)) & ex_valid & ~clear_ex2if_npc_valid);
  assign  ex2if_npc_valid_pre = hold_ex2if_kill_ff | (is_branch & mispred & ex_valid & ~clear_ex2if_npc_valid);
  assign  is_misaligned_if_addr = (ex2if_npc.pc[0] != 1'b0) & ex2if_npc_valid_pre;
  assign  ex2if_npc.valid = ex2if_npc_valid_pre & ~ex2ma_excp.valid;
  assign  ex2if_kill = ex2if_npc.valid;
  // assign  ex2id_kill = ex2if_kill;
  assign  ex2id_kill = ex2if_kill | (need_clear_delay_slot & ~delay_slot_kill_once);

  // Handle the branch prediction logic 
  rrv64_vaddr_t current_pc = id2ex_ff.pc;
  rrv64_vaddr_t pred_pc = id2ex_ff.bpred.pred_target;
  assign pred_taken = id2ex_ff.bpred.pred_taken;
  assign mispred = (pred_taken != branch_taken) || (pred_taken && pred_pc != ex2if_npc.pc);

  // Update logic
  assign ex2bpu_update.valid = is_branch & ex_valid & ma_ready;
  assign ex2bpu_update.branch_addr = ex_pc;
  assign ex2bpu_update.branch_taken = branch_taken;
  assign ex2bpu_update.branch_bimodal = id2ex_ff.bpred.pred_bimodal;

  // Debug

   
  // }}}
 
  //==========================================================
  // MULDIV {{{
  logic wait_for_mul, wait_for_div;
`ifdef RRV64_SUPPORT_MULDIV

  // explicitly defined clock gating
  logic   clkg_mul, clkg_div;
//   logic   mul_en, mul_en_ff;
//   logic   div_en, div_en_ff;
  rrv64_clk_gating CG_MUL(.tst_en(1'b0), .en(id2mul_ff.en), .clk(clk), .rst(rst), .clkg(clkg_mul));
  rrv64_clk_gating CG_DIV(.tst_en(1'b0), .en(id2div_ff.en), .clk(clk), .rst(rst), .clkg(clkg_div));

  // MUL {{{
  rrv64_data_t  mul_rdh, mul_rdl;
  logic   mul_start_pulse, mul_complete;

  always_ff @ (posedge clk)
    if (rst | (id2mul.en & mul_complete & ~id2mul.is_same & ex_ready))
      mul_start_pulse <= 1'b1;
    else if (id2mul_ff.en & ~ex_wait_for_reg & mul_start_pulse)
      mul_start_pulse <= 1'b0;

//   assign  mul_en = (id2ex_ctrl_ff.mul_type != RRV64_MUL_TYPE_NONE) & id_valid_ff;
  assign  wait_for_mul = id2mul_ff.en & ~mul_complete;

  // MA2EX_FORWARDING begin
  rrv64_mul MUL(.rdh(mul_rdh), .rdl(mul_rdl), .complete(mul_complete),
   .rs1(final_rs1), .rs2(final_rs2), .mul_type(id2ex_ctrl_ff.mul_type), .start_pulse(mul_start_pulse),
   .clk(clkg_mul), .*);
  // MA2EX_FORWARDING end
  // rrv64_mul MUL(.rdh(mul_rdh), .rdl(mul_rdl), .complete(mul_complete),
  //   .rs1(id2mul_ff.rs1), .rs2(id2mul_ff.rs2), .mul_type(id2ex_ctrl_ff.mul_type), .start_pulse(mul_start_pulse),
  //   .clk(clkg_mul), .*);
  // }}}

  // DIV {{{
  rrv64_data_t  div_rdq, div_rdr;
  logic   div_start_pulse, div_complete;

  always_ff @ (posedge clk)
    if (rst | (id2div.en & div_complete & ~id2div.is_same & ex_ready))
      div_start_pulse <= 1'b1;
    else if (id2div_ff.en & ~ex_wait_for_reg & div_start_pulse)
      div_start_pulse <= 1'b0;

//   assign  div_en = (id2ex_ctrl_ff.div_type != RRV64_DIV_TYPE_NONE) & id_valid_ff;
  assign  wait_for_div = id2div_ff.en & ~div_complete;

  // MA2EX_FORWARDING begin
  rrv64_div DIV(.rdq(div_rdq), .rdr(div_rdr), .complete(div_complete),
   .rs1(final_rs1), .rs2(final_rs2), .div_type(id2ex_ctrl_ff.div_type), .start_pulse(div_start_pulse),
   .clk(clkg_div), .*);
  // MA2EX_FORWARDING end
  // rrv64_div DIV(.rdq(div_rdq), .rdr(div_rdr), .complete(div_complete),
  //   .rs1(id2div_ff.rs1), .rs2(id2div_ff.rs2), .div_type(id2ex_ctrl_ff.div_type), .start_pulse(div_start_pulse),
  //   .clk(clkg_div), .*);
  // }}}
`else
  
  assign wait_for_mul = '0;
  assign wait_for_div = '0;

`endif // RRV64_SUPPORT_MULDIV
  // }}}

  //==========================================================
  // FP {{{
`ifdef RRV64_SUPPORT_FP

  // cycle counter and timing assertion {{{
  //  input = fp_start_pulse, fp_cntr_tgt
  //  output = fp_complete
  localparam  FP_CNTR_BIT_WIDTH = $clog2(RRV64_N_CYCLE_FP_SQRT_D);

  logic fp_cntr_en;
  logic fp_start_pulse, fp_complete, wait_for_fp;
  logic [FP_CNTR_BIT_WIDTH-1:0] fp_cntr_ff, fp_cntr_tgt;

  assign  fp_cntr_en = (id2ex_ctrl_ff.fp_op != RRV64_FP_OP_NONE) & id_valid_ff;
  assign  fp_start_pulse = (fp_cntr_ff == 'b0);
  assign  wait_for_fp = fp_cntr_en & ~fp_complete;

  always @ (posedge clk)
    if (rst | (fp_complete & ma_ready))
      fp_cntr_ff <= 'b0;
    else if (fp_cntr_en)
      fp_cntr_ff <= fp_cntr_ff + 1;

  assign fp_complete = (fp_cntr_ff == fp_cntr_tgt);

  import rrv64_param_pkg::*;

  always_comb
    case (id2ex_ctrl_ff.fp_op)
      RRV64_FP_OP_ADD_S:  fp_cntr_tgt = RRV64_N_CYCLE_FP_ADD_S - 1;
      RRV64_FP_OP_MAC_S:  fp_cntr_tgt = RRV64_N_CYCLE_FP_MAC_S - 1;
      RRV64_FP_OP_DIV_S:  fp_cntr_tgt = RRV64_N_CYCLE_FP_DIV_S - 1;
      RRV64_FP_OP_SQRT_S: fp_cntr_tgt = RRV64_N_CYCLE_FP_SQRT_S - 1;

      `ifdef RRV64_SUPPORT_FP_DOUBLE
      RRV64_FP_OP_ADD_D:  fp_cntr_tgt = RRV64_N_CYCLE_FP_ADD_D - 1;
      RRV64_FP_OP_MAC_D:  fp_cntr_tgt = RRV64_N_CYCLE_FP_MAC_D - 1;
      RRV64_FP_OP_DIV_D:  fp_cntr_tgt = RRV64_N_CYCLE_FP_DIV_D - 1;
      RRV64_FP_OP_SQRT_D: fp_cntr_tgt = RRV64_N_CYCLE_FP_SQRT_D - 1;
      `endif

      RRV64_FP_OP_MISC:   fp_cntr_tgt = RRV64_N_CYCLE_FP_MISC - 1;
      default:    fp_cntr_tgt = 'h0;
    endcase

  logic [130:0] add_s_input;
  assign add_s_input = {id2fp_add_s_ff.rs1, id2fp_add_s_ff.rs2, id2fp_add_s_ff.frm_dw};
  // }}}

  // ADD {{{
  rrv64_data_t        rd_add_s, rd_add_d;
  rrv64_fstatus_dw_t  fstatus_dw_add_s, fstatus_dw_add_d;

  `ifndef FPGA
  rrv64_fp_add_single FP_ADD_S(
    .rd(rd_add_s), .fstatus_dw(fstatus_dw_add_s),
    .rs1(id2fp_add_s_ff.rs1), .rs2(id2fp_add_s_ff.rs2), .frm_dw(id2fp_add_s_ff.frm_dw)
  );
  `else
    XIL_fp_add_single f_add_u (
      .aclk(clk),
      .aclken(1'b1),
      .aresetn(~rst),
      .s_axis_a_tvalid(1'b1),
      .s_axis_a_tdata(id2fp_add_s_ff.rs1[31:0]),
      .s_axis_b_tvalid(1'b1),
      .s_axis_b_tdata(id2fp_add_s_ff.rs2[31:0]),
      .m_axis_result_tvalid(),
      .m_axis_result_tdata(rd_add_s[31:0]),
      .m_axis_result_tuser({fstatus_dw_add_s.invalid, fstatus_dw_add_s.huge, fstatus_dw_add_s.tiny})
      );
    assign rd_add_s[63:32] = {32{1'b1}};
    assign fstatus_dw_add_s.compspecific = 1'b0;
    assign fstatus_dw_add_s.hugeint = 1'b0;
    assign fstatus_dw_add_s.inexact = 1'b0;
    assign fstatus_dw_add_s.infinity = 1'b0;
    assign fstatus_dw_add_s.zero = 1'b0;
  `endif

`ifdef RRV64_SUPPORT_FP_DOUBLE
  `ifndef FPGA
  rrv64_fp_add_double FP_ADD_D(
    .rd(rd_add_d), .fstatus_dw(fstatus_dw_add_d),
    .rs1(id2fp_add_d_ff.rs1), .rs2(id2fp_add_d_ff.rs2), .frm_dw(id2fp_add_d_ff.frm_dw)
  );
  `else
    XIL_fp_add_double d_add_u (
      .aclk(clk),
      .aclken(1'b1),
      .aresetn(~rst),
      .s_axis_a_tvalid(1'b1),
      .s_axis_a_tdata(id2fp_add_d_ff.rs1),
      .s_axis_b_tvalid(1'b1),
      .s_axis_b_tdata(id2fp_add_d_ff.rs2),
      .m_axis_result_tvalid(),
      .m_axis_result_tdata(rd_add_d),
      .m_axis_result_tuser({fstatus_dw_add_d.invalid, fstatus_dw_add_d.huge, fstatus_dw_add_d.tiny})
      );
    assign fstatus_dw_add_d.compspecific = 1'b0;
    assign fstatus_dw_add_d.hugeint = 1'b0;
    assign fstatus_dw_add_d.inexact = 1'b0;
    assign fstatus_dw_add_d.infinity = 1'b0;
    assign fstatus_dw_add_d.zero = 1'b0;
  `endif
`endif
  // }}}

  // CMP {{{
  rrv64_data_t        rd_cmp_s, rd_cmp_d;
  rrv64_fflags_t      fflags_cmp_s, fflags_cmp_d;
`ifndef FPGA
  rrv64_fp_cmp #(.is_32(1))  FP_CMP_S(.rd_cmp(rd_cmp_s), .fflags(fflags_cmp_s), .rs1(id2fp_add_s_ff.rs1), .rs2(id2fp_add_s_ff.rs2), .rd_add(rd_add_s), .fp_cmp_sel(id2ex_ctrl_ff.fp_cmp_sel));
`else
  logic [31:0] f_cmp_u_rslt;
  XIL_fp_cmp_single f_cmp_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_add_s_ff.rs1[31:0]),
    .s_axis_b_tvalid(1'b1),
    .s_axis_b_tdata(id2fp_add_s_ff.rs2[31:0]),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(f_cmp_u_rslt)
    );
  always_comb begin
    fflags_cmp_s = '0;
    case (id2ex_ctrl_ff.fp_cmp_sel)
      RRV64_FP_CMP_SEL_MAX: begin
        if (f_cmp_u_rslt[0] | f_cmp_u_rslt[1]) begin
          rd_cmp_s[31:0]  = id2fp_add_s_ff.rs2[31:0];
          rd_cmp_s[63:32] = {32{id2fp_add_s_ff.rs2[31]}};
        end else if (f_cmp_u_rslt[2]) begin
          rd_cmp_s[31:0]  = id2fp_add_s_ff.rs1[31:0];
          rd_cmp_s[63:32] = {32{id2fp_add_s_ff.rs1[31]}};
        end else if (f_cmp_u_rslt[3]) begin
          fflags_cmp_s.nv = 1'b1;
          rd_cmp_s = RRV64_CONST_FP_S_CANON_NAN;
        end
      end
      RRV64_FP_CMP_SEL_MIN: begin
        if (f_cmp_u_rslt[0] | f_cmp_u_rslt[1]) begin
          rd_cmp_s[31:0]  = id2fp_add_s_ff.rs1[31:0];
          rd_cmp_s[63:32] = {32{id2fp_add_s_ff.rs1[31]}};
        end else if (f_cmp_u_rslt[2]) begin
          rd_cmp_s[31:0]  = id2fp_add_s_ff.rs2[31:0];
          rd_cmp_s[63:32] = {32{id2fp_add_s_ff.rs2[31]}};
        end else if (f_cmp_u_rslt[3]) begin
          fflags_cmp_s.nv = 1'b1;
          rd_cmp_s = RRV64_CONST_FP_S_CANON_NAN;
        end
      end
      RRV64_FP_CMP_SEL_EQ: begin
        rd_cmp_s = f_cmp_u_rslt[0];
      end
      RRV64_FP_CMP_SEL_LT: begin
        fflags_cmp_s.nv = f_cmp_u_rslt[3];
        rd_cmp_s = f_cmp_u_rslt[1];
      end
      RRV64_FP_CMP_SEL_LE: begin
        fflags_cmp_s.nv = f_cmp_u_rslt[3];
        rd_cmp_s = f_cmp_u_rslt[1] | f_cmp_u_rslt[0];
      end
      default: begin
      end
    endcase
  end
`endif
`ifdef RRV64_SUPPORT_FP_DOUBLE
  `ifndef FPGA
  rrv64_fp_cmp #(.is_32(0))  FP_CMP_D(.rd_cmp(rd_cmp_d), .fflags(fflags_cmp_d), .rs1(id2fp_add_d_ff.rs1), .rs2(id2fp_add_d_ff.rs2), .rd_add(rd_add_d), .fp_cmp_sel(id2ex_ctrl_ff.fp_cmp_sel));
  `else
  logic [31:0] d_cmp_u_rslt;
  XIL_fp_cmp_double d_cmp_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_add_d_ff.rs1),
    .s_axis_b_tvalid(1'b1),
    .s_axis_b_tdata(id2fp_add_d_ff.rs2),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(d_cmp_u_rslt)
    );
  always_comb begin
    fflags_cmp_d = '0;
    case (id2ex_ctrl_ff.fp_cmp_sel)
      RRV64_FP_CMP_SEL_MAX: begin
        if (d_cmp_u_rslt[0] | d_cmp_u_rslt[1]) begin
          rd_cmp_d = id2fp_add_d_ff.rs2;
        end else if (d_cmp_u_rslt[2]) begin
          rd_cmp_d = id2fp_add_s_ff.rs1;
        end else if (d_cmp_u_rslt[3]) begin
          fflags_cmp_d.nv = 1'b1;
          rd_cmp_d = RRV64_CONST_FP_D_CANON_NAN;
        end
      end
      RRV64_FP_CMP_SEL_MIN: begin
        if (d_cmp_u_rslt[0] | d_cmp_u_rslt[1]) begin
          rd_cmp_d = id2fp_add_d_ff.rs1;
        end else if (d_cmp_u_rslt[2]) begin
          rd_cmp_d = id2fp_add_d_ff.rs2;
        end else if (d_cmp_u_rslt[3]) begin
          fflags_cmp_d.nv = 1'b1;
          rd_cmp_d = RRV64_CONST_FP_D_CANON_NAN;
        end
      end
      RRV64_FP_CMP_SEL_EQ: begin
        rd_cmp_d = d_cmp_u_rslt[0];
      end
      RRV64_FP_CMP_SEL_LT: begin
        fflags_cmp_d.nv = d_cmp_u_rslt[3];
        rd_cmp_d = d_cmp_u_rslt[1];
      end
      RRV64_FP_CMP_SEL_LE: begin
        fflags_cmp_d.nv = d_cmp_u_rslt[3];
        rd_cmp_d = d_cmp_u_rslt[1] | d_cmp_u_rslt[0];
      end
      default: begin
      end
    endcase
  end
  `endif
`endif
  // }}}

  // MAC {{{
  rrv64_data_t        rd_mac_s, rd_mac_d;
  rrv64_fstatus_dw_t  fstatus_dw_mac_s, fstatus_dw_mac_d;

`ifndef FPGA
  rrv64_fp_mac_single FP_MAC_S(
    .rd(rd_mac_s), .fstatus_dw(fstatus_dw_mac_s),
    .rs1(id2fp_mac_s_ff.rs1), .rs2(id2fp_mac_s_ff.rs2), .rs3(id2fp_mac_s_ff.rs3), .frm_dw(id2fp_mac_s_ff.frm_dw),
    .is_mul(id2fp_mac_s_ff.is_mul)
  );
`else
  XIL_fp_mac_single f_mac_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_mac_s_ff.rs1[31:0]),
    .s_axis_b_tvalid(1'b1),
    .s_axis_b_tdata(id2fp_mac_s_ff.rs2[31:0]),
    .s_axis_c_tvalid(1'b1),
    .s_axis_c_tdata(id2fp_mac_s_ff.rs3[31:0]),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(rd_mac_s[31:0]),
    .m_axis_result_tuser({fstatus_dw_mac_s.invalid, fstatus_dw_mac_s.huge, fstatus_dw_mac_s.tiny})
    );
  assign rd_mac_s[63:32] = {32{1'b1}};
  assign fstatus_dw_mac_s.compspecific = 1'b0;
  assign fstatus_dw_mac_s.hugeint = 1'b0;
  assign fstatus_dw_mac_s.inexact = 1'b0;
  assign fstatus_dw_mac_s.infinity = 1'b0;
  assign fstatus_dw_mac_s.zero = 1'b0;
`endif

`ifdef RRV64_SUPPORT_FP_DOUBLE
  `ifndef FPGA
  rrv64_fp_mac_double FP_MAC_D(
    .rd(rd_mac_d), .fstatus_dw(fstatus_dw_mac_d),
    .rs1(id2fp_mac_d_ff.rs1), .rs2(id2fp_mac_d_ff.rs2), .rs3(id2fp_mac_d_ff.rs3), .frm_dw(id2fp_mac_d_ff.frm_dw),
    .is_mul(id2fp_mac_d_ff.is_mul)
  );
  `else
  XIL_fp_mac_double d_mac_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_mac_d_ff.rs1[31:0]),
    .s_axis_b_tvalid(1'b1),
    .s_axis_b_tdata(id2fp_mac_d_ff.rs2[31:0]),
    .s_axis_c_tvalid(1'b1),
    .s_axis_c_tdata(id2fp_mac_d_ff.rs3[31:0]),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(rd_mac_d[31:0]),
    .m_axis_result_tuser({fstatus_dw_mac_d.invalid, fstatus_dw_mac_d.huge, fstatus_dw_mac_d.tiny})
    );
  assign rd_mac_d[63:32] = {32{1'b1}};
  assign fstatus_dw_mac_d.compspecific = 1'b0;
  assign fstatus_dw_mac_d.hugeint = 1'b0;
  assign fstatus_dw_mac_d.inexact = 1'b0;
  assign fstatus_dw_mac_d.infinity = 1'b0;
  assign fstatus_dw_mac_d.zero = 1'b0;
  `endif
`endif
  // }}}

  // DIV {{{
  logic fp_div_s_en, fp_div_d_en, clkg_fp_div_s, clkg_fp_div_d;
  assign fp_div_s_en = (id2ex_ctrl_ff.fp_op == RRV64_FP_OP_DIV_S) & id_valid_ff;
  assign fp_div_d_en = (id2ex_ctrl_ff.fp_op == RRV64_FP_OP_DIV_D) & id_valid_ff;
  rrv64_clk_gating CG_FP_DIV_S(.tst_en(1'b0), .en(fp_div_s_en), .clk(clk), .rst(rst), .clkg(clkg_fp_div_s));
  rrv64_clk_gating CG_FP_DIV_D(.tst_en(1'b0), .en(fp_div_d_en), .clk(clk), .rst(rst), .clkg(clkg_fp_div_d));

  rrv64_data_t        rd_div_s, rd_div_d;
  rrv64_fstatus_dw_t  fstatus_dw_div_s, fstatus_dw_div_d;
  logic         fp_div_s_div_0, fp_div_d_div_0;
  logic         complete_div_s, complete_div_d;
`ifndef FPGA
  rrv64_fp_div_single FP_DIV_S(
    .rd(rd_div_s), .fstatus_dw(fstatus_dw_div_s), .is_div_0(fp_div_s_div_0), .complete(complete_div_s),
    .rs1(id2fp_div_s_ff.rs1), .rs2(id2fp_div_s_ff.rs2), .frm_dw(id2fp_div_s_ff.frm_dw),
    .start_pulse(fp_start_pulse), .rst, .clk(clkg_fp_div_s)
  );
`else
  XIL_fp_div_single f_div_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_div_s_ff.rs1[31:0]),
    .s_axis_b_tvalid(1'b1),
    .s_axis_b_tdata(id2fp_div_s_ff.rs2[31:0]),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(rd_div_s[31:0]),
    .m_axis_result_tuser({fp_div_s_div_0, fstatus_dw_div_s.invalid, fstatus_dw_div_s.huge, fstatus_dw_div_s.tiny})
    );
  assign rd_div_s[63:32] = {32{1'b1}};
  assign fstatus_dw_div_s.compspecific = 1'b0;
  assign fstatus_dw_div_s.hugeint = 1'b0;
  assign fstatus_dw_div_s.inexact = 1'b0;
  assign fstatus_dw_div_s.infinity = 1'b0;
  assign fstatus_dw_div_s.zero = 1'b0;
`endif
`ifdef RRV64_SUPPORT_FP_DOUBLE
  `ifndef FPGA
  rrv64_fp_div_double FP_DIV_D(
    .rd(rd_div_d), .fstatus_dw(fstatus_dw_div_d), .is_div_0(fp_div_d_div_0), .complete(complete_div_d),
    .rs1(id2fp_div_d_ff.rs1), .rs2(id2fp_div_d_ff.rs2), .frm_dw(id2fp_div_d_ff.frm_dw),
    .start_pulse(fp_start_pulse), .rst, .clk(clkg_fp_div_d)
  );
  `else
  XIL_fp_div_double d_div_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_div_d_ff.rs1),
    .s_axis_b_tvalid(1'b1),
    .s_axis_b_tdata(id2fp_div_d_ff.rs2),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(rd_div_d),
    .m_axis_result_tuser({fp_div_d_div_0, fstatus_dw_div_d.invalid, fstatus_dw_div_d.huge, fstatus_dw_div_d.tiny})
    );
  assign fstatus_dw_div_d.compspecific = 1'b0;
  assign fstatus_dw_div_d.hugeint = 1'b0;
  assign fstatus_dw_div_d.inexact = 1'b0;
  assign fstatus_dw_div_d.infinity = 1'b0;
  assign fstatus_dw_div_d.zero = 1'b0;
  `endif
`endif
  // }}}

  // SQRT {{{
  rrv64_data_t        rd_sqrt_s, rd_sqrt_d;
  rrv64_fstatus_dw_t  fstatus_dw_sqrt_s, fstatus_dw_sqrt_d;
`ifndef FPGA
  rrv64_fp_sqrt_single FP_SQRT_S(
    .rd(rd_sqrt_s), .fstatus_dw(fstatus_dw_sqrt_s),
    .rs1(id2fp_sqrt_s_ff.rs1), .frm_dw(id2fp_sqrt_s_ff.frm_dw)
  );
`else
  XIL_fp_sqrt_single f_sqrt_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_sqrt_s_ff.rs1[31:0]),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(rd_sqrt_s[31:0]),
    .m_axis_result_tuser(fstatus_dw_sqrt_s.invalid)
    );
  assign fstatus_dw_sqrt_s.huge = 1'b0;
  assign fstatus_dw_sqrt_s.tiny = 1'b0;
  assign fstatus_dw_sqrt_s.compspecific = 1'b0;
  assign fstatus_dw_sqrt_s.hugeint = 1'b0;
  assign fstatus_dw_sqrt_s.inexact = 1'b0;
  assign fstatus_dw_sqrt_s.infinity = 1'b0;
  assign fstatus_dw_sqrt_s.zero = 1'b0;
`endif
`ifdef RRV64_SUPPORT_FP_DOUBLE
  `ifndef FPGA
  rrv64_fp_sqrt_double FP_SQRT_D(
    .rd(rd_sqrt_d), .fstatus_dw(fstatus_dw_sqrt_d),
    .rs1(id2fp_sqrt_d_ff.rs1), .frm_dw(id2fp_sqrt_d_ff.frm_dw)
  );
  `else
  XIL_fp_sqrt_double d_sqrt_u (
    .aclk(clk),
    .aclken(1'b1),
    .aresetn(~rst),
    .s_axis_a_tvalid(1'b1),
    .s_axis_a_tdata(id2fp_sqrt_d_ff.rs1[31:0]),
    .m_axis_result_tvalid(),
    .m_axis_result_tdata(rd_sqrt_d[31:0]),
    .m_axis_result_tuser(fstatus_dw_sqrt_d.invalid)
    );
  assign fstatus_dw_sqrt_d.huge = 1'b0;
  assign fstatus_dw_sqrt_d.tiny = 1'b0;
  assign fstatus_dw_sqrt_d.compspecific = 1'b0;
  assign fstatus_dw_sqrt_d.hugeint = 1'b0;
  assign fstatus_dw_sqrt_d.inexact = 1'b0;
  assign fstatus_dw_sqrt_d.infinity = 1'b0;
  assign fstatus_dw_sqrt_d.zero = 1'b0;
  `endif
`endif
  // }}}

  // MISC {{{
  rrv64_data_t        rd_sgnj, rd_cls, rd_i2s, rd_i2d, rd_s2i, rd_d2i, rd_s2d, rd_d2s;
  rrv64_fstatus_dw_t  fstatus_dw_i2s, fstatus_dw_i2d, fstatus_dw_s2i, fstatus_dw_d2i, fstatus_dw_s2d, fstatus_dw_d2s;
  rrv64_fflags_t      fflags_s2i, fflags_d2i;
  rrv64_fp_misc FP_MISC(
    .rs1(id2fp_misc_ff.rs1), .rs2(id2fp_misc_ff.rs2), .is_32(id2ex_ctrl_ff.is_32), .frm_dw(id2fp_misc_ff.frm_dw),
    .fp_sgnj_sel(id2ex_ctrl_ff.fp_sgnj_sel), .fp_cvt_sel(id2ex_ctrl_ff.fp_cvt_sel), .rstn(~rst),
    .*);
  // }}}

  rrv64_data_t  fp_rd;
  always_comb begin
    unique case (id2ex_ctrl_ff.fp_out_sel)
      RRV64_FP_OUT_SEL_ADD_S:   fp_rd = rd_add_s;
      RRV64_FP_OUT_SEL_CMP_S:   fp_rd = rd_cmp_s;
      RRV64_FP_OUT_SEL_MAC_S:   fp_rd = rd_mac_s;
      RRV64_FP_OUT_SEL_DIV_S:   fp_rd = rd_div_s;
      RRV64_FP_OUT_SEL_SQRT_S:  fp_rd = rd_sqrt_s;
      RRV64_FP_OUT_SEL_SGNJ:    fp_rd = rd_sgnj;
      RRV64_FP_OUT_SEL_CVT_I2S: fp_rd = rd_i2s;
      RRV64_FP_OUT_SEL_CVT_S2I: fp_rd = rd_s2i;
      RRV64_FP_OUT_SEL_CLS:     fp_rd = rd_cls;
`ifdef RRV64_SUPPORT_FP_DOUBLE
      RRV64_FP_OUT_SEL_ADD_D:   fp_rd = rd_add_d;
      RRV64_FP_OUT_SEL_CMP_D:   fp_rd = rd_cmp_d;
      RRV64_FP_OUT_SEL_MAC_D:   fp_rd = rd_mac_d;
      RRV64_FP_OUT_SEL_DIV_D:   fp_rd = rd_div_d;
      RRV64_FP_OUT_SEL_SQRT_D:  fp_rd = rd_sqrt_d;
      RRV64_FP_OUT_SEL_CVT_I2D: fp_rd = rd_i2d;
      RRV64_FP_OUT_SEL_CVT_D2I: fp_rd = rd_d2i;
      RRV64_FP_OUT_SEL_CVT_S2D: fp_rd = rd_s2d;
      RRV64_FP_OUT_SEL_CVT_D2S: fp_rd = rd_d2s;
`endif
      default:            fp_rd = 64'b0;
    endcase
  end
  
  always_comb
    unique case (id2ex_ctrl_ff.fp_out_sel)
      RRV64_FP_OUT_SEL_ADD_S:   ex2ma.fflags = func_fp_fflags(fstatus_dw_add_s);
      RRV64_FP_OUT_SEL_CMP_S:   ex2ma.fflags = fflags_cmp_s;
      RRV64_FP_OUT_SEL_MAC_S:   ex2ma.fflags = func_fp_fflags(fstatus_dw_mac_s);
      RRV64_FP_OUT_SEL_DIV_S: begin
        ex2ma.fflags = func_fp_fflags(fstatus_dw_div_s);
        ex2ma.fflags.dz = fp_div_s_div_0;
      end
      RRV64_FP_OUT_SEL_SQRT_S:  ex2ma.fflags = func_fp_fflags(fstatus_dw_sqrt_s);
      RRV64_FP_OUT_SEL_CVT_I2S: ex2ma.fflags = func_fp_fflags(fstatus_dw_i2s);
      RRV64_FP_OUT_SEL_CVT_S2I: ex2ma.fflags = fflags_s2i;
`ifdef RRV64_SUPPORT_FP_DOUBLE
      RRV64_FP_OUT_SEL_ADD_D:   ex2ma.fflags = func_fp_fflags(fstatus_dw_add_d);
      RRV64_FP_OUT_SEL_CMP_D:   ex2ma.fflags = fflags_cmp_d;
      RRV64_FP_OUT_SEL_MAC_D:   ex2ma.fflags = func_fp_fflags(fstatus_dw_mac_d);
      RRV64_FP_OUT_SEL_DIV_D: begin
        ex2ma.fflags = func_fp_fflags(fstatus_dw_div_d);
        ex2ma.fflags.dz = fp_div_d_div_0;
      end
      RRV64_FP_OUT_SEL_SQRT_D:  ex2ma.fflags = func_fp_fflags(fstatus_dw_sqrt_d);
      RRV64_FP_OUT_SEL_CVT_I2D: ex2ma.fflags = func_fp_fflags(fstatus_dw_i2d);
      RRV64_FP_OUT_SEL_CVT_D2I: ex2ma.fflags = fflags_d2i;
      RRV64_FP_OUT_SEL_CVT_S2D: ex2ma.fflags = func_fp_fflags(fstatus_dw_s2d);
      RRV64_FP_OUT_SEL_CVT_D2S: ex2ma.fflags = func_fp_fflags(fstatus_dw_d2s);
`endif
      default:            ex2ma.fflags = 5'b0;
    endcase
`else // RRV64_SUPPORT_FP
  logic wait_for_fp;
  assign wait_for_fp = 1'b0;
  assign ex2ma.fflags = 5'b0;

`endif // RRV64_SUPPORT_FP }}}

`ifdef RRV64_SUPPORT_FP // {{{
  rrv64_data_t    fp_mv;

  always_comb begin
    unique if(id2ex_ctrl_ff.is_32 == 1'b1) begin
      fp_mv = {{32{1'b1}},final_rs1[31:0]};
    end else begin
      fp_mv = final_rs1;
    end
  end
`endif // RRV64_SUPPORT_FP }}}

  //==========================================================
  // select ex_out {{{
  always_comb
    case (id2ex_ctrl_ff.ex_out_sel)
      RRV64_EX_OUT_SEL_RS1:       ex2ma.ex_out = final_rs1;
      RRV64_EX_OUT_SEL_PC_ADDER:  ex2ma.ex_out = pc_adder_out;
      `ifdef RRV64_SUPPORT_MULDIV
      RRV64_EX_OUT_SEL_MUL_L: ex2ma.ex_out = mul_rdl;
      RRV64_EX_OUT_SEL_MUL_H: ex2ma.ex_out = mul_rdh;
      RRV64_EX_OUT_SEL_DIV_Q: ex2ma.ex_out = div_rdq;
      RRV64_EX_OUT_SEL_DIV_R: ex2ma.ex_out = div_rdr;
      `endif // RRV64_SUPPORT_MULDIV
      `ifdef RRV64_SUPPORT_FP
      RRV64_EX_OUT_SEL_FP_MV: ex2ma.ex_out = fp_mv;
      RRV64_EX_OUT_SEL_FP:    ex2ma.ex_out = fp_rd;
      `endif // RRV64_SUPPORT_FP
      default:          ex2ma.ex_out = alu_out;
    endcase
  // }}}
  
  //==========================================================
  // bypass {{{
  assign ex2id_bps.valid_data = (id2ex_ctrl_ff.rd_avail == RRV64_RD_AVAIL_EX) & id2ex_ctrl_ff.rd_we & ex_valid;
  assign ex2id_bps.is_rd_fp = id2ex_ctrl_ff.is_rd_fp & id2ex_ctrl_ff.rd_we;
  assign ex2id_bps.rd_addr = ex2ma.rd_addr;
  assign ex2id_bps.valid_addr = {5{ex2ma_ctrl.rd_we & id_valid_ff & ~ex_kill}};
  assign ex2id_bps.rd = ex2ma.ex_out;

  assign ex2id_kill_addr = ex2ma.rd_addr & {5{ex2ma_ctrl.rd_we & id_valid_ff & ex_kill}};
  // }}}
  
  assign is_vcsr_addr = '0;
  assign ex2id_mask_vc_req = id_valid_ff & ~ex_kill & ((ex2ma_ctrl.csr_op != RRV64_CSR_OP_NONE) | (ex2dc_en) | (ex2ma_excp.valid));
  // }}}

  //==========================================================
  // decode for MA {{{

  // break-down inst

  always_comb
    func_break_inst(.opcode(opcode), .rd_addr(rd_addr), .rs1_addr(rs1_addr), .rs2_addr(rs2_addr), .rs3_addr(rs3_addr), .funct2(funct2), .funct3(funct3), .funct5(funct5), .funct6(funct6), .funct7(funct7), .funct12(funct12), .fmt(fmt), .i_imm(i_imm), .s_imm(s_imm), .b_imm(b_imm), .u_imm(u_imm), .j_imm(j_imm), .z_imm(z_imm), .frm(frm), .inst(id2ex_ff.inst));


  logic is_illegal_inst, is_ecall, is_ebreak;
  always_comb begin
    is_illegal_inst = 1'b0;
    is_ecall = 1'b0;
    is_ebreak = 1'b0;

    case (opcode)
      RRV64_AMO: begin
	case (funct7[3:2])
          2'b10: begin
            case(funct3)
              3'h2: func_rrv64_decode_ex2ma_lr_w(ex2ma_ctrl);
              3'h3: func_rrv64_decode_ex2ma_lr_d(ex2ma_ctrl);
              default: begin
                func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
                is_illegal_inst = 1'b1;
              end
            endcase
          end
          2'b11: begin
            case(funct3)
              3'h2: func_rrv64_decode_ex2ma_sc_w(ex2ma_ctrl);
              3'h3: func_rrv64_decode_ex2ma_sc_d(ex2ma_ctrl);
              default: begin
                func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
                is_illegal_inst = 1'b1;
              end
            endcase
          end
	  default: begin
	    func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
	  end
	endcase
      end
      RRV64_LOAD: // {{{
        case (funct3)
          3'h0: func_rrv64_decode_ex2ma_lb(ex2ma_ctrl);
          3'h4: func_rrv64_decode_ex2ma_lbu(ex2ma_ctrl);
          3'h1: func_rrv64_decode_ex2ma_lh(ex2ma_ctrl);
          3'h5: func_rrv64_decode_ex2ma_lhu(ex2ma_ctrl);
          3'h2: func_rrv64_decode_ex2ma_lw(ex2ma_ctrl);
          3'h6: func_rrv64_decode_ex2ma_lwu(ex2ma_ctrl);
          3'h3: func_rrv64_decode_ex2ma_ld(ex2ma_ctrl);
          default: begin
            func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
            is_illegal_inst = 1'b1;
          end
        endcase // }}}
      RRV64_STORE: // {{{
        case (funct3)
          3'h0: func_rrv64_decode_ex2ma_sb(ex2ma_ctrl);
          3'h1: func_rrv64_decode_ex2ma_sh(ex2ma_ctrl);
          3'h2: func_rrv64_decode_ex2ma_sw(ex2ma_ctrl);
          3'h3: func_rrv64_decode_ex2ma_sd(ex2ma_ctrl);
          default: begin
            func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
            is_illegal_inst = 1'b1;
          end
        endcase // }}}
      `ifdef RRV64_SUPPORT_FP
      RRV64_LOAD_FP: // {{{
        case (funct3)
          3'h2: func_rrv64_decode_ex2ma_flw(ex2ma_ctrl);
          3'h3: func_rrv64_decode_ex2ma_fld(ex2ma_ctrl);
          default: begin
            func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
            is_illegal_inst = 1'b1;
          end
        endcase
      // }}}
      RRV64_STORE_FP: // {{{
        case (funct3)
          3'h2: func_rrv64_decode_ex2ma_fsw(ex2ma_ctrl);
          3'h3: func_rrv64_decode_ex2ma_fsd(ex2ma_ctrl);
          default: begin
            func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
            is_illegal_inst = 1'b1;
          end
        endcase
      // }}}
      `endif
      RRV64_SYSTEM: // {{{
        case (funct3)
          3'h0: begin
            if ((funct7 == 7'h09) & (rd_addr == 5'h0)) begin
              if (do_trap_sfence) begin
                is_illegal_inst = 1'b1;
                func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
              end else begin
                func_rrv64_decode_ex2ma_sfence_vma(ex2ma_ctrl);
              end
            end else begin
              if ((rs1_addr == 5'h00) & (rd_addr == 5'h00)) begin
                case (funct12)
                  12'h000: begin
                    is_ecall = 1'b1;
                    func_rrv64_decode_ex2ma_ecall(ex2ma_ctrl);
                  end
                  12'h001: begin
                    is_ebreak = 1'b1;
                    func_rrv64_decode_ex2ma_ebreak(ex2ma_ctrl);
                  end
                  12'h105: begin
                    func_rrv64_decode_ex2ma_wfi(ex2ma_ctrl);
                  end
                  12'h106: begin
                    func_rrv64_decode_ex2ma_wfe(ex2ma_ctrl);
                  end
                  12'h002: func_rrv64_decode_ex2ma_uret(ex2ma_ctrl);
                  12'h102: begin
                    if (do_trap_sret) begin
                      is_illegal_inst = 1'b1;
                      func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
                    end else begin
                      func_rrv64_decode_ex2ma_sret(ex2ma_ctrl);
                    end
                  end
                  12'h302: begin
                    if (prv == RRV64_PRV_M) begin
                      func_rrv64_decode_ex2ma_mret(ex2ma_ctrl);
                    end else begin
                      func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
                      is_illegal_inst = 1'b1;
                    end
                  end
                  default: begin
                    func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
                    is_illegal_inst = 1'b1;
                  end 
                endcase
              end else begin
                func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
                is_illegal_inst = 1'b1;
              end
            end
          end
          3'h1: func_rrv64_decode_ex2ma_csrrw(ex2ma_ctrl);
          3'h2: func_rrv64_decode_ex2ma_csrrs(ex2ma_ctrl);
          3'h3: func_rrv64_decode_ex2ma_csrrc(ex2ma_ctrl);
          3'h5: func_rrv64_decode_ex2ma_csrrwi(ex2ma_ctrl);
          3'h6: func_rrv64_decode_ex2ma_csrrsi(ex2ma_ctrl);
          3'h7: func_rrv64_decode_ex2ma_csrrci(ex2ma_ctrl);
          default: begin
            func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
            is_illegal_inst = 1'b1;
          end
        endcase // }}}
      RRV64_MISC_MEM: begin // {{{
        case(funct3)
          3'h0: func_rrv64_decode_ex2ma_fence(ex2ma_ctrl);
          3'h1: func_rrv64_decode_ex2ma_fence_i(ex2ma_ctrl);
          default: begin
            func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
            is_illegal_inst = 1'b1;
          end
        endcase
      end
      // }}}
      default: begin
        func_rrv64_decode_ex2ma_default(ex2ma_ctrl);
      end
    endcase

    // copy from ID ctrl
    ex2ma_ctrl.is_rd_fp = id2ex_ctrl_ff.is_rd_fp;
    ex2ma_ctrl.rd_we = id2ex_ctrl_ff.rd_we;
    ex2ma_ctrl.rd_avail = id2ex_ctrl_ff.rd_avail;
    ex2ma_ctrl.is_lr = id2ex_ctrl_ff.is_lr;
    ex2ma_ctrl.is_sc = id2ex_ctrl_ff.is_sc;
    ex2ma_ctrl.is_aq_rl = id2ex_ctrl_ff.is_aq_rl;
    ex2ma_ctrl.is_amo_load = id2ex_ctrl_ff.is_amo_load;
    ex2ma_ctrl.is_amo_op = id2ex_ctrl_ff.is_amo_op;
    ex2ma_ctrl.is_amo_store = id2ex_ctrl_ff.is_amo_store;
    ex2ma_ctrl.is_amo = id2ex_ctrl_ff.is_amo;
    ex2ma_ctrl.is_amo_done = id2ex_ctrl_ff.is_amo_done;
    ex2ma_ctrl.is_fp_32 = id2ex_ctrl_ff.is_fp_32;
    `ifndef SYNTHESIS
      if (ex2ma_ctrl.inst_code == "default")
        ex2ma_ctrl.inst_code = id2ex_ctrl_ff.inst_code;
    `endif
  end

  // }}}
  
  //==========================================================
  // prepare to read/write L1D$ {{{
  rrv64_ex2dc_t ex2dc_saved_ff; // hold the address while rmiss/wmiss
  rrv64_vaddr_t ex2dc_addr_mux;
  assign hold_ex2dc = ex2dc_en_ff & ~dc2ma.valid;

  always_ff @ (posedge clk)
    if (rst) begin
      hold_ex2dc_ff <= '0;
      ex2dc_saved_ff.re <= '0;
      ex2dc_saved_ff.we <= '0;
      ex2dc_en_ff <= '0;
      ex2dc_saved_ff.lr <= '0;
      ex2dc_saved_ff.sc <= '0;
      ex2dc_saved_ff.aq_rl <= '0;
      ex2dc_saved_ff.amo_load <= '0;
      ex2dc_saved_ff.amo_store <= '0;
    end else begin
      ex2dc_en_ff <= ex2dc_en;
      ex2dc_saved_ff.re <= ex2dc.re;
      ex2dc_saved_ff.we <= ex2dc.we;
      ex2dc_saved_ff.lr <= ex2dc.lr;
      ex2dc_saved_ff.sc <= ex2dc.sc;
      ex2dc_saved_ff.amo_load <= ex2dc.amo_load;
      ex2dc_saved_ff.amo_store <= ex2dc.amo_store;
      ex2dc_saved_ff.aq_rl <= ex2dc.aq_rl;
      hold_ex2dc_ff <= hold_ex2dc;
    end

  always_ff @(posedge ex2dc_clkg) begin
    if (ex2dc_en) begin
      ex2dc_saved_ff.addr <= ex2dc.addr;
      ex2dc_saved_ff.wdata <= ex2dc.wdata;
      ex2dc_saved_ff.mask <= ex2dc.mask;
      ex2dc_saved_ff.width <= ex2dc.width;
    end
  end

  assign ex2dc_en = ex2dc.re | ex2dc.we | ex2dc.aq_rl;

  //------------------------------------------------------
  // write {{{

  // write en

  // write addr
  rrv64_vaddr_t waddr;
  rrv64_data_t 	ex2dc_amo_wdata;
  assign  waddr = ex2ma.ex_out;

  // write data
  rrv64_data_t  wdata;

  logic [7:0] wbyte0, wbyte1, wbyte2, wbyte3;
  assign  wbyte0 = id2ex_ctrl_ff.is_amo_store ? ex2dc_amo_wdata[7:0]: final_rs2[ 7: 0];
  assign  wbyte1 = id2ex_ctrl_ff.is_amo_store ? ex2dc_amo_wdata[15:8]: final_rs2[15: 8];
  assign  wbyte2 = id2ex_ctrl_ff.is_amo_store ? ex2dc_amo_wdata[23:16]: final_rs2[23:16];
  assign  wbyte3 = id2ex_ctrl_ff.is_amo_store ? ex2dc_amo_wdata[31:24]: final_rs2[31:24];

  always_comb begin
    case (ex2ma_ctrl.ma_byte_sel)
      RRV64_MA_BYTE_SEL_1:
        case (ex2dc_addr_mux[2:0])
          3'b001:   wdata = {48'b0,  wbyte0,    8'b0};
          3'b010:   wdata = {40'b0,  wbyte0,   16'b0};
          3'b011:   wdata = {32'b0,  wbyte0,   24'b0};
          3'b100:   wdata = {24'b0,  wbyte0,   32'b0};
          3'b101:   wdata = {16'b0,  wbyte0,   40'b0};
          3'b110:   wdata = { 8'b0,  wbyte0,   48'b0};
          3'b111:   wdata = {wbyte0, 56'b0          };
          default:  wdata = {56'b0,           wbyte0};
        endcase
      RRV64_MA_BYTE_SEL_2:
        case (ex2dc_addr_mux[2:0])
          3'b000: wdata = {48'b0,  wbyte1, wbyte0};
          3'b010: wdata = {32'b0,  wbyte1, wbyte0, 16'b0};
          3'b100: wdata = {16'b0,  wbyte1, wbyte0, 32'b0};
          3'b110: wdata = {wbyte1, wbyte0, 48'b0};
          default: begin
            wdata = 64'b0;
          end
        endcase
      RRV64_MA_BYTE_SEL_4:
        case (ex2dc_addr_mux[2:0])
          3'b000: wdata = {32'b0, wbyte3, wbyte2, wbyte1, wbyte0};
          3'b100: wdata = {wbyte3, wbyte2, wbyte1, wbyte0, 32'b0};
          default: begin
            wdata = 64'b0;
          end
        endcase
      RRV64_MA_BYTE_SEL_8:
        case (ex2dc_addr_mux[2:0])
          3'b000: wdata = id2ex_ctrl_ff.is_amo_store ? ex2dc_amo_wdata: final_rs2;
          default: begin
            wdata = 64'b0;
          end
        endcase
      default:
        wdata = 64'b0;
    endcase
  end

  cpu_byte_mask_t width;

  // write mask
  logic [7:0] wmask;
  always_comb begin
    case (ex2ma_ctrl.ma_byte_sel)
      RRV64_MA_BYTE_SEL_1: begin
        case (ex2dc_addr_mux[2:0])
          3'b001:   wmask = 8'b00000010; 
          3'b010:   wmask = 8'b00000100;
          3'b011:   wmask = 8'b00001000;
          3'b100:   wmask = 8'b00010000;
          3'b101:   wmask = 8'b00100000;
          3'b110:   wmask = 8'b01000000;
          3'b111:   wmask = 8'b10000000;
          default:  wmask = 8'b00000001;
        endcase
      end
      RRV64_MA_BYTE_SEL_2: begin
        case (ex2dc_addr_mux[2:0])
          3'b000: wmask = 8'b00000011;
          3'b010: wmask = 8'b00001100;
          3'b100: wmask = 8'b00110000;
          3'b110: wmask = 8'b11000000;
          default: begin
            wmask = 8'b00000000;
          end
        endcase
      end
      RRV64_MA_BYTE_SEL_4: begin
        case (ex2dc_addr_mux[2:0])
          3'b000: wmask = 8'b00001111;
          3'b100: wmask = 8'b11110000;
          default: begin
            wmask = 8'b00000000;
          end
        endcase
      end
      RRV64_MA_BYTE_SEL_8: begin
        wmask = 8'b11111111;
      end
      default: begin
        wmask = 8'b00000000;
      end
    endcase
  end

  always_comb begin
    case (ex2ma_ctrl.ma_byte_sel)
      RRV64_MA_BYTE_SEL_1, RRV64_MA_BYTE_SEL_U1:  width = 'h1;
      RRV64_MA_BYTE_SEL_2, RRV64_MA_BYTE_SEL_U2:  width = 'h2;
      RRV64_MA_BYTE_SEL_4, RRV64_MA_BYTE_SEL_U4:  width = 'h4;
      RRV64_MA_BYTE_SEL_8:                        width = 'h8;
      default:                                    width = 'h0;
    endcase
  end
  // }}}

  rrv64_vaddr_t rff_amo_ld_addr;

  always_ff @ (posedge amo_wdata_clkg) begin
    if (en_amo_wdata) begin
      ex2dc_amo_wdata <= alu_out;
    end
  end

  always_ff @ (posedge amo_addr_clkg) begin
    if (en_amo_addr) begin
      rff_amo_ld_addr <= ex2dc.addr;
    end
  end

  assign  ex2dc.we = hold_ex2dc ? ex2dc_saved_ff.we : ex2ma_ctrl.is_store & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.re = hold_ex2dc ? ex2dc_saved_ff.re : ex2ma_ctrl.is_load & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.lr = hold_ex2dc ? ex2dc_saved_ff.lr : ex2ma_ctrl.is_lr & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.sc = hold_ex2dc ? ex2dc_saved_ff.sc : ex2ma_ctrl.is_sc & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.aq_rl = hold_ex2dc ? ex2dc_saved_ff.aq_rl : ex2ma_ctrl.is_aq_rl & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.amo_load = hold_ex2dc ? ex2dc_saved_ff.amo_load : ex2ma_ctrl.is_amo_load & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.amo_store = hold_ex2dc ? ex2dc_saved_ff.amo_store : ex2ma_ctrl.is_amo_store & ex_valid & ~ex2ma_excp.valid & ma_ready & ~ex_stall;
  assign  ex2dc.width = hold_ex2dc ? ex2dc_saved_ff.width : width;

  always_comb begin
    if (id2ex_ctrl_ff.is_amo_store) begin
      ex2dc_addr_mux = rff_amo_ld_addr;
    end else begin
      ex2dc_addr_mux = alu_out;
    end
  end

  assign  ex2dc.addr = hold_ex2dc ? ex2dc_saved_ff.addr : ex2dc_addr_mux;
  assign  ex2dc.wdata = hold_ex2dc ? ex2dc_saved_ff.wdata : wdata;
  assign  ex2dc.mask = hold_ex2dc ? ex2dc_saved_ff.mask : wmask;
  // }}}

  //------------------------------------------------------
  // address misaligned exception {{{
  logic is_addr_misaligned;

  always_comb begin
    is_addr_misaligned = 1'b0;
    case (ex2ma_ctrl.ma_byte_sel)
      RRV64_MA_BYTE_SEL_2, RRV64_MA_BYTE_SEL_U2:
        if (ex2dc_addr_mux[0] != 1'b0) begin
          is_addr_misaligned = 1'b1;
        end
      RRV64_MA_BYTE_SEL_4, RRV64_MA_BYTE_SEL_U4:
        if (ex2dc_addr_mux[1:0] != 2'b00) begin
          is_addr_misaligned = 1'b1;
        end
      RRV64_MA_BYTE_SEL_8:
        if (ex2dc_addr_mux[2:0] != 3'b000) begin
          is_addr_misaligned = 1'b1;
        end
    endcase
  end
  // }}}
  
  //==========================================================
  // exception {{{
  logic is_kill;
  always_comb begin
    is_kill = '0;
    ex2ma_excp.inst = id2ex_excp_ff.inst;
    if (ex_kill) begin
      ex2ma_excp.valid = 1'b0;
      ex2ma_excp.is_half1 = 1'b0;
      ex2ma_excp.cause = RRV64_EXCP_CAUSE_NONE;
      is_kill = '1;
    end else begin
      case (1'b1)
        (id2ex_excp_ff.valid): begin
          ex2ma_excp = id2ex_excp_ff;
          is_kill = '1;
        end
        (is_illegal_inst | is_addr_misaligned): begin
          ex2ma_excp.valid = 1'b1;
          ex2ma_excp.is_half1 = 1'b0;
          ex2ma_excp.cause = is_addr_misaligned ? (ex2ma_ctrl.is_store ? RRV64_EXCP_CAUSE_STORE_ADDR_MISALIGNED: RRV64_EXCP_CAUSE_LOAD_ADDR_MISALIGNED) : RRV64_EXCP_CAUSE_ILLEGAL_INST;
          is_kill = '1;
        end
        is_ecall: begin
          ex2ma_excp.valid = 1'b1;
          ex2ma_excp.is_half1 = 1'b0;
          case (prv)
            RRV64_PRV_U: ex2ma_excp.cause = RRV64_EXCP_CAUSE_ECALL_FROM_U;
            RRV64_PRV_S: ex2ma_excp.cause = RRV64_EXCP_CAUSE_ECALL_FROM_S;
            RRV64_PRV_M: ex2ma_excp.cause = RRV64_EXCP_CAUSE_ECALL_FROM_M;
            default: ex2ma_excp.cause = RRV64_EXCP_CAUSE_ILLEGAL_INST;
          endcase
        end
        is_ebreak: begin
          ex2ma_excp.valid = 1'b1;
          ex2ma_excp.is_half1 = 1'b0;
          ex2ma_excp.cause = RRV64_EXCP_CAUSE_BREAKPOINT;
        end
        is_misaligned_if_addr: begin
          ex2ma_excp.valid = 1'b1;
          ex2ma_excp.is_half1 = 1'b0;
          ex2ma_excp.cause = RRV64_EXCP_CAUSE_INST_ADDR_MISALIGNED;
        end
        default: begin
          ex2ma_excp.valid = 1'b0;
          ex2ma_excp.is_half1 = 1'b0;
          ex2ma_excp.cause = id2ex_excp_ff.cause;
        end
      endcase
    end
  end
  // }}}

  //==========================================================
  // pipeline control {{{
  assign wait_for_ex = wait_for_mul | wait_for_div | wait_for_fp | hold_ex2if_kill_ff;
  assign ex_valid = ~ex_kill & id_valid_ff & ~wait_for_ex & ~ex_wait_for_reg;
  assign ex_ready = (ma_ready | ~ex_valid) & ~wait_for_ex & ~ex_wait_for_reg;

  assign ex_is_stable_stall = ~ex_valid & ex_ready;

  assign ex2da.wait_for_reg = ex_wait_for_reg;
  assign ex2da.wait_for_mul = wait_for_mul;
  assign ex2da.wait_for_div = wait_for_div;
  assign ex2da.wait_for_fp = wait_for_fp;
  assign ex2da.hold_ex2if_kill = hold_ex2if_kill_ff;

  always_ff @ (posedge clk)
    if (rst)
      ex_valid_ff <= 1'b0;
    else if (ma_ready)
      ex_valid_ff <= ex_valid;
  `ifndef SYNTHESIS
    logic [63:0] ex_status;
    always_comb
      case ({ex_valid, ex_ready})
        2'b00: ex_status = "BUSY";
        2'b01: ex_status = "IDLE";
        2'b10: ex_status = "BLOCK";
        2'b11: ex_status = "DONE";
        default: ex_status = 64'bx;
      endcase
  `endif
  // }}}

  //==========================================================
  // ITB Stack trace {{{

  assign ex2ma_itb_data.push_to_stack = rff_id2ex_itb_data.push_to_stack | ((opcode == RRV64_JALR) & (rs1_addr != 5'h01) & (rd_addr != 5'h00));
  assign ex2ma_itb_data.data.vpc = rff_id2ex_itb_data.push_to_stack ? rff_id2ex_itb_data.data.vpc: jump_pc;

  // }}}

  //==========================================================
  // output {{{
  assign ex2ma.pc = id2ex_ff.pc;
  assign ex2ma.inst = id2ex_ff.inst;
  assign ex2ma.rd_addr = rd_addr;
  assign ex2ma.is_rvc = id2ex_ff.is_rvc;

  assign ex_pc = ex2ma.pc;

  assign ex2ma.csr_addr = rrv64_csr_addr_t'(funct12);

  always_ff @ (posedge clk) begin
//     if (rst | ((ex_kill | ex2ma_excp.valid) & ma_ready)) begin
    if (rst | (is_kill & ma_ready)) begin
      ex2ma_ctrl_ff.is_rd_fp <= 1'b0;
      ex2ma_ctrl_ff.rd_we <= 1'b0;
      ex2ma_ctrl_ff.rd_avail <= RRV64_RD_AVAIL_EX;
      ex2ma_ctrl_ff.is_load <= 1'b0;
      ex2ma_ctrl_ff.is_store <= 1'b0;
      ex2ma_ctrl_ff.fence_type <= RRV64_FENCE_TYPE_NONE;
      ex2ma_ctrl_ff.rd_sel <= RRV64_RD_SEL_EX_OUT;
      ex2ma_ctrl_ff.ma_byte_sel <= RRV64_MA_BYTE_SEL_8;
      ex2ma_ctrl_ff.csr_op <= RRV64_CSR_OP_NONE;
      ex2ma_ctrl_ff.ret_type <= RRV64_RET_TYPE_NONE;
      ex2ma_ctrl_ff.is_wfi <= 1'b0;
      ex2ma_ctrl_ff.is_wfe <= 1'b0;
      rff_ex2ma_itb_data.push_to_stack <= 1'b0;
`ifndef SYNTHESIS
        ex2ma_ctrl_ff.inst_code <= "none";
`endif
    end else if (ma_ready & ex_valid) begin
      ex2ma_ctrl_ff <= ex2ma_ctrl;
      rff_ex2ma_itb_data <= ex2ma_itb_data;
    end

    if (ex_valid & ma_ready) begin
      ex2ma_ff <= ex2ma;
    end

    if (rst | (ex_kill & ma_ready)) begin
      ex2ma_excp_ff.valid <= 1'b0;
      ex2ma_excp_ff.is_half1 <= 1'b0;
      ex2ma_excp_ff.cause <= RRV64_EXCP_CAUSE_NONE;
    end else if (ma_ready & ex_valid) begin
      ex2ma_excp_ff <= ex2ma_excp;
    end
  end
  // }}}

`ifndef SYNTHESIS
`ifndef VERILATOR
//  logic [63:0] cnt, mill;
//  assign mill = 64'd1000000;
//  always_ff @(posedge clk) begin
//    if (rst) begin
//      cnt <= '0;
//    end else begin
//      cnt <= cnt + ((ex_valid & ma_ready) ? 64'd1: 64'd0);
//    end
//  end
//
//  always_ff @(posedge clk) begin
//    if (~rst) begin
//      if (((cnt % mill) == '0) & ex_valid & ma_ready) begin
//        $display("%t instret%d=%1d", $time, core_id, cnt);
//      end
//    end 
//  end
//
  string filename;
  int pc_file;
  logic pc_print_flag;
  logic inst_print_flag;

  initial begin
    if ($test$plusargs("print_rrv_pc")) begin
      #1;
      filename = $sformatf("core%0d.log", core_id);
      pc_file = $fopen(filename, "w");
      pc_print_flag = 1'b1;
    end else begin
      pc_print_flag = 1'b0;
    end
    if ($test$plusargs("print_rrv_inst")) begin
      inst_print_flag = 1'b1;
    end else begin
      inst_print_flag = 1'b0;
    end
  end

  always_ff @ (posedge clk) begin
    if (pc_print_flag) begin
      if (~rst) begin
        if (ex_valid & ma_ready & (~id2ex_ctrl_ff.is_amo | id2ex_ctrl_ff.is_amo_done)) begin
          if (inst_print_flag) begin
            $fdisplay(pc_file, "%t PC=%h, INST=%h", $time, id2ex_ff.pc, id2ex_ff.inst);
          end else begin
            $fdisplay(pc_file, "%t PC=%h", $time, id2ex_ff.pc);
          end
        end
      end
    end
  end
`endif
`endif
endmodule
