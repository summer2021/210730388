// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*************************************************************************
 * Ring arbiter for multicore
 **************************************************************************/
module rrv64_ring_if_arbiter
  import soc_cfg::*;
  import soc_typedef::*;
  import soc_intf_typedef::*;
(
  // icache
  input ring_req_if_ar_t ic_req_if_ar, 
  input logic ic_req_if_awvalid, 
  output logic ic_req_if_awready, 
  input logic ic_req_if_wvalid, 
  output logic ic_req_if_wready, 
  input logic ic_req_if_arvalid, 
  output logic ic_req_if_arready, 
  input ring_req_if_w_t ic_req_if_w, 
  input ring_req_if_aw_t ic_req_if_aw,
  output ring_resp_if_b_t ic_resp_ppln_if_b, 
  output ring_resp_if_r_t ic_resp_ppln_if_r, 
  output logic ic_resp_ppln_if_rvalid, 
  input logic ic_resp_ppln_if_rready, 
  output logic ic_resp_ppln_if_bvalid, 
  input logic ic_resp_ppln_if_bready,
  // dcache
  input ring_req_if_ar_t dc_req_if_ar, 
  input logic dc_req_if_awvalid, 
  output logic dc_req_if_awready, 
  input logic dc_req_if_wvalid, 
  output logic dc_req_if_wready, 
  input logic dc_req_if_arvalid, 
  output logic dc_req_if_arready, 
  input ring_req_if_w_t dc_req_if_w, 
  input ring_req_if_aw_t dc_req_if_aw,
  output ring_resp_if_b_t dc_resp_ppln_if_b, 
  output ring_resp_if_r_t dc_resp_ppln_if_r, 
  output logic dc_resp_ppln_if_rvalid, 
  input logic dc_resp_ppln_if_rready, 
  output logic dc_resp_ppln_if_bvalid, 
  input logic dc_resp_ppln_if_bready,
  // to port
  output ring_req_if_ar_t req_ppln_if_ar, 
  output logic req_ppln_if_awvalid, 
  input logic req_ppln_if_awready, 
  output logic req_ppln_if_wvalid, 
  input logic req_ppln_if_wready, 
  output logic req_ppln_if_arvalid, 
  input logic req_ppln_if_arready, 
  output ring_req_if_w_t req_ppln_if_w, 
  output ring_req_if_aw_t req_ppln_if_aw,
  input ring_resp_if_b_t resp_if_b, 
  input ring_resp_if_r_t resp_if_r, 
  input logic resp_if_rvalid, 
  output logic resp_if_rready, 
  input logic resp_if_bvalid, 
  output logic resp_if_bready,
  // rst & clk
  input   logic           rstn, clk
);
  // AW+W (Icache won't send out writes)
  assign req_ppln_if_awvalid = dc_req_if_awvalid;
  assign req_ppln_if_wvalid = dc_req_if_wvalid;
  assign req_ppln_if_aw = dc_req_if_aw;
  assign req_ppln_if_w = dc_req_if_w;
  assign dc_req_if_awready = req_ppln_if_awready;
  assign dc_req_if_wready = req_ppln_if_wready;

  assign ic_req_if_awready = '0;
  assign ic_req_if_wready = '0;
  assign ic_resp_ppln_if_bvalid = '0;

  // AR
  logic [1:0] arvalid;
  logic [1:0] arready;

  assign arvalid[0] = dc_req_if_arvalid;
  assign arvalid[1] = ic_req_if_arvalid;

  vld_rdy_rr_arb #(
  .N_INPUT(2)
  ) sysbus_ar_arb (
    .vld(arvalid),
    .rdy(req_ppln_if_arready),
    .grt(arready),
    .rstn, 
    .clk
  );

  always_comb begin
    if (arready[0]) begin
      req_ppln_if_ar = dc_req_if_ar;
      req_ppln_if_ar.arid[4] = 1'b0;
    end else begin
      req_ppln_if_ar = ic_req_if_ar;
      req_ppln_if_ar.arid[4] = 1'b1;
    end
  end

  assign req_ppln_if_arvalid = |arvalid;

  assign dc_req_if_arready = arready[0] & req_ppln_if_arready;
  assign ic_req_if_arready = arready[1] & req_ppln_if_arready;

  always_comb begin
    // R: use rid[4] to select icache and dcache
    ic_resp_ppln_if_r.rid = resp_if_r.rid;
    ic_resp_ppln_if_r.rdata = resp_if_r.rdata;
    ic_resp_ppln_if_r.rresp = resp_if_r.rresp;
    ic_resp_ppln_if_r.rlast = resp_if_r.rlast;
    dc_resp_ppln_if_r.rid = resp_if_r.rid;
    dc_resp_ppln_if_r.rdata = resp_if_r.rdata;
    dc_resp_ppln_if_r.rresp = resp_if_r.rresp;
    dc_resp_ppln_if_r.rlast = resp_if_r.rlast;
    dc_resp_ppln_if_rvalid = resp_if_rvalid & ~resp_if_r.rid[4];
    ic_resp_ppln_if_rvalid = resp_if_rvalid & resp_if_r.rid[4];
    resp_if_rready = resp_if_rvalid ? (resp_if_r.rid[4] ? ic_resp_ppln_if_rready : dc_resp_ppln_if_rready): '0;
    // B: only dcache
    dc_resp_ppln_if_bvalid = resp_if_bvalid;
    dc_resp_ppln_if_b.bid = resp_if_b.bid;
    dc_resp_ppln_if_b.bresp = resp_if_b.bresp;
    resp_if_bready = dc_resp_ppln_if_bready;
  end

endmodule
