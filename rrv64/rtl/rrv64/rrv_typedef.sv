// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

`ifndef __RRV_TYPEDEF__SV__
`define __RRV_TYPEDEF__SV__

package rrv_typedef;
  import soc_cfg::*;
  import soc_typedef::*;
  import rrv_cfg::*;

  typedef logic [XLEN-1:0]    data_t; // data
  typedef logic [31:0]        inst_t; // instruction
  typedef logic [REG_ADDR_WIDTH-1:0]         reg_addr_t; // register file address
  typedef logic [XLEN/8-1:0]  data_byte_mask_t;

  typedef logic [55:0]        cntr_t; // 2^56 / 1GHz > 2 years

  //==========================================================
  // enumeration
  typedef enum logic { // {{{
    RD_AVAIL_EX,
    RD_AVAIL_MA
    // }}}
  } rd_avail_t;
  typedef enum logic [4:0] { // {{{
    LOAD      = 5'h00,
    MISC_MEM  = 5'h03,
    OP_IMM    = 5'h04,
    AUIPC     = 5'h05,
    OP_IMM_32 = 5'h06,
    STORE     = 5'h08,
    OP        = 5'h0C,
    LUI       = 5'h0D,
    OP_32     = 5'h0E,
    BRANCH    = 5'h18,
    JALR      = 5'h19,
    JAL       = 5'h1B,
    SYSTEM    = 5'h1C,
    LOAD_FP   = 5'h01,
    STORE_FP  = 5'h09,
    OP_FP     = 5'h14,
    FMADD     = 5'h10,
    FMSUB     = 5'h11,
    FNMADD    = 5'h13,
    FNMSUB    = 5'h12,
    OURS      = 5'h1E
  } opcode_t;
  typedef enum logic [3:0] { // {{{
    ALU_OP_ADD  = 4'h0,
    ALU_OP_SLL  = 4'h1,
    ALU_OP_SEQ  = 4'h2,
    ALU_OP_SNE  = 4'h3,
    ALU_OP_XOR  = 4'h4,
    ALU_OP_SRL  = 4'h5,
    ALU_OP_OR   = 4'h6,
    ALU_OP_AND  = 4'h7,
    ALU_OP_SUB  = 4'hA,
    ALU_OP_SRA  = 4'hB,
    ALU_OP_SLT  = 4'hC,
    ALU_OP_SGE  = 4'hD,
    ALU_OP_SLTU = 4'hE,
    ALU_OP_SGEU = 4'hF
    // }}}
  } alu_op_t;
  typedef enum logic [1:0] { // {{{
    OP1_SEL_RS1,
    OP1_SEL_FP_RS1,
    OP1_SEL_FP_RS1_SEXT,
    OP1_SEL_ZERO // }}}
  } op1_sel_t;
  typedef enum logic [1:0] { // {{{
    OP2_SEL_RS2,
    OP2_SEL_ZERO,
    OP2_SEL_IMM // }}}
  } op2_sel_t;
  typedef enum logic [4:0] { // {{{
    IMM_SEL_I,
    IMM_SEL_S,
    IMM_SEL_B,
    IMM_SEL_U,
    IMM_SEL_J,
    IMM_SEL_Z,
    IMM_SEL_F0,
    IMM_SEL_ZERO // }}}
  } imm_sel_t;
  typedef enum logic [1:0] { // {{{
    EX_PC_ADDER_SEL_P4,
    EX_PC_ADDER_SEL_P2,
    EX_PC_ADDER_SEL_IMM // }}}
  } ex_pc_adder_sel_t;
  typedef enum logic [1:0] { // {{{
    EX_NEXT_PC_SEL_PC_ADDER,
    EX_NEXT_PC_SEL_RS1,
    EX_NEXT_PC_SEL_NONE // }}}
  } ex_next_pc_sel_t;
  typedef enum logic [3:0] { // {{{
    EX_OUT_SEL_RS1,
    EX_OUT_SEL_ALU_OUT,
    EX_OUT_SEL_PC_ADDER,
    EX_OUT_SEL_MUL_L,
    EX_OUT_SEL_MUL_H,
    EX_OUT_SEL_DIV_Q,
    EX_OUT_SEL_DIV_R,
    EX_OUT_SEL_FP,
    EX_OUT_SEL_ACC_LO,
    EX_OUT_SEL_ACC_HI,
    EX_OUT_SEL_NONE // }}}
  } ex_out_sel_t;
  typedef enum logic [2:0] { // {{{
    MA_BYTE_SEL_1   = 3'h0,
    MA_BYTE_SEL_U1  = 3'h1,
    MA_BYTE_SEL_2   = 3'h2,
    MA_BYTE_SEL_U2  = 3'h3,
    MA_BYTE_SEL_4   = 3'h4,
    MA_BYTE_SEL_U4  = 3'h5,
    MA_BYTE_SEL_8   = 3'h6
    // }}}
  } ma_byte_sel_t;
  typedef enum logic [2:0] { // {{{
    MUL_TYPE_L,
    MUL_TYPE_HSS,
    MUL_TYPE_HUU,
    MUL_TYPE_HSU,
    MUL_TYPE_W,
    MUL_TYPE_NONE // }}}
  } mul_type_t;
  typedef enum logic [3:0] { // {{{
    DIV_TYPE_Q,
    DIV_TYPE_R,
    DIV_TYPE_QU,
    DIV_TYPE_RU,
    DIV_TYPE_QW,
    DIV_TYPE_RW,
    DIV_TYPE_QUW,
    DIV_TYPE_RUW,
    DIV_TYPE_NONE // }}}
  } div_type_t;
  typedef enum logic [1:0] { // {{{
    RD_SEL_EX_OUT,
    RD_SEL_MEM_READ,
    RD_SEL_CSR_READ // }}}
  } rd_sel_t;
  typedef enum logic [1:0] { // {{{
    RET_TYPE_U,
    RET_TYPE_S,
    RET_TYPE_M,
    RET_TYPE_NONE // }}}
  } ret_type_t;
  typedef enum logic [11:0] { // {{{
    // non-isa defined CSR  {{{
`ifdef RRV_SUPPORT_DMA
    CSR_ADDR_DMA_DONE         = 12'h602,
`endif

`ifdef RRV_SUPPORT_UART
    CSR_ADDR_TO_UART          = 12'h610,
    CSR_ADDR_FROM_UART        = 12'h611,
`endif

    // }}}
    // isa defined CSR {{{
    CSR_ADDR_USTATUS         = 12'h000,
    CSR_ADDR_UIE             = 12'h004,
    CSR_ADDR_UTVEC           = 12'h005,
    CSR_ADDR_USCRATCH        = 12'h040,
    CSR_ADDR_UEPC            = 12'h041,
    CSR_ADDR_UCAUSE          = 12'h042,
    CSR_ADDR_UTVAL           = 12'h043,
    CSR_ADDR_UIP             = 12'h044,
    CSR_ADDR_FFLAGS          = 12'h001,
    CSR_ADDR_FRM             = 12'h002,
    CSR_ADDR_FCSR            = 12'h003,
    CSR_ADDR_CYCLE           = 12'hC00,
    CSR_ADDR_TIME            = 12'hC01,
    CSR_ADDR_INSTRET         = 12'hC02,
    CSR_ADDR_HPMCOUNTER3     = 12'hC03,
    CSR_ADDR_HPMCOUNTER4     = 12'hC04,
    CSR_ADDR_HPMCOUNTER5     = 12'hC05,
    CSR_ADDR_HPMCOUNTER6     = 12'hC06,
    CSR_ADDR_HPMCOUNTER7     = 12'hC07,
    CSR_ADDR_HPMCOUNTER8     = 12'hC08,
    CSR_ADDR_HPMCOUNTER9     = 12'hC09,
    CSR_ADDR_HPMCOUNTER10    = 12'hC0A,
    CSR_ADDR_HPMCOUNTER11    = 12'hC0B,
    CSR_ADDR_HPMCOUNTER12    = 12'hC0C,
    CSR_ADDR_HPMCOUNTER13    = 12'hC0D,
    CSR_ADDR_HPMCOUNTER14    = 12'hC0E,
    CSR_ADDR_HPMCOUNTER15    = 12'hC0F,
    CSR_ADDR_HPMCOUNTER16    = 12'hC10,
    CSR_ADDR_HPMCOUNTER17    = 12'hC11,
    CSR_ADDR_HPMCOUNTER18    = 12'hC12,
    CSR_ADDR_HPMCOUNTER19    = 12'hC13,
    CSR_ADDR_HPMCOUNTER20    = 12'hC14,
    CSR_ADDR_HPMCOUNTER21    = 12'hC15,
    CSR_ADDR_HPMCOUNTER22    = 12'hC16,
    CSR_ADDR_HPMCOUNTER23    = 12'hC17,
    CSR_ADDR_HPMCOUNTER24    = 12'hC18,
    CSR_ADDR_HPMCOUNTER25    = 12'hC19,
    CSR_ADDR_HPMCOUNTER26    = 12'hC1A,
    CSR_ADDR_HPMCOUNTER27    = 12'hC1B,
    CSR_ADDR_HPMCOUNTER28    = 12'hC1C,
    CSR_ADDR_HPMCOUNTER29    = 12'hC1D,
    CSR_ADDR_HPMCOUNTER30    = 12'hC1E,
    CSR_ADDR_HPMCOUNTER31    = 12'hC1F,
    CSR_ADDR_SSTATUS         = 12'h100,
    CSR_ADDR_SEDELEG         = 12'h102,
    CSR_ADDR_SIDELEG         = 12'h103,
    CSR_ADDR_SIE             = 12'h104,
    CSR_ADDR_STVEC           = 12'h105,
    CSR_ADDR_SCOUNTEREN      = 12'h106,
    CSR_ADDR_SSCRATCH        = 12'h140,
    CSR_ADDR_SEPC            = 12'h141,
    CSR_ADDR_SCAUSE          = 12'h142,
    CSR_ADDR_STVAL           = 12'h143,
    CSR_ADDR_SIP             = 12'h144,
    CSR_ADDR_SATP            = 12'h180,
    CSR_ADDR_MVENDORID       = 12'hF11,
    CSR_ADDR_MARCHID         = 12'hF12,
    CSR_ADDR_MIMPID          = 12'hF13,
    CSR_ADDR_MHARTID         = 12'hF14,
    CSR_ADDR_MSTATUS         = 12'h300,
    CSR_ADDR_MISA            = 12'h301,
    CSR_ADDR_MEDELEG         = 12'h302,
    CSR_ADDR_MIDELEG         = 12'h303,
    CSR_ADDR_MIE             = 12'h304,
    CSR_ADDR_MTVEC           = 12'h305,
    CSR_ADDR_MCOUNTEREN      = 12'h306,
    CSR_ADDR_MCOUNTINHIBIT   = 12'h320,
    CSR_ADDR_MSCRATCH        = 12'h340,
    CSR_ADDR_MEPC            = 12'h341,
    CSR_ADDR_MCAUSE          = 12'h342,
    CSR_ADDR_MTVAL           = 12'h343,
    CSR_ADDR_MIP             = 12'h344,
    CSR_ADDR_PMPCFG0         = 12'h3A0,
    CSR_ADDR_PMPCFG1         = 12'h3A1,
    CSR_ADDR_PMPCFG2         = 12'h3A2,
    CSR_ADDR_PMPCFG3         = 12'h3A3,
    CSR_ADDR_PMPADDR0        = 12'h3B0,
    CSR_ADDR_PMPADDR1        = 12'h3B1,
    CSR_ADDR_PMPADDR2        = 12'h3B2,
    CSR_ADDR_PMPADDR3        = 12'h3B3,
    CSR_ADDR_PMPADDR4        = 12'h3B4,
    CSR_ADDR_PMPADDR5        = 12'h3B5,
    CSR_ADDR_PMPADDR6        = 12'h3B6,
    CSR_ADDR_PMPADDR7        = 12'h3B7,
    CSR_ADDR_PMPADDR8        = 12'h3B8,
    CSR_ADDR_PMPADDR9        = 12'h3B9,
    CSR_ADDR_PMPADDR10       = 12'h3BA,
    CSR_ADDR_PMPADDR11       = 12'h3BB,
    CSR_ADDR_PMPADDR12       = 12'h3BC,
    CSR_ADDR_PMPADDR13       = 12'h3BD,
    CSR_ADDR_PMPADDR14       = 12'h3BE,
    CSR_ADDR_PMPADDR15       = 12'h3BF,
    CSR_ADDR_MCYCLE          = 12'hB00,
    CSR_ADDR_MINSTRET        = 12'hB02,
    CSR_ADDR_MHPMCOUNTER3    = 12'hB03,
    CSR_ADDR_MHPMCOUNTER4    = 12'hB04,
    CSR_ADDR_MHPMCOUNTER5    = 12'hB05,
    CSR_ADDR_MHPMCOUNTER6    = 12'hB06,
    CSR_ADDR_MHPMCOUNTER7    = 12'hB07,
    CSR_ADDR_MHPMCOUNTER8    = 12'hB08,
    CSR_ADDR_MHPMCOUNTER9    = 12'hB09,
    CSR_ADDR_MHPMCOUNTER10   = 12'hB0A,
    CSR_ADDR_MHPMCOUNTER11   = 12'hB0B,
    CSR_ADDR_MHPMCOUNTER12   = 12'hB0C,
    CSR_ADDR_MHPMCOUNTER13   = 12'hB0D,
    CSR_ADDR_MHPMCOUNTER14   = 12'hB0E,
    CSR_ADDR_MHPMCOUNTER15   = 12'hB0F,
    CSR_ADDR_MHPMCOUNTER16   = 12'hB10,
    CSR_ADDR_MHPMCOUNTER17   = 12'hB11,
    CSR_ADDR_MHPMCOUNTER18   = 12'hB12,
    CSR_ADDR_MHPMCOUNTER19   = 12'hB13,
    CSR_ADDR_MHPMCOUNTER20   = 12'hB14,
    CSR_ADDR_MHPMCOUNTER21   = 12'hB15,
    CSR_ADDR_MHPMCOUNTER22   = 12'hB16,
    CSR_ADDR_MHPMCOUNTER23   = 12'hB17,
    CSR_ADDR_MHPMCOUNTER24   = 12'hB18,
    CSR_ADDR_MHPMCOUNTER25   = 12'hB19,
    CSR_ADDR_MHPMCOUNTER26   = 12'hB1A,
    CSR_ADDR_MHPMCOUNTER27   = 12'hB1B,
    CSR_ADDR_MHPMCOUNTER28   = 12'hB1C,
    CSR_ADDR_MHPMCOUNTER29   = 12'hB1D,
    CSR_ADDR_MHPMCOUNTER30   = 12'hB1E,
    CSR_ADDR_MHPMCOUNTER31   = 12'hB1F,

    CSR_ADDR_MCYCLEH         = 12'hB80,
    CSR_ADDR_MINSTRETH       = 12'hB82,
    CSR_ADDR_MHPMCOUNTER3H   = 12'hB83,
    CSR_ADDR_MHPMCOUNTER4H   = 12'hB84,
    CSR_ADDR_MHPMCOUNTER5H   = 12'hB85,
    CSR_ADDR_MHPMCOUNTER6H   = 12'hB86,
    CSR_ADDR_MHPMCOUNTER7H   = 12'hB87,
    CSR_ADDR_MHPMCOUNTER8H   = 12'hB88,
    CSR_ADDR_MHPMCOUNTER9H   = 12'hB89,
    CSR_ADDR_MHPMCOUNTER10H  = 12'hB8A,
    CSR_ADDR_MHPMCOUNTER11H  = 12'hB8B,
    CSR_ADDR_MHPMCOUNTER12H  = 12'hB8C,
    CSR_ADDR_MHPMCOUNTER13H  = 12'hB8D,
    CSR_ADDR_MHPMCOUNTER14H  = 12'hB8E,
    CSR_ADDR_MHPMCOUNTER15H  = 12'hB8F,
    CSR_ADDR_MHPMCOUNTER16H  = 12'hB90,
    CSR_ADDR_MHPMCOUNTER17H  = 12'hB91,
    CSR_ADDR_MHPMCOUNTER18H  = 12'hB92,
    CSR_ADDR_MHPMCOUNTER19H  = 12'hB93,
    CSR_ADDR_MHPMCOUNTER20H  = 12'hB94,
    CSR_ADDR_MHPMCOUNTER21H  = 12'hB95,
    CSR_ADDR_MHPMCOUNTER22H  = 12'hB96,
    CSR_ADDR_MHPMCOUNTER23H  = 12'hB97,
    CSR_ADDR_MHPMCOUNTER24H  = 12'hB98,
    CSR_ADDR_MHPMCOUNTER25H  = 12'hB99,
    CSR_ADDR_MHPMCOUNTER26H  = 12'hB9A,
    CSR_ADDR_MHPMCOUNTER27H  = 12'hB9B,
    CSR_ADDR_MHPMCOUNTER28H  = 12'hB9C,
    CSR_ADDR_MHPMCOUNTER29H  = 12'hB9D,
    CSR_ADDR_MHPMCOUNTER30H  = 12'hB9E,
    CSR_ADDR_MHPMCOUNTER31H  = 12'hB9F,

    CSR_ADDR_MHPMEVENT3      = 12'h323,
    CSR_ADDR_MHPMEVENT4      = 12'h324,
    CSR_ADDR_MHPMEVENT5      = 12'h325,
    CSR_ADDR_MHPMEVENT6      = 12'h326,
    CSR_ADDR_MHPMEVENT7      = 12'h327,
    CSR_ADDR_MHPMEVENT8      = 12'h328,
    CSR_ADDR_MHPMEVENT9      = 12'h329,
    CSR_ADDR_MHPMEVENT10     = 12'h32A,
    CSR_ADDR_MHPMEVENT11     = 12'h32B,
    CSR_ADDR_MHPMEVENT12     = 12'h32C,
    CSR_ADDR_MHPMEVENT13     = 12'h32D,
    CSR_ADDR_MHPMEVENT14     = 12'h32E,
    CSR_ADDR_MHPMEVENT15     = 12'h32F,
    CSR_ADDR_MHPMEVENT16     = 12'h330,
    CSR_ADDR_MHPMEVENT17     = 12'h331,
    CSR_ADDR_MHPMEVENT18     = 12'h332,
    CSR_ADDR_MHPMEVENT19     = 12'h333,
    CSR_ADDR_MHPMEVENT20     = 12'h334,
    CSR_ADDR_MHPMEVENT21     = 12'h335,
    CSR_ADDR_MHPMEVENT22     = 12'h336,
    CSR_ADDR_MHPMEVENT23     = 12'h337,
    CSR_ADDR_MHPMEVENT24     = 12'h338,
    CSR_ADDR_MHPMEVENT25     = 12'h339,
    CSR_ADDR_MHPMEVENT26     = 12'h33A,
    CSR_ADDR_MHPMEVENT27     = 12'h33B,
    CSR_ADDR_MHPMEVENT28     = 12'h33C,
    CSR_ADDR_MHPMEVENT29     = 12'h33D,
    CSR_ADDR_MHPMEVENT30     = 12'h33E,
    CSR_ADDR_MHPMEVENT31     = 12'h33F,
    CSR_ADDR_TSELECT         = 12'h7A0,
    CSR_ADDR_TDATA1          = 12'h7A1,
    CSR_ADDR_TDATA2          = 12'h7A2,
    CSR_ADDR_TDATA3          = 12'h7A3,
    CSR_ADDR_DCSR            = 12'h7B0,
    CSR_ADDR_DPC             = 12'h7B1,
    CSR_ADDR_DSCRATCH        = 12'h7B2
    // }}}
    // }}}
  } csr_addr_t;
  typedef enum logic [1:0] { // {{{
    OFF   = 2'b00,
    TOR   = 2'b01,
    NA4   = 2'b10,
    NAPOT = 2'b11 // }}}
  } pmp_access_type;
  typedef enum logic [1:0] { // {{{
    PRV_U = 2'b00,
    PRV_S = 2'b01,
    PRV_M = 2'b11 // }}}
  } prv_t;
  typedef enum logic [1:0] { // {{{
    CSR_OP_RW = 2'b01,
    CSR_OP_RS = 2'b10,
    CSR_OP_RC = 2'b11,
    CSR_OP_NONE = 2'b00 // }}}
  } csr_op_t;
  typedef enum logic [1:0] { // {{{
    CSR_MTVEC_DIRECT = 2'b00,
    CSR_MTVEC_VECTORED = 2'b01,
    CSR_MTVEC_RSVD0 = 2'b10,
    CSR_MTVEC_RSVD1 = 2'b11 // }}}
  } csr_mtvec_mode_t;
  typedef enum logic [63:0] { // {{{
    CSR_MCAUSE_U_SW_INT = {1'b1, 63'('d0)},
    CSR_MCAUSE_S_SW_INT = {1'b1, 63'('d1)},
    CSR_MCAUSE_M_SW_INT = {1'b1, 63'('d3)},
    CSR_MCAUSE_U_TIME_INT = {1'b1, 63'('d4)},
    CSR_MCAUSE_S_TIME_INT = {1'b1, 63'('d5)},
    CSR_MCAUSE_M_TIME_INT = {1'b1, 63'('d7)},
    CSR_MCAUSE_U_EXT_INT = {1'b1, 63'('d8)},
    CSR_MCAUSE_S_EXT_INT = {1'b1, 63'('d9)},
    CSR_MCAUSE_M_EXT_INT = {1'b1, 63'('d11)},
    CSR_MCAUSE_INST_ADDR_MISALIGNED = {1'b0, 63'('d0)},
    CSR_MCAUSE_INST_ACCESS_FAULT = {1'b0, 63'('d1)},
    CSR_MCAUSE_ILLEGAL_INST = {1'b0, 63'('d2)},
    CSR_MCAUSE_BREAKPOINT = {1'b0, 63'('d3)},
    CSR_MCAUSE_LOAD_ADDR_MISALIGNED = {1'b0, 63'('d4)},
    CSR_MCAUSE_LOAD_ACCESS_FAULT = {1'b0, 63'('d5)},
    CSR_MCAUSE_STORE_ADDR_MISALIGNED = {1'b0, 63'('d6)},
    CSR_MCAUSE_STORE_ACCESS_FAULT = {1'b0, 63'('d7)},
    CSR_MCAUSE_ECALL_FROM_U = {1'b0, 63'('d8)},
    CSR_MCAUSE_ECALL_FROM_S = {1'b0, 63'('d9)},
    CSR_MCAUSE_ECALL_FROM_M = {1'b0, 63'('d11)},
    CSR_MCAUSE_INST_PAGE_FAULT = {1'b0, 63'('d12)},
    CSR_MCAUSE_LOAD_PAGE_FAULT = {1'b0, 63'('d13)},
    CSR_MCAUSE_STORE_PAGE_FAULT = {1'b0, 63'('d15)}
    // }}}
  } csr_mcause_t;
  typedef enum logic [3:0] { // {{{
    INT_U_SW    = 'd0,
    INT_S_SW    = 'd1,
    INT_M_SW    = 'd3,
    INT_U_TIME  = 'd4,
    INT_S_TIME  = 'd5,
    INT_M_TIME  = 'd7,
    INT_U_EXT   = 'd8,
    INT_S_EXT   = 'd9,
    INT_M_EXT   = 'd11 // }}}
  } int_cause_t;
  typedef enum logic [3:0] { // {{{
    EXCP_CAUSE_INST_ADDR_MISALIGNED = 4'('d0),
    EXCP_CAUSE_INST_ACCESS_FAULT = 4'('d1),
    EXCP_CAUSE_ILLEGAL_INST = 4'('d2),
    EXCP_CAUSE_BREAKPOINT = 4'('d3),
    EXCP_CAUSE_LOAD_ADDR_MISALIGNED = 4'('d4),
    EXCP_CAUSE_LOAD_ACCESS_FAULT = 4'('d5),
    EXCP_CAUSE_STORE_ADDR_MISALIGNED = 4'('d6),
    EXCP_CAUSE_STORE_ACCESS_FAULT = 4'('d7),
    EXCP_CAUSE_ECALL_FROM_U = 4'('d8),
    EXCP_CAUSE_ECALL_FROM_S = 4'('d9),
    EXCP_CAUSE_ECALL_FROM_M = 4'('d11),
    EXCP_CAUSE_INST_PAGE_FAULT = 4'('d12),
    EXCP_CAUSE_LOAD_PAGE_FAULT = 4'('d13),
    EXCP_CAUSE_STORE_PAGE_FAULT = 4'('d15),
    EXCP_CAUSE_NONE = 4'('d10)
    // }}}
  } excp_cause_t;
  typedef enum logic [1:0] { // {{{
    FENCE_TYPE_FENCE,
    FENCE_TYPE_FENCE_I,
    FENCE_TYPE_NONE // }}}
  } fence_type_t;
  typedef struct packed { // {{{
    logic         HPM31;
    logic         HPM30;
    logic         HPM29;
    logic         HPM28;
    logic         HPM27;
    logic         HPM26;
    logic         HPM25;
    logic         HPM24;
    logic         HPM23;
    logic         HPM22;
    logic         HPM21;
    logic         HPM20;
    logic         HPM19;
    logic         HPM18;
    logic         HPM17;
    logic         HPM16;
    logic         HPM15;
    logic         HPM14;
    logic         HPM13;
    logic         HPM12;
    logic         HPM11;
    logic         HPM10;
    logic         HPM9;
    logic         HPM8;
    logic         HPM7;
    logic         HPM6;
    logic         HPM5;
    logic         HPM4;
    logic         HPM3;
    logic         IR;
    logic         TM;
    logic         CY; // }}}
  } csr_counteren_t;
  typedef struct packed { // {{{
    logic         HPM31;
    logic         HPM30;
    logic         HPM29;
    logic         HPM28;
    logic         HPM27;
    logic         HPM26;
    logic         HPM25;
    logic         HPM24;
    logic         HPM23;
    logic         HPM22;
    logic         HPM21;
    logic         HPM20;
    logic         HPM19;
    logic         HPM18;
    logic         HPM17;
    logic         HPM16;
    logic         HPM15;
    logic         HPM14;
    logic         HPM13;
    logic         HPM12;
    logic         HPM11;
    logic         HPM10;
    logic         HPM9;
    logic         HPM8;
    logic         HPM7;
    logic         HPM6;
    logic         HPM5;
    logic         HPM4;
    logic         HPM3;
    logic         IR;
    logic         O;
    logic         CY; // }}}
  } csr_countinhibit_t;
  typedef enum logic {
    BARE = 1'b0,
    SV32 = 1'b1
  } satp_mode_t;

  //------------------------------------------------------
  // floating point {{{
  typedef enum logic [2:0] {
    FRM_RNE = 3'b000,
    FRM_RTZ = 3'b001,
    FRM_RDN = 3'b010,
    FRM_RUP = 3'b011,
    FRM_RMM = 3'b100
  } frm_t; // rounding mode for floating-point
  typedef enum logic [2:0] { // {{{
    FRM_DW_RNE = 3'b000,
    FRM_DW_RTZ = 3'b001,
    FRM_DW_RDN = 3'b011,
    FRM_DW_RUP = 3'b010,
    FRM_DW_RMM = 3'b100 // }}}
  } frm_dw_t; // rounding mode for floating-point
  typedef enum logic [3:0] { // {{{
    FP_OP_ADD_S,
    FP_OP_MAC_S,
    FP_OP_DIV_S,
    FP_OP_SQRT_S,
    FP_OP_ADD_D,
    FP_OP_MAC_D,
    FP_OP_DIV_D,
    FP_OP_SQRT_D,
    FP_OP_MISC,
    FP_OP_NONE // }}}
  } fp_op_t;
  typedef enum logic { // {{{
    FP_RS1_SEL_RS1,
    FP_RS1_SEL_RS1_INV // }}}
  } fp_rs1_sel_t;
  typedef enum logic { // {{{
    FP_RS2_SEL_RS2,
    FP_RS2_SEL_RS2_INV // }}}
  } fp_rs2_sel_t;
  typedef enum logic [1:0] { // {{{
    FP_RS3_SEL_ZERO,
    FP_RS3_SEL_RS3,
    FP_RS3_SEL_RS3_INV // }}}
  } fp_rs3_sel_t;
  typedef enum logic [1:0] { // {{{
    FP_SGNJ_SEL_J,
    FP_SGNJ_SEL_JN,
    FP_SGNJ_SEL_JX // }}}
  } fp_sgnj_sel_t;
  typedef enum logic [2:0] { // {{{
    FP_CMP_SEL_MAX,
    FP_CMP_SEL_MIN,
    FP_CMP_SEL_EQ,
    FP_CMP_SEL_LT,
    FP_CMP_SEL_LE,
    FP_CMP_SEL_NONE // }}}
  } fp_cmp_sel_t;
  typedef enum logic [1:0] { // {{{
    FP_CVT_SEL_W,
    FP_CVT_SEL_WU,
    FP_CVT_SEL_L,
    FP_CVT_SEL_LU // }}}
  } fp_cvt_sel_t;
  typedef enum logic [4:0] { // {{{
    FP_OUT_SEL_ADD_S,
    FP_OUT_SEL_CMP_S,
    FP_OUT_SEL_MAC_S,
    FP_OUT_SEL_DIV_S,
    FP_OUT_SEL_SQRT_S,
    FP_OUT_SEL_ADD_D,
    FP_OUT_SEL_CMP_D,
    FP_OUT_SEL_MAC_D,
    FP_OUT_SEL_DIV_D,
    FP_OUT_SEL_SQRT_D,
    FP_OUT_SEL_SGNJ,
    FP_OUT_SEL_CVT_I2S,
    FP_OUT_SEL_CVT_I2D,
    FP_OUT_SEL_CVT_S2I,
    FP_OUT_SEL_CVT_D2I,
    FP_OUT_SEL_CVT_S2D,
    FP_OUT_SEL_CVT_D2S,
    FP_OUT_SEL_CLS,
    FP_OUT_SEL_NONE // }}}
  } fp_out_sel_t;
  // }}}

  //------------------------------------------------------
  typedef enum logic [1:0] {
    ACC_SEL_ACC = 2'b01,
    ACC_SEL_RS  = 2'b10,
    ACC_SEL_NONE = 2'b00
  } acc_sel_t;

  //==========================================================
  // struct

  typedef struct packed { // {{{
    logic Z;
    logic Y;
    logic X;
    logic W;
    logic V;
    logic U;
    logic T;
    logic S;
    logic R;
    logic Q;
    logic P;
    logic O;
    logic N;
    logic M;
    logic L;
    logic K;
    logic J;
    logic I;
    logic H;
    logic G;
    logic F;
    logic E;
    logic D;
    logic C;
    logic B;
    logic A;// }}}
  } csr_misa_ext_t;
  typedef struct packed { // {{{
    logic [ 1:0]  MXL; // [63:62]
    csr_misa_ext_t EXTENSIONS;
  } csr_misa_t;
  typedef struct packed { // {{{
    logic         SD;
    logic [ 7:0]  reserved_31_to_23;
    logic         TSR;
    logic         TW;
    logic         TVM;
    logic         MXR;
    logic         SUM;
    logic         MPRV;
    logic [ 1:0]  XS;
    logic [ 1:0]  FS;
    prv_t         MPP;
    logic [ 1:0]  reserved_10_to_9;
    logic         SPP;
    logic         MPIE;
    logic         reserved_6;
    logic         SPIE;
    logic         UPIE;
    logic         MIE;
    logic         reserved_2;
    logic         SIE;
    logic         UIE; // }}}
  } csr_mstatus_t;
  typedef struct packed { // {{{
    logic [(XLEN-2)-1:0]  base;
    csr_mtvec_mode_t mode; // }}}
  } csr_mtvec_t;
  typedef struct packed { // {{{
    logic en_hpmcounter; // }}}
  } csr_cfg_t;
  typedef struct packed { // {{{
    logic MEIP;
    logic HEIP;
    logic SEIP;
    logic UEIP;
    logic MTIP;
    logic HTIP;
    logic STIP;
    logic UTIP;
    logic MSIP;
    logic HSIP;
    logic SSIP;
    logic USIP; // }}}
  } csr_mip_t;
  typedef struct packed {
    logic MEIE;
    logic HEIE;
    logic SEIE;
    logic UEIE;
    logic MTIE;
    logic HTIE;
    logic STIE;
    logic UTIE;
    logic MSIE;
    logic HSIE;
    logic SSIE;
    logic USIE;
  } csr_mie_t;
  typedef struct packed {
    logic           l;
    logic [1:0]     reserved;
    pmp_access_type a;
    logic           x;
    logic           w;
    logic           r;
  } csr_pmpcfg_part_t;
  typedef struct packed {
    csr_pmpcfg_part_t [7:0] pmpcfg;
  } csr_pmpcfg_t; // for RV64
  typedef struct packed {
    logic [9:0]     reserved;
    logic [53:0]    addr;
  } csr_pmpaddr_t; // for RV64

//------------------------------------------------------
  // // floating point
   typedef struct packed { // {{{
    logic nv;
    logic dz;
    logic of;
    logic uf;
    logic nx; // }}}
  } fflags_t; // floating-point flags
  typedef struct packed { // {{{
    logic compspecific;
    logic hugeint;
    logic inexact;
    logic huge;
    logic tiny;
    logic invalid;
    logic infinity;
    logic zero; // }}}
  } fstatus_dw_t; // status output from DW
  typedef struct packed { // {{{
    logic q_nan;      // quiet NaN
    logic s_nan;      // signaling NaN
    logic pos_inf;    // +infinity
    logic pos;        // +normal
    logic pos_sub;    // +subnormal
    logic pos_zero;   // +0
    logic neg_zero;   // -0
    logic neg_sub;    // -subnormal
    logic neg;        // -normal
    logic neg_inf;    // -infinity }}}
  } fp_cls_t;

  //------------------------------------------------------
  // exception
  typedef struct packed { // {{{
    logic         valid;
    excp_cause_t  cause;
    vaddr_t       vpc;
    // }}}
  } excp_t;

  //==========================================================
  // pipeline registers

  //------------------------------------------------------
  // if2id {{{
  typedef struct packed {
    inst_t        inst;
    vaddr_t       vpc;
    reg_addr_t    rs1_addr, rs2_addr, rs3_addr, rd_addr;
    logic         is_rvc;
  } if2id_t;

  typedef struct packed {
    logic         is_rs1_fp, is_rs2_fp,
                  rs1_re, rs2_re, rs3_re,
                  is_rs1_x0, is_rs2_x0;
`ifndef SYNTHESIS
    logic [63:0]  inst_code;
`endif
  } if2id_ctrl_t;

  typedef struct packed { // (by Anh)
      logic [2:0]                 thread_id;
      logic [31:0]                instr;
  } if2vid_t;
  // }}}

  //------------------------------------------------------
  // id2ex {{{
  typedef struct packed {
    vaddr_t       vpc;
    inst_t        inst;
    data_t        rs1, rs2;
`ifdef RRV_SUPPORT_FP
    data_t        fp_rs1;
`endif
    reg_addr_t    rd_addr;
    logic         is_rvc;
  } id2ex_t;

  typedef struct packed {
    // copy from IF
    logic         is_rs1_fp, is_rs2_fp;
    // new in ID
    alu_op_t      alu_op;
    logic         alu_en;
    logic         is_32;
    op1_sel_t     op1_sel;
    op2_sel_t     op2_sel;
    imm_sel_t     imm_sel;
    rd_avail_t    rd_avail;
    logic         rd_we;
    logic         is_rd_fp;
    ex_pc_adder_sel_t ex_pc_adder_sel;
    ex_next_pc_sel_t  ex_next_pc_sel;
    ex_out_sel_t      ex_out_sel;
    // new in ID for MULDIV
    mul_type_t    mul_type;
    div_type_t    div_type;
    // new in ID for FP
    fp_op_t       fp_op;
    fp_rs1_sel_t  fp_rs1_sel;
    fp_rs2_sel_t  fp_rs2_sel;
    fp_rs3_sel_t  fp_rs3_sel;
    fp_sgnj_sel_t fp_sgnj_sel;
    fp_cmp_sel_t  fp_cmp_sel;
    fp_cvt_sel_t  fp_cvt_sel;
    fp_out_sel_t  fp_out_sel;
    // new in ID
    acc_sel_t     acc_sel;
`ifndef SYNTHESIS
    logic [63:0]  inst_code;
`endif
  } id2ex_ctrl_t;

  typedef struct packed {
    data_t        rs1, rs2;
  } id2muldiv_t;

  typedef struct packed {
    logic         valid;
    logic         is_same;
  } id2muldiv_ctrl_t;
  // }}}

  //------------------------------------------------------
  // forwarding {{{
  typedef struct packed {
    logic       is_rd_fp;
    reg_addr_t  rd_addr;
    data_t      rd;
  } forward_t;
  // }}}

  //------------------------------------------------------
  // ex2ma {{{
  typedef struct packed {
    vaddr_t       vpc;
    inst_t        inst;
    data_t        ex_out;
    reg_addr_t    rd_addr;
    csr_addr_t    csr_addr;
    fflags_t      fflags;
  } ex2ma_t;

  typedef struct packed {
    // copy from ID
    logic         is_rd_fp,
                  rd_we;
    rd_avail_t    rd_avail;
    // new in EX to MA
    logic         is_load, is_store;
    fence_type_t  fence_type;
    rd_sel_t      rd_sel;
    ma_byte_sel_t ma_byte_sel;
    // new in EX to CS
    csr_op_t      csr_op;
    ret_type_t    ret_type;
    logic         is_wfi;
    logic         is_wfe;

`ifndef SYNTHESIS
    logic [63:0]  inst_code;
`endif
  } ex2ma_ctrl_t;
  // }}}

  //------------------------------------------------------
  // ma2cs {{{
  typedef struct packed {
    vaddr_t     vpc;
    inst_t      inst;
    csr_addr_t  csr_addr;
    data_t      rs1;
    fflags_t    fflags;
    vaddr_t     mem_addr;
  } ma2cs_t;

  typedef struct packed {
    csr_op_t    csr_op;
    ret_type_t  ret_type;
    logic       is_wfi;
  } ma2cs_ctrl_t;

  typedef struct packed {
    data_t      csr;
  } cs2ma_t;
  // }}}

  //------------------------------------------------------
  // ma2wb {{{
  typedef struct packed {
    vaddr_t     vpc;
    inst_t      inst;
    data_t      rd;
    reg_addr_t  rd_addr;
  } ma2wb_t;

  typedef struct packed {
    logic rd_we, is_rd_fp;
`ifndef SYNTHESIS
    logic [63:0]  inst_code;
`endif
  } ma2wb_ctrl_t;
  // }}}

  //------------------------------------------------------
  // regfile {{{
  typedef struct packed {
    reg_addr_t  rs1_addr, rs2_addr;
    logic       rs1_re, rs2_re;
  } if2irf_t;

  typedef struct packed {
    reg_addr_t  rs1_addr, rs2_addr, rs3_addr;
    logic       rs1_re, rs2_re, rs3_re;
  } if2fprf_t;
  typedef struct packed {
    data_t  rs1, rs2;
  } irf2id_t;
  typedef struct packed {
    data_t  rs1, rs2, rs3;
  } fprf2id_t;
  typedef struct packed {
    data_t      rd;
    reg_addr_t  rd_addr;
    logic       rd_we;
  } ma2rf_t;
  // }}}

  //------------------------------------------------------
  // cpunoc i/f {{{

  typedef enum logic [CPUNOC_TID_SRCID_SIZE-1:0] {
    RRV_DC_SRC_ID   = CPUNOC_TID_SRCID_SIZE'(4'h0),
    RRV_IC_SRC_ID   = CPUNOC_TID_SRCID_SIZE'(4'h1)
  } rrv_src_id_t;
 
  // }}}

  //------------------------------------------------------
  // inst buffer {{{

  typedef struct packed { // {{{
    vaddr_t     vpc;
    logic       en; // }}}
  } pc2ib_t;

  typedef struct packed { // {{{
    inst_t       inst;
    logic        is_rvc;
    logic        valid; // }}}
  } ib2if_t;

  typedef struct packed { // {{{
    vaddr_t     vpc;
    logic       en; // }}}
  } ib2icmem_t;

  typedef struct packed { // {{{
    cache_line_t       rdata;
    logic              valid; // }}}
  } icmem2ib_t;

  typedef struct packed { // {{{
    vaddr_t     vpc; // }}}
  } ib2icmem_req_t;

  typedef struct packed { // {{{
    cache_line_t       rdata; // }}}
  } icmem2ib_resp_t;

  // }}}

  //------------------------------------------------------
  // L1 path enum {{{

  typedef enum logic [1:0] {
    BYPASS_PATH = 2'b00,
    L1_PATH     = 2'b01,
    SYS_PATH    = 2'b10,
    TCM_PATH    = 2'b11
  } l1_path_t;


  // }}}
  //------------------------------------------------------
  // icache {{{
  typedef struct packed {
    vaddr_t   vpc;
  } pc2ic_t;

  typedef struct packed {
    logic     is_rvc;
    inst_t    inst;
  } ic2if_t;

  typedef struct packed {
    logic           is_excp;
    cache_line_t    rdata;
  } ic2ib_t;

  typedef logic [ICACHE_TAG_WIDTH-1:0]  ic_tag_t;
  typedef struct packed {
    logic     valid;
    ic_tag_t  tag;
  } ic_tag_ram_t;
  typedef logic [ICACHE_INDEX_WIDTH-1:0]        ic_idx_t;
  typedef logic [ICACHE_DATA_BANK_ID_WIDTH-1:0] ic_bkid_t;
  typedef logic [$clog2(N_ICACHE_PREFETCH)-1:0] ic_prefetch_idx_t;
  // }}}

  //------------------------------------------------------
  // dcache {{{
  typedef struct packed {
    logic             rw;
    vaddr_t           vaddr;
    data_t            wdata;
    data_byte_mask_t  mask;
  } ex2dc_t;

  typedef struct packed {
    // logic             is_page_fault;
    logic             is_excp;
    data_t            rdata;
  } dc2ma_t;

  typedef logic [DCACHE_TAG_WIDTH-1:0]  dc_tag_t;
  typedef struct packed {
    logic     valid;
    dc_tag_t  tag;
  } dc_tag_ram_t;
  typedef logic [31:0]                          dc_data_ram_t;
  typedef logic [N_DCACHE_WAY-1:0]              dc_dirty_ram_t;
  typedef logic [DCACHE_INDEX_WIDTH-1:0]        dc_idx_t;
  typedef logic [DCACHE_DATA_BANK_ID_WIDTH-1:0] dc_bkid_t;

  typedef struct packed {
    logic                                     valid;
    data_t  [CACHE_LINE_BYTE*8/64-1:0]        data;
    logic   [CACHE_LINE_BYTE*8/64-1:0] [7:0]  dirty; // every byte has 1 dirty bit to indicate that it's been written
    logic   [DCACHE_TAG_WIDTH+DCACHE_INDEX_WIDTH-1:0] tag;
  } store_buf_t;
  // }}}

  //------------------------------------------------------
  // tlb & ptw {{{
  typedef struct packed {
    ppn_t             ppn;
    logic       [1:0] reserved;
    logic             dirty;
    logic             accessed;
    logic             global_map;
    logic             user;
    logic             perm_x;
    logic             perm_w;
    logic             perm_r;
    logic             valid;
  } pte_t; // page table entry
  //typedef logic [$clog2((PAGE_SIZE_BYTE/PTE_SIZE))-1:0] pte_idx_t;
  typedef logic [8:0] pte_idx_t;
  typedef enum logic [1:0] {
    ACCESS_FETCH = 2'b00,
    ACCESS_LOAD  = 2'b01,
    ACCESS_STORE = 2'b10,
    ACCESS_NONE  = 2'b11
  } access_type_t;
  typedef struct packed {
    asid_t  asid;
    vpn_t   vpn;
    pte_t   pte;
  } tlb_t;

  typedef struct packed {
    access_type_t   access_type;
    vpn_t           vpn;
  } cc2tlb_t;

  typedef struct packed {
    logic        excp_valid;
    excp_cause_t excp_cause;
    ppn_t        ppn;
  } tlb2cc_t;

  typedef struct packed {
    logic         rw;
    vpn_t         vpn;
    pte_t         pte;
    access_type_t access_type;
  } tlb2ptw_t;

  typedef struct packed {
    logic         excp_valid;
    excp_cause_t  excp_cause;
    pte_t         pte;
  } ptw2tlb_t;

  // }}}

  //------------------------------------------------------
  // floating point {{{
  typedef struct packed {
    data_t        rs1, rs2;
    frm_dw_t      frm_dw;
  } id2fp_add_t;
  typedef struct packed {
    data_t        rs1, rs2, rs3;
    frm_dw_t      frm_dw;
  } id2fp_mac_t;
  typedef struct packed {
    data_t        rs1, rs2;
    frm_dw_t      frm_dw;
  } id2fp_div_t;
  typedef struct packed {
    data_t        rs1;
    frm_dw_t      frm_dw;
  } id2fp_sqrt_t;
  typedef struct packed {
    data_t        rs1, rs2;
    frm_dw_t      frm_dw;
  } id2fp_misc_t;
  typedef struct packed {
    frm_t     frm;
    fflags_t  fflags;
  } csr_fcsr_t;
  typedef struct packed {
    logic ovl_32;
    logic ovl_32u;
    logic ovl_64;
    logic ovl_64u;
  } fp2i_ovl_t;
  // }}}

  typedef enum logic [1:0] {
    SFENCE_ASID       = 2'b00,
    SFENCE_VPN        = 2'b01,
    SFENCE_ASID_VPN   = 2'b10,
    SFENCE_ALL        = 2'b11
  } sfence_type_t;

  typedef struct packed {
    sfence_type_t   sfence_type;
    asid_t          flush_asid;
    vpn_t           flush_vpn;
  } sfence_req_t;

  // L1 debug access
   typedef logic [N_ICACHE_WAY-1:0]                                              ic_tag_ram_en_pway_t;
  typedef logic [N_ICACHE_WAY-1:0]                                              ic_tag_ram_rw_pway_t;
  typedef logic [N_ICACHE_WAY*ICACHE_INDEX_WIDTH-1:0]                           ic_tag_ram_addr_pway_t;
  typedef logic [N_ICACHE_WAY*$bits(ic_tag_ram_t)-1:0]                          ic_tag_ram_din_pway_t;
  typedef logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK-1:0]                           ic_data_ram_en_pway_pbank_t;
  typedef logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK-1:0]                           ic_data_ram_rw_pway_pbank_t;
  typedef logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*ICACHE_INDEX_WIDTH-1:0]        ic_data_ram_idx_pway_pbank_t;
  typedef logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*ICACHE_DATA_BANK_ID_WIDTH-1:0] ic_data_ram_bkid_pway_pbank_t;
  typedef logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*32-1:0]                        ic_data_ram_din_pway_pbank_t;
  // IC to DA
  typedef logic [N_ICACHE_WAY*$bits(ic_tag_ram_t)-1:0]                          ic_tag_ram_dout_pway_t;
  typedef logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*32-1:0]                        ic_data_ram_dout_pway_pbank_t;
  
  // DA to DC
  typedef logic [N_DCACHE_WAY-1:0]                                              dc_tag_ram_en_pway_t;
  typedef logic [N_DCACHE_WAY-1:0]                                              dc_tag_ram_rw_pway_t;
  typedef logic [N_DCACHE_WAY*DCACHE_INDEX_WIDTH-1:0]                           dc_tag_ram_addr_pway_t;
  typedef logic [N_DCACHE_WAY*$bits(dc_tag_ram_t)-1:0]                          dc_tag_ram_din_pway_t;
  typedef logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK-1:0]                           dc_data_ram_en_pway_pbank_t;
  typedef logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK-1:0]                           dc_data_ram_rw_pway_pbank_t;
  typedef logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*DCACHE_INDEX_WIDTH-1:0]        dc_data_ram_idx_pway_pbank_t;
  typedef logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*DCACHE_DATA_BANK_ID_WIDTH-1:0] dc_data_ram_bkid_pway_pbank_t;
  typedef logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*$bits(data_t)-1:0]             dc_data_ram_din_pway_pbank_t;
  // DC to DA
  typedef logic [N_DCACHE_WAY*$bits(dc_tag_ram_t)-1:0]                          dc_tag_ram_dout_pway_t;
  typedef logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*$bits(data_t)-1:0]             dc_data_ram_dout_pway_pbank_t;
  
  
  typedef enum logic [3:0] {
    DA_ICACHE_TAG,
    DA_ICACHE_DATA,
    DA_DCACHE_TAG,
    DA_DCACHE_DATA,
    DA_REG
  } da_type_t;

  typedef struct packed {
    logic [3:0] way_id;
    logic [9:0] index;
    logic [3:0] data_bank;
  } l1da_addr_t;

  typedef struct packed {
    logic [N_ICACHE_WAY-1:0]                       tag_ram_en_pway;
    logic [N_ICACHE_WAY-1:0]                       tag_ram_rw_pway;
    logic [N_ICACHE_WAY*ICACHE_INDEX_WIDTH-1:0]    tag_ram_addr_pway;
    logic [N_ICACHE_WAY*$bits(ic_tag_ram_t)-1:0]   tag_ram_din_pway;
    logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK-1:0]    data_ram_en_pway_pbank;
    logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK-1:0]    data_ram_rw_pway_pbank;
    logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*ICACHE_INDEX_WIDTH-1:0] data_ram_idx_pway_pbank;
    logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*ICACHE_DATA_BANK_ID_WIDTH-1:0] data_ram_bkid_pway_pbank;
    logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*32-1:0] data_ram_din_pway_pbank;
  } da2ic_t;

  typedef struct packed {
    logic [N_ICACHE_WAY*$bits(ic_tag_ram_t)-1:0]    tag_ram_dout_pway;
    logic [N_ICACHE_WAY*N_ICACHE_DATA_BANK*32-1:0]  data_ram_dout_pway_pbank;
  } ic2da_t;

  typedef struct packed {
    logic [N_DCACHE_WAY-1:0]                       tag_ram_en_pway;
    logic [N_DCACHE_WAY-1:0]                       tag_ram_rw_pway;
    logic [N_DCACHE_WAY*DCACHE_INDEX_WIDTH-1:0]    tag_ram_addr_pway;
    logic [N_DCACHE_WAY*$bits(dc_tag_ram_t)-1:0]   tag_ram_din_pway;
    logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK-1:0]    data_ram_en_pway_pbank;
    logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK-1:0]    data_ram_rw_pway_pbank;
    logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*DCACHE_INDEX_WIDTH-1:0] data_ram_idx_pway_pbank;
    logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*DCACHE_DATA_BANK_ID_WIDTH-1:0] data_ram_bkid_pway_pbank;
    logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*$bits(data_t)-1:0] data_ram_din_pway_pbank;
  } da2dc_t;

  typedef struct packed {
    logic [N_DCACHE_WAY*$bits(dc_tag_ram_t)-1:0]               tag_ram_dout_pway;
    logic [N_DCACHE_WAY*N_DCACHE_DATA_BANK*$bits(data_t)-1:0]  data_ram_dout_pway_pbank;
  } dc2da_t;

  typedef struct packed {
    cntr_t          mcycle;
    cntr_t          minstret;
    csr_mstatus_t   mstatus;
    vaddr_t         mepc;
    csr_mcause_t    mcause;
    cntr_t          hpmcounter3,
                    hpmcounter4,
                    hpmcounter5,
                    hpmcounter6,
                    hpmcounter7,
                    hpmcounter8,
                    hpmcounter9,
                    hpmcounter10;
  } cs2da_t;

  //------------------------------------------------------
  // L1 debug access
  typedef struct packed {
    vaddr_t   if_pc, wb_pc;
    logic     if_stall, if_kill, if_valid, if_ready,
              id_stall, id_kill, id_valid, id_ready,
              ex_stall, ex_kill, ex_valid, ex_ready,
              ma_stall, ma_kill, ma_valid, ma_ready,
              wb_valid, wb_ready,
              cs2if_kill, cs2id_kill, cs2ex_kill, cs2ma_kill,
              l2_req_valid, l2_req_ready, l2_resp_valid, l2_resp_ready,
              is_wfe, ma2if_npc_valid, ex2if_kill, ex2id_kill, branch_solved;
    pc2ic_t         pc2ic;
    ic2if_t         ic2if;
    excp_t          if2id_excp_ff;
    excp_t          id2ex_excp_ff;
    excp_t          ex2ma_excp_ff;
    excp_t          ma2cs_excp;
    if2id_ctrl_t    if2id_ctrl_ff;
    id2ex_ctrl_t    id2ex_ctrl_ff;
    ex2ma_ctrl_t    ex2ma_ctrl_ff;
    ma2cs_ctrl_t    ma2cs_ctrl;
    cs2da_t         cs2da;
  } rrv2da_t;

  //==========================================================
  // RVC
  typedef enum logic [1:0] {
    RVC_C0  = 2'b00,
    RVC_C1  = 2'b01,
    RVC_C2  = 2'b10
  } rvc_opcode_t;

  //==========================================================
  // ACP
  typedef struct packed {
    vaddr_t       vaddr;
  } acp_t;

  //==========================================================
  // TCM

  typedef logic [TCM_ADDR_WIDTH-1:0] tcm_addr_t;
  typedef logic [TCM_DATA_WIDTH-1:0] tcm_data_t;
  typedef logic [TCM_MASK_WIDTH-1:0] tcm_bytemask_t;
  typedef logic [TCM_BANK_ID_WIDTH-1:0] tcm_bank_id_t;
  typedef struct packed {
    logic          rw;
    tcm_data_t     wdata;
    tcm_bytemask_t mask;
    tcm_addr_t     addr;
  } tcm_req_t;
  typedef struct packed {
    tcm_data_t rdata;
  } tcm_resp_t;
  typedef struct packed {
    logic       dbg_en;
    logic       dbg_rw;
    tcm_addr_t  dbg_addr;
    tcm_data_t  dbg_din;
    tcm_bytemask_t dbg_mask;
  } da2tcm_t;
  typedef struct packed {
    tcm_data_t dbg_dout;
  } tcm2da_t;

  //==========================================================
  // instruction trace buffer

  typedef logic [ITB_ADDR_WIDTH-1:0]  itb_addr_t;
  typedef struct packed {
    vaddr_t   vpc;
  } itb_data_t;
  typedef enum logic [2:0] {
    ITB_SEL_IF = 3'h0,
    ITB_SEL_ID = 3'h1,
    ITB_SEL_EX = 3'h2,
    ITB_SEL_MA = 3'h3,
    ITB_SEL_WB = 3'h4
  } itb_sel_t;

  typedef struct packed {
    logic       en, rw;
    itb_addr_t  addr;
    itb_data_t  din;
  } da2itb_t;

  typedef struct packed {
    itb_data_t  dout;
  } itb2da_t;

endpackage

`endif
