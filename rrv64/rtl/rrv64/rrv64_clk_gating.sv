// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

module rrv64_clk_gating
  import rrv64_param_pkg::*;
  import rrv64_typedef_pkg::*;
#(parameter DRIVEN=4) (
  output  clkg,
  input   en, tst_en, rst, clk
);
    assign clkg = clk;
endmodule
