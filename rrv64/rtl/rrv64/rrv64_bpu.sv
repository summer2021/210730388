
// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*************************************************************************
 * Branch Prediction Unit
 **************************************************************************/

module rrv64_bpu
  import rrv64_param_pkg::*;
  import rrv64_typedef_pkg::*;
  import rrv64_func_pkg::*;
(
  input   logic               clk,
  input   logic               rstn, 
  // Prediction Results
  input   rrv64_vaddr_t       if2bpu_branch_addr,
  output  rrv64_npc_t         bpu2if_npc,
  // From decode
  input   logic               id2bpu_is_branch, 
  // input   logic               id2bpu_is_indirect,
  input   rrv64_vaddr_t       id2bpu_branch_addr, 
  output  rrv64_pred_info_t   bpu2id_pred_info, 
  // Update the predictors
  output  rrv64_pred_update_t ex2bpu_update
);

// Produce the prediction result with 1-cycle latency

parameter BHT_SIZE = 1024;
parameter BHT_IDX_BITS = $clog2(BHT_SIZE);
// Untagged BHT
reg [BHT_SIZE-1: 0] bhts[1: 0];

// Prediction logic
// use lower bits of addr to index
wire [BHT_IDX_BITS-1: 0] pred_bht_idx = if2bpu_branch_addr[BHT_IDX_BITS-1: 0];
reg [1: 0] bhts_line;
always @(posedge clk) begin
    bhts_line <= bhts[pred_bht_idx];
end

wire pred_taken = bhts_line[1] & id2bpu_is_branch;
// wire pred_taken = id2bpu_is_branch;
// wire pred_taken = 1'b0;
wire [1:0] pred_bimodal = bhts_line;
rrv64_vaddr_t pred_target = id2bpu_branch_addr;  // TODO: Add BTB

assign bpu2id_pred_info.pred_taken   = pred_taken  ;
assign bpu2id_pred_info.pred_target  = pred_target  ;
assign bpu2id_pred_info.pred_bimodal = pred_bimodal  ;

assign bpu2if_npc.pc = pred_target;
assign bpu2if_npc.valid = pred_taken;

// Update logic
wire [BHT_IDX_BITS-1: 0] update_bht_idx = ex2bpu_update.branch_addr[BHT_IDX_BITS-1: 0];
wire [1: 0] updated_bimodal_inc = ex2bpu_update.branch_bimodal == 2'b11 ? 2'b11 : ex2bpu_update.branch_bimodal + 1'b1;
wire [1: 0] updated_bimodal_dec = ex2bpu_update.branch_bimodal == 2'b00 ? 2'b00 : ex2bpu_update.branch_bimodal - 1'b1;
wire [1: 0] updated_bimodal = ex2bpu_update.branch_taken ? updated_bimodal_inc : updated_bimodal_dec;

always @(posedge clk) begin
    if(~rstn) begin
    end else if(ex2bpu_update.valid) begin
        bhts[update_bht_idx] <= updated_bimodal;
    end
end

endmodule
