// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

`ifndef __RRV_CFG__SV__
`define __RRV_CFG__SV__

package rrv_cfg;

  parameter int XLEN = 32;

  parameter ICACHE_SIZE_KBYTE = 4;
  parameter N_ICACHE_WAY = 4;
  parameter N_ICACHE_PREFETCH = 1;

  parameter N_ICACHE_LINE_SET = 32;
  parameter N_ICACHE_DATA_BANK = 8; // 32 is instruction width

  parameter ICACHE_LINE_WIDTH = 256;
  parameter ICACHE_OFFSET_WIDTH = 5;
  parameter ICACHE_INDEX_WIDTH = 5;
  parameter ICACHE_TAG_WIDTH = 22;

  parameter ICACHE_TAG_MSB = 31;
  parameter ICACHE_TAG_LSB = 10;

  parameter ICACHE_INDEX_MSB = 9;
  parameter ICACHE_INDEX_LSB = 5;

  parameter ICACHE_OFFSET_MSB = 4;
  parameter ICACHE_OFFSET_LSB = 0;

  parameter ICACHE_DATA_BANK_ID_WIDTH = 3;

  parameter ICACHE_DATA_BANK_ID_MSB = 4;
  parameter ICACHE_DATA_BANK_ID_LSB = 2;

  parameter ICACHE_WAY_ID_WIDTH = 2;

  //==========================================================

  parameter DCACHE_SIZE_KBYTE = 4;
  parameter N_DCACHE_WAY = 4;

  parameter N_DCACHE_LINE_SET = 32;
  parameter N_DCACHE_DATA_BANK = 8;

  parameter DCACHE_OFFSET_WIDTH = 5;
  parameter DCACHE_INDEX_WIDTH = 5;
  parameter DCACHE_TAG_WIDTH = 22;

  parameter DCACHE_TAG_MSB = 31;
  parameter DCACHE_TAG_LSB = 10;

  parameter DCACHE_INDEX_MSB = 9;
  parameter DCACHE_INDEX_LSB = 5;

  parameter DCACHE_OFFSET_MSB = 4;
  parameter DCACHE_OFFSET_LSB = 0;

  parameter DCACHE_DATA_BANK_ID_WIDTH = 3;

  parameter DCACHE_DATA_BANK_ID_MSB = 4;
  parameter DCACHE_DATA_BANK_ID_LSB = 2;

  parameter DCACHE_WAY_ID_WIDTH = 2;

  //==========================================================

  parameter N_DMA_CHNL = 4;
  parameter N_DMA_SIZE_BIT = 21;

  // Multicycle arithmetic unit completion cycles
  parameter N_CYCLE_INT_MUL = 1;
  parameter N_CYCLE_INT_DIV = 17;

  parameter N_CYCLE_INT_MUL_FPGA = 33;
  parameter N_CYCLE_INT_DIV_FPGA = 70;

  parameter N_CYCLE_FP_ADD_S = 16;
  parameter N_CYCLE_FP_ADD_D = 16;

  parameter N_CYCLE_FP_MAC_S = 16;
  parameter N_CYCLE_FP_MAC_D = 16;

  parameter N_CYCLE_FP_DIV_S = 16;
  parameter N_CYCLE_FP_DIV_D = 16;

  parameter N_CYCLE_FP_SQRT_S = 16;
  parameter N_CYCLE_FP_SQRT_D = 16;

  parameter N_CYCLE_FP_MISC = 16;

  // constant
  parameter CONST_INST_NOP = 64'h00000013;

  // constant for integer
  parameter CONST_INT32_MIN = 32'h80000000;
  parameter CONST_INT32_MAX = 32'h7fffffff;
  parameter CONST_INT32U_MAX = 32'hffffffff;

  // constant for floating-points
  parameter CONST_FP_D_CANON_NAN  = {1'b0, {11{1'b1}}, 1'b1, 51'b0};
  parameter CONST_FP_D_POS_ZERO   = 64'b0;
  parameter CONST_FP_D_NEG_ZERO   = {1'b1, 63'b0};
  parameter CONST_FP_D_POS_INF    = {1'b0, {11{1'b1}}, 52'b0};
  parameter CONST_FP_D_NEG_INF    = {1'b1, {11{1'b1}}, 52'b0};

  parameter CONST_FP_S_CANON_NAN  = {1'b0, {8{1'b1}}, 1'b1, 22'b0};
  parameter CONST_FP_S_POS_ZERO   = 32'b0;
  parameter CONST_FP_S_NEG_ZERO   = {1'b1, 31'b0};
  parameter CONST_FP_S_POS_INF    = {1'b0, {8{1'b1}}, 23'b0};
  parameter CONST_FP_S_NEG_INF    = {1'b1, {8{1'b1}}, 23'b0};

  // parameter MAGICMEM_BUS_ID   = 6'b10_0001;
  parameter MAGICMEM_FROMHOST_ADDR = 32'h0000_0010;
  parameter MAGICMEM_TOHOST_ADDR   = 32'h0000_0011;
  parameter MAGICMEM_START_ADDR    = 32'h0000_0020;
  parameter MAGICMEM_END_ADDR      = 32'h0000_002F;

  // TCM
  parameter N_TCM_BANKS       = 2;
  parameter TCM_DATA_WIDTH    = 256; // Shared tcm
  parameter TCM_MASK_WIDTH    = 32;
  parameter TCM_ADDR_WIDTH    = 8;
  parameter TCM_ADDR_OFFSET   = 5;
  parameter TCM_BANK_ID_WIDTH = 1;
  parameter TCM_BANK_ID_MSB   = 7;
  parameter TCM_BANK_ID_LSB   = 7;

  parameter ITCM_DATA_WIDTH = 256;
  parameter ITCM_ADDR_WIDTH = 13;
  parameter DTCM_DATA_WIDTH = 256;
  parameter DTCM_ADDR_WIDTH = 13;

  //parameter ITCM_START_ADDR = 32'h1001_0000;
  //parameter ITCM_END_ADDR   = 32'h1001_FFFF;
  //parameter ITCM_ADDR_CMP_WIDTH = 16;
  //parameter DTCM_START_ADDR = 32'h1002_0000;
  //parameter DTCM_END_ADDR   = 32'h1002_FFFF;
  //parameter DTCM_ADDR_CMP_WIDTH = 16;

  // Trace buffer
  parameter ITB_DATA_WIDTH = 32;
  parameter ITB_ADDR_OFFSET_WIDTH = 2;
  parameter ITB_ADDR_WIDTH = 5;

  // REGFILE
  parameter REG_ADDR_WIDTH = 5;

endpackage

`endif
