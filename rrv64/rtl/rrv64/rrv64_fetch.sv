// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*************************************************************************
 * Fetch stage
 **************************************************************************/

module rrv64_fetch
  import rrv64_param_pkg::*;
  import rrv64_typedef_pkg::*;
  import rrv64_func_pkg::*;
(
  // IF -> ID
  output  rrv64_if2id_t       if2id_ff,
  output  rrv64_if2id_ctrl_t  if2id_ctrl_ff,
  output  rrv64_excp_t        if2id_excp_ff,
  output  rrv64_itb_packet_t  rff_if2id_itb_data,
  // IF -> Regfile
  `ifdef RRV64_SUPPORT_FP
  output  rrv64_if2fprf_t     if2fprf,
  `endif
  // IF -> L1I$
  output rrv64_ib2icmem_t     ib2ic,
  input rrv64_icmem2ib_t     ic2ib,
  input  logic           ic_hit, 
                        ic_miss,
  // BPU <-> IF
  output rrv64_vaddr_t          if2bpu_branch_addr,
  // next PC
  input   rrv64_npc_t         id2if_npc, ex2if_npc, cs2if_npc, wb2if_npc, bpu2if_npc,
  input   logic         ma2if_npc_valid,
  input   logic         branch_solved,
  // pipeline ctrl
  input   logic         if_stall, if_kill,
  input   logic         id_ready,
  output  logic         if_ready, if_valid, if_valid_ff,
  output  logic         is_wfe,
  output  logic         if_is_stable_stall,
//  input   rrv64_csr_mstatus_t mstatus,
  // fence
  input   logic                 id2ib_flush_req_valid,
  input   logic                 cs2ib_flush_req_valid,
  input   logic                 id2ic_fence_req_valid,
  output  logic                 inst_buf_idle,
  // ID -> DA
  output  rrv64_if2da_t if2da,
  output  rrv64_inst_t  if_inst,

  // performance counter
  output  logic         wait_for_npc,
  output  logic         wait_for_icr,
  // reset PC
  input   rrv64_vaddr_t       cfg_rst_pc,
  output  rrv64_vaddr_t       if_pc,
  
  // Atomic 
  input  logic 	   dc2if_amo_store_sent, 
  output logic     if_has_amo_inst,
  // prv
  input   rrv64_prv_t                prv,
  input   rrv64_csr_mstatus_t        mstatus,
  input   rrv64_csr_pmpcfg_part_t [RRV64_N_PMP_CSR-1:0] pmpcfg,
  input   rrv64_csr_pmpaddr_t [15:0] pmpaddr,
  // rst & clk
  input   logic         rst, clk

);
  rrv64_if2id_t       if2id;
  rrv64_if2id_ctrl_t  if2id_ctrl;
  rrv64_excp_t        if2id_excp;
  rrv64_itb_packet_t  if2id_itb_data;
  rrv64_ic2if_t       ib2if_saved_ff;

  rrv64_opcode_t      opcode;
  rrv64_reg_addr_t    rd_addr, rs1_addr, rs2_addr, rs3_addr;
  logic [ 1:0]  funct2;
  logic [ 2:0]  funct3;
  logic [ 4:0]  funct5;
  logic [ 5:0]  funct6;
  logic [ 6:0]  funct7;
  logic [11:0]  funct12;
  logic [ 1:0]  fmt;
  rrv64_data_t        i_imm, s_imm, b_imm, u_imm, j_imm, z_imm;
  rrv64_frm_t         frm;
  rrv64_csr_addr_t    csr_addr;

  rrv64_inst_t  ib2if_inst;
  rrv64_inst_t  ib2if_rvc_inst;
  logic             ib2if_is_rvc;
  logic   hold_ib2if, hold_ib2if_ff, if2ib_en_ff, ib2if_valid_hold;
  logic   ib2if_excp_valid;
  rrv64_excp_cause_t   ib2if_excp_cause;
  logic   ib2if_is_half1_excp;
  logic   fp_access_excp, rs1_is_fp, rs2_is_fp;

  //==========================================================
  // PC {{{
  rrv64_vaddr_t   jump_pc;
  rrv64_npc_t     npc, pc_ff, // next PC
            if2if_npc;  // next PC from if
  logic     is_ret_inst;

  assign if_pc = pc_ff.pc;
  always_ff @ (posedge clk) begin
    if (rst) begin
      pc_ff.valid <= '0;
      pc_ff.pc <= {cfg_rst_pc[$bits(cfg_rst_pc)-1:1], 1'b0};
    end else begin
      // PC valid
      if (if_ready) begin
        pc_ff.valid <= if2ib.en;
      end
      // PC
      if (if_ready & if2ib.en) begin
        pc_ff.pc <= if2ib.pc;
      //  pc_ff.pc <= npc.pc;
      end
    end
  end

  assign ib2if_excp_valid = (hold_ib2if_ff ? ib2if_saved_ff.valid & ib2if_saved_ff.excp_valid : ib2if.valid & ib2if.excp_valid);
  assign ib2if_excp_cause = (hold_ib2if_ff ? ib2if_saved_ff.excp_cause : ib2if.excp_cause);
  assign ib2if_is_half1_excp = (hold_ib2if_ff ? ib2if_saved_ff.is_half1_excp : ib2if.is_half1_excp);

  assign is_ret_inst = (if2id.inst[6:2] == RRV64_SYSTEM) & (if2id.inst[14:12] == 3'h0) & (if2id.inst[24:20] == 5'h02) & (if2id.inst[19:15] == 5'h00) & (if2id.inst[11:7] == 5'h00);

  // generate next PC
  assign  if2if_npc.pc = (ib2if_is_rvc) ? (pc_ff.pc + 'h2): (pc_ff.pc + 'h4);
  assign  if2if_npc.valid = if_valid & ~is_ret_inst;
  // Redirect the npc
  assign if2bpu_branch_addr = if_pc;
  always_comb begin
    npc.pc = if2if_npc.pc;
    if (rst) begin
      npc.valid = 1'b0;
    end else if (cs2if_npc.valid & if_kill) begin
      // exception/interrupt happends: higher priority
      npc.pc = cs2if_npc.pc;
      npc.valid = 1'b1;
    end else if (wb2if_npc.valid & if_ready) begin // 
      npc.pc = wb2if_npc.pc;
      npc.valid = 1'b1;
    end else if (ex2if_npc.valid & if_ready) begin // BR, the address is resolved
      // branch taken: second high priority
      npc.pc = ex2if_npc.pc;
      npc.valid = 1'b1;
    end else if (id2if_npc.valid & if_ready) begin // PC is resolved in decode stage(JAL)
      // jal branch taken
      npc.pc = id2if_npc.pc;
      npc.valid = 1'b1;
    end else if (bpu2if_npc.valid & if_ready) begin
      // Branch prediction, tagged BHT, no need for pre-decode
      npc.pc = bpu2if_npc.pc;
      npc.valid = 1'b1;
    end else if ((if2if_npc.valid | ma2if_npc_valid) & if_ready & ~if_kill) begin
      // normal cases
      npc.valid = 1'b1;
    end else begin
      npc.valid = 1'b0;
    end
  end

  // INST_ADDR_MISALIGNED exception
  always_comb begin
    if2id_excp.inst = ib2if_is_rvc ? ib2if_rvc_inst: ib2if_inst;
    if (if_kill) begin
      if2id_excp.valid = 1'b0;
      if2id_excp.cause = RRV64_EXCP_CAUSE_NONE;
      if2id_excp.is_half1 = 1'b0;
    end else
      case (1'b1)
        (ib2if_excp_valid): begin
          if2id_excp.valid = 1'b1;
          if2id_excp.is_half1 = ib2if_is_half1_excp;
          if2id_excp.cause = ib2if_excp_cause;
        end
        (fp_access_excp): begin
          if2id_excp.valid = 1'b1;
          if2id_excp.is_half1 = 1'b0;
          if2id_excp.cause = RRV64_EXCP_CAUSE_ILLEGAL_INST;
        end
        ((pc_ff.pc[0] != 1'b0) & pc_ff.valid): begin 
          if2id_excp.valid = 1'b1;
          if2id_excp.is_half1 = 1'b0;
          if2id_excp.cause = RRV64_EXCP_CAUSE_INST_ADDR_MISALIGNED;
        end
        ((if2id.inst[1:0] != 2'b11) & if_valid): begin
          if2id_excp.valid = 1'b1;
          if2id_excp.is_half1 = 1'b0;
          if2id_excp.cause = RRV64_EXCP_CAUSE_ILLEGAL_INST;
        end
        default: begin
          if2id_excp.valid = 1'b0;
          if2id_excp.is_half1 = 1'b0;
          if2id_excp.cause = RRV64_EXCP_CAUSE_NONE;
        end
      endcase
  end

  // }}}
  //==========================================================
  // instr buffer
  rrv64_if2ic_t       if2ib, temp_if2ib;
  rrv64_ic2if_t       ib2if, temp_ib2if, ib_itb2if;
  rrv64_ic2if_t             itb2if;
  logic                     itb_hit, itb_hit_ff;
  logic                     ib_hit;
  logic                     ib_miss;
  rrv64_ib2da_t             ib2da;
  logic                     ib_kill;
  logic                     itb2ib_hit;
  assign ib_kill = if_kill;
  assign itb2ib_hit = itb_hit;

  rrv64_inst_buffer inst_buf_u (
    .if2ib(itb_hit ? '0 : if2ib),
    .ib2if(temp_ib2if),
    .ic2ib,
    .ib2ic,
    .ib_hit, 
    .ib_miss,
    .l1_hit(ic_hit), 
    .l1_miss(ic_miss),
    .ib_kill,
    .itb2ib_hit,
    .prv,
    .mstatus,
    .pmpcfg,
    .pmpaddr,
    .ib_flush(id2ic_fence_req_valid | id2ib_flush_req_valid | cs2ib_flush_req_valid),
    .ib_idle(inst_buf_idle),
    .da2ib('b0),
    .ib2da,
    .rst, 
    .clk
  );

  rrv64_inst_trace_buffer inst_trace_buf_u (
  .if2ib(if2ib),
  .ib2if(temp_ib2if),

  .itb2if,
  .itb_hit,
  .itb_hit_ff,

  .itb_flush(id2ic_fence_req_valid | id2ib_flush_req_valid | cs2ib_flush_req_valid),

  .rst, 
  .clk
);

  assign ib_itb2if = itb_hit_ff ? itb2if : temp_ib2if;

  rrv64_inst_t expanded_inst;

  rrv64_rvc_inst_build rvc_build_u (
    .rvc_inst(ib_itb2if.inst),
    .full_inst(expanded_inst)
  );

  assign ib2if.valid = ib_itb2if.valid;
  assign ib2if.is_rvc = ~(ib_itb2if.inst[1:0] == 2'b11);
  assign ib2if.inst = ib2if.is_rvc ? expanded_inst: ib_itb2if.inst;
  assign ib2if.rvc_inst = ib_itb2if.inst;
  assign ib2if.excp_valid = ib_itb2if.excp_valid;
  assign ib2if.excp_cause = ib_itb2if.excp_cause;
  assign ib2if.is_half1_excp = ib_itb2if.is_half1_excp;


  //==========================================================
  // Read I-Cache {{{
  logic   hold_if2ib;
  rrv64_if2ic_t if2ib_saved_ff;

  logic         if2vid_vld;
  logic         vid2if_rdy;
  

  assign hold_if2ib = if2ib_en_ff & ~ib2if.valid;
  assign wait_for_icr = hold_if2ib;

  assign hold_ib2if = (~if2ib.en | if_stall | ~id_ready) & ib2if_valid_hold;


  always_ff @ (posedge clk) begin
    if (rst) begin
      if2ib_saved_ff.en <= '1;
      if2ib_saved_ff.pc <= {cfg_rst_pc[$bits(cfg_rst_pc)-1:1], 1'b0};
      hold_ib2if_ff <= '1;
      if2ib_en_ff <= '1;
      ib2if_saved_ff <= '0;
    end else begin
      if (if2ib.en) begin
        if2ib_saved_ff <= if2ib;
      end

      if2ib_en_ff <= if2ib.en;

      if (ib2if.valid) begin
        ib2if_saved_ff <= ib2if;
      end

      hold_ib2if_ff <= hold_ib2if;
    end
  end

  assign  if2ib.pc = hold_if2ib ? if2ib_saved_ff.pc : npc.pc;
  assign  if2ib.en = ~rst & (hold_if2ib ? '1 : npc.valid);

  assign ib2if_inst = hold_ib2if_ff ? ib2if_saved_ff.inst: ib2if.inst;
  assign ib2if_rvc_inst = hold_ib2if_ff ? ib2if_saved_ff.rvc_inst: ib2if.rvc_inst;
  assign ib2if_is_rvc = (hold_ib2if_ff ? ib2if_saved_ff.is_rvc : ib2if.is_rvc);

  assign  if2id.inst = ib2if_inst;
  assign  if2id.is_rvc = ib2if_is_rvc;
  assign  ib2if_valid_hold = if_kill ? 'b0 : (hold_ib2if_ff ? '1 : ib2if.valid);
  // }}}

  //------------------------------------------------------
  // WFE
  logic is_wfe_inst;
  assign is_wfe_inst = ((ib2if_inst == RRV64_CONST_INST_WFE) & ib2if_valid_hold & ~if_kill);
  always_comb begin
    if (is_wfe_inst)
      is_wfe = '1;
    else
      is_wfe = '0;
  end

  //==========================================================
  // pipeline ctrl {{{

  assign if_valid = ~if_kill & (pc_ff.valid & ib2if_valid_hold) & ~rst & ~if_stall;
  assign if_ready = (id_ready | ~if_valid) & (ib2if_valid_hold | (~ib2if_valid_hold & ~if2ib_en_ff)) & ~if_stall /*& ~amo_fsm_is_active*/;

  assign if_is_stable_stall = ((if_valid & ~if_ready) | wait_for_npc) & ~if2ib.en;

  assign if2da.if_amo_st = '0;
  assign if2da.wait_for_branch = 1'b0;
  assign if_inst = if2id.inst;

  always_ff @ (posedge clk)
    if (rst) begin
      if_valid_ff <= 1'b0;
    end else begin
      if (id_ready) begin
        if_valid_ff <= if_valid;
      end
    end  
      
`ifndef SYNTHESIS
  logic [63:0] if_status;
  always_comb
    case ({if_valid, if_ready})
      2'b00: if_status = "BUSY";
      2'b01: if_status = "IDLE";
      2'b10: if_status = "BLOCK";
      2'b11: if_status = "DONE";
      default: if_status = 64'bx;
    endcase
`endif
  // }}}

  //==========================================================
  // ITB Stack trace {{{

  assign if2id_itb_data.push_to_stack = 'b0;//(opcode == RRV64_JAL) & ~(rd_addr == 5'h00);
  assign if2id_itb_data.data.vpc = 'b0;//jump_pc;

  // }}}

  //==========================================================
  // output {{{
  assign if2id.pc = pc_ff.pc;
  always_ff @ (posedge clk) begin
    if (rst | ((if_kill | if2id_excp.valid) & id_ready)) begin
      if2id_ff.inst <= RRV64_CONST_INST_NOP;
      if2id_ff.pc <= if2id.pc;
      if2id_ff.bpred <= 0;
      if2id_ctrl_ff.is_rs1_fp <= 1'b0;
      if2id_ctrl_ff.is_rs2_fp <= 1'b0;
      if2id_ctrl_ff.rs1_re <= 1'b0;
      if2id_ctrl_ff.rs2_re <= 1'b0;
      if2id_ctrl_ff.rs3_re <= 1'b0;
      if2id_ctrl_ff.is_rs1_x0 <= 1'b1;
      if2id_ctrl_ff.is_rs2_x0 <= 1'b1;

      rff_if2id_itb_data.push_to_stack <= 1'b0;
    end else begin 
      if (id_ready & if_valid) begin
        if2id_ff <= if2id;
        if2id_ctrl_ff <= if2id_ctrl; // spyglass disable W123
        rff_if2id_itb_data <= if2id_itb_data;
      end
    end

    if (rst | (if_kill & id_ready)) begin
      if2id_excp_ff.valid <= 1'b0;
      if2id_excp_ff.is_half1 <= 1'b0;
      if2id_excp_ff.cause <= RRV64_EXCP_CAUSE_NONE;
    end else if (id_ready & if_valid) begin
      if2id_excp_ff <= if2id_excp;
    end
  end
  assign wait_for_npc =~ npc.valid;

`ifndef SYNTHESIS
`ifndef VERILATOR
  chk_npc_valid_not_unknown: assert property (@ (posedge clk) disable iff (rst !== '0) (!$isunknown(if2if_npc.valid) && !$isunknown(ex2if_npc.valid) && !$isunknown(cs2if_npc.valid) && !$isunknown(ma2if_npc_valid))) else `olog_warning("RRV_FETCH", "one of the npc.valid is unknown, it will stall the creation of NPC");
  chk_if_valid_non_x: assert property (@(posedge clk) disable iff (rst !== '0) (!$isunknown(if_valid))) else `olog_fatal("RRV_FETCH", $sformatf("%m: if_vald is X after being reset"));
  chk_pc_non_x: assert property (@(posedge clk) disable iff (rst !== '0) (if2ib.en |-> (!$isunknown(if2ib.pc)))) else `olog_error("RRV_FETCH", $sformatf("%m: PC is unknown"));
  chk_inst_non_x: assert property (@(posedge clk) disable iff (rst !== '0) (if_valid |-> (!$isunknown(if2id.inst)))) else `olog_warning("RRV_FETCH", $sformatf("%m: inst is unknown, it's only OK when it's been killed later"));
`endif
`endif
  // }}}

logic is_here_pc;
assign is_here_pc = if_pc == 38'h80001cd4;

endmodule
