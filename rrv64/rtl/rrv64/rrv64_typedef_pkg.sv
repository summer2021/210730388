// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*************************************************************************
 * Type define packages
 **************************************************************************/
package rrv64_typedef_pkg;

  import soc_cfg::*;
  import soc_typedef::*;
  import rrv64_param_pkg::*;

  typedef logic [RRV64_XLEN-1:0]           rrv64_data_t; // data
  typedef logic [31:0]                     rrv64_data32_t; // 32-bit data
  typedef logic [RRV64_VIR_ADDR_WIDTH-1:0] rrv64_vaddr_t; // virtual address
  typedef logic [RRV64_PHY_ADDR_WIDTH-1:0] rrv64_paddr_t; // physical address
  typedef logic [31:0]                     rrv64_inst_t; // instruction
  typedef logic [4:0]                      rrv64_reg_addr_t; // register file address
  typedef logic [7:0]                      rrv64_data_byte_mask_t; // data
  typedef logic [RRV64_CNTR_WIDTH-1:0]     rrv64_cntr_t;

  typedef enum logic [1:0] {
    RRV64_NPC_AVAIL_IF = 2'h0,
    RRV64_NPC_AVAIL_ID = 2'h1,
    RRV64_NPC_AVAIL_EX = 2'h2,
    RRV64_NPC_AVAIL_MA = 2'h3
  } rrv64_npc_avail_t;

  typedef enum logic {
    RRV64_RD_AVAIL_EX,
    RRV64_RD_AVAIL_MA
  } rrv64_rd_avail_t;

  typedef enum logic [4:0] { // {{{
    RRV64_LOAD      = 5'h00,
    RRV64_MISC_MEM  = 5'h03,
    RRV64_OP_IMM    = 5'h04,
    RRV64_AUIPC     = 5'h05,
    RRV64_OP_IMM_32 = 5'h06,
    RRV64_STORE     = 5'h08,
    // AMO
    RRV64_AMO		= 5'h0B,   // LR.D, SC.D, AMOSWAP.D, AMOADD.D, AMOXOR.D, AMOAND.D, AMOOR.D, AMOMIN.D, AMOMAX.D, AMOMINU.D, AMOMAXU.D 
    RRV64_OP        = 5'h0C,
    RRV64_LUI       = 5'h0D,
    RRV64_OP_32     = 5'h0E,
    RRV64_BRANCH    = 5'h18,
    RRV64_JALR      = 5'h19,
    RRV64_JAL       = 5'h1B,
    RRV64_SYSTEM    = 5'h1C,
    RRV64_LOAD_FP   = 5'h01,
    RRV64_STORE_FP  = 5'h09,
    RRV64_OP_FP     = 5'h14,
    RRV64_FMADD     = 5'h10,
    RRV64_FMSUB     = 5'h11,
    RRV64_FNMADD    = 5'h13,
    RRV64_FNMSUB    = 5'h12
    // }}}
  } rrv64_opcode_t;

  typedef enum logic [4:0] { // {{{
    RRV64_ALU_OP_ADD    = 5'h00,
    RRV64_ALU_OP_SLL    = 5'h01,
    RRV64_ALU_OP_SEQ    = 5'h02,
    RRV64_ALU_OP_SNE    = 5'h03,
    RRV64_ALU_OP_XOR    = 5'h04,
    RRV64_ALU_OP_SRL    = 5'h05,
    RRV64_ALU_OP_OR     = 5'h06,
    RRV64_ALU_OP_AND    = 5'h07,
    RRV64_ALU_OP_SUB    = 5'h0A,
    RRV64_ALU_OP_SRA    = 5'h0B,
    RRV64_ALU_OP_SLT    = 5'h0C,
    RRV64_ALU_OP_SGE    = 5'h0D,
    RRV64_ALU_OP_SLTU   = 5'h0E,
    RRV64_ALU_OP_SGEU   = 5'h0F,
    RRV64_ALU_OP_MIN    = 5'h10,
    RRV64_ALU_OP_MAX    = 5'h11,
    RRV64_ALU_OP_MIN_U  = 5'h12,
    RRV64_ALU_OP_MAX_U  = 5'h13,
    RRV64_ALU_OP_SWAP   = 5'h14
    // }}}
  } rrv64_alu_op_t;

  typedef enum logic [1:0] { // {{{
    RRV64_OP1_SEL_RS1,
    RRV64_OP1_SEL_FP_RS1,
    RRV64_OP1_SEL_FP_RS1_SEXT,
    RRV64_OP1_SEL_ZERO // }}}
  } rrv64_op1_sel_t;

  typedef enum logic [1:0]{ // {{{
    RRV64_OP2_SEL_0,
    RRV64_OP2_SEL_RS2,
    RRV64_OP2_SEL_IMM // }}}
  } rrv64_op2_sel_t;

  typedef enum logic [2:0] { // {{{
    RRV64_IMM_SEL_I,
    RRV64_IMM_SEL_S,
    RRV64_IMM_SEL_B,
    RRV64_IMM_SEL_U,
    RRV64_IMM_SEL_J,
    RRV64_IMM_SEL_Z,
    RRV64_IMM_SEL_F0,
    RRV64_IMM_SEL_ZERO // }}}
  } rrv64_imm_sel_t;

  typedef enum logic { // {{{
    RRV64_EX_PC_SEL_P4,
    RRV64_EX_PC_SEL_IMM // }}}
  } rrv64_ex_pc_sel_t;

  typedef enum logic [3:0] { // {{{
    RRV64_EX_OUT_SEL_RS1,
    RRV64_EX_OUT_SEL_ALU_OUT,
    RRV64_EX_OUT_SEL_PC_ADDER,
    RRV64_EX_OUT_SEL_MUL_L,
    RRV64_EX_OUT_SEL_MUL_H,
    RRV64_EX_OUT_SEL_DIV_Q,
    RRV64_EX_OUT_SEL_DIV_R,
    RRV64_EX_OUT_SEL_FP,
    RRV64_EX_OUT_SEL_FP_MV,
    RRV64_EX_OUT_SEL_NONE // }}}
  } rrv64_ex_out_sel_t;

  typedef enum logic [2:0] { // {{{
    RRV64_MA_BYTE_SEL_1   = 3'h0,
    RRV64_MA_BYTE_SEL_U1  = 3'h1,
    RRV64_MA_BYTE_SEL_2   = 3'h2,
    RRV64_MA_BYTE_SEL_U2  = 3'h3,
    RRV64_MA_BYTE_SEL_4   = 3'h4,
    RRV64_MA_BYTE_SEL_U4  = 3'h5,
    RRV64_MA_BYTE_SEL_8   = 3'h6
    // }}}
  } rrv64_ma_byte_sel_t;

  typedef enum logic [2:0] { // {{{
    RRV64_MUL_TYPE_L,
    RRV64_MUL_TYPE_HSS,
    RRV64_MUL_TYPE_HUU,
    RRV64_MUL_TYPE_HSU,
    RRV64_MUL_TYPE_W,
    RRV64_MUL_TYPE_NONE // }}}
  } rrv64_mul_type_t;

  typedef enum logic [3:0] { // {{{
    RRV64_DIV_TYPE_Q,
    RRV64_DIV_TYPE_R,
    RRV64_DIV_TYPE_QU,
    RRV64_DIV_TYPE_RU,
    RRV64_DIV_TYPE_QW,
    RRV64_DIV_TYPE_RW,
    RRV64_DIV_TYPE_QUW,
    RRV64_DIV_TYPE_RUW,
    RRV64_DIV_TYPE_NONE // }}}
  } rrv64_div_type_t;

  typedef enum logic [1:0] { // {{{
    RRV64_RD_SEL_EX_OUT,
    RRV64_RD_SEL_MEM_READ,
    RRV64_RD_SEL_CSR_READ // }}}
  } rrv64_rd_sel_t;

  typedef enum logic [1:0] { // {{{
    RRV64_RET_TYPE_U,
    RRV64_RET_TYPE_S,
    RRV64_RET_TYPE_M,
    RRV64_RET_TYPE_NONE // }}}
  } rrv64_ret_type_t;

  typedef logic [RRV64_CSR_ADDR_RW_PRV_WIDTH-1:0] rrv64_csr_addr_rw_prv_field_t;

  typedef enum logic [11:0] { // {{{
    // non-isa defined CSR  {{{
    RRV64_CSR_ADDR_CFG              = 12'h600,
    RRV64_CSR_ADDR_TIMER            = 12'h601,

`ifdef RRV64_SUPPORT_DMA
    RRV64_CSR_ADDR_DMA_DONE         = 12'h602,
`endif

`ifdef RRV64_SUPPORT_UART
    RRV64_CSR_ADDR_TO_UART          = 12'h610,
    RRV64_CSR_ADDR_FROM_UART        = 12'h611,
`endif

    // isa defined CSR {{{
    
    // M mode accessible registers
    RRV64_CSR_ADDR_MSTATUS         = 12'h300,
    RRV64_CSR_ADDR_MISA            = 12'h301,
    RRV64_CSR_ADDR_MEDELEG         = 12'h302,
    RRV64_CSR_ADDR_MIDELEG         = 12'h303,
    RRV64_CSR_ADDR_MIE             = 12'h304,
    RRV64_CSR_ADDR_MTVEC           = 12'h305,
    RRV64_CSR_ADDR_MCOUNTEREN      = 12'h306,
    RRV64_CSR_ADDR_MSCRATCH        = 12'h340,
    RRV64_CSR_ADDR_MEPC            = 12'h341,
    RRV64_CSR_ADDR_MCAUSE          = 12'h342,
    RRV64_CSR_ADDR_MTVAL           = 12'h343,
    RRV64_CSR_ADDR_MIP             = 12'h344,
    RRV64_CSR_ADDR_PMPCFG0         = 12'h3A0,
    RRV64_CSR_ADDR_PMPCFG1         = 12'h3A1,
    RRV64_CSR_ADDR_PMPCFG2         = 12'h3A2,
    RRV64_CSR_ADDR_PMPCFG3         = 12'h3A3,
    RRV64_CSR_ADDR_PMPADDR0        = 12'h3B0,
    RRV64_CSR_ADDR_PMPADDR1        = 12'h3B1,
    RRV64_CSR_ADDR_PMPADDR2        = 12'h3B2,
    RRV64_CSR_ADDR_PMPADDR3        = 12'h3B3,
    RRV64_CSR_ADDR_PMPADDR4        = 12'h3B4,
    RRV64_CSR_ADDR_PMPADDR5        = 12'h3B5,
    RRV64_CSR_ADDR_PMPADDR6        = 12'h3B6,
    RRV64_CSR_ADDR_PMPADDR7        = 12'h3B7,
    RRV64_CSR_ADDR_PMPADDR8        = 12'h3B8,
    RRV64_CSR_ADDR_PMPADDR9        = 12'h3B9,
    RRV64_CSR_ADDR_PMPADDR10       = 12'h3BA,
    RRV64_CSR_ADDR_PMPADDR11       = 12'h3BB,
    RRV64_CSR_ADDR_PMPADDR12       = 12'h3BC,
    RRV64_CSR_ADDR_PMPADDR13       = 12'h3BD,
    RRV64_CSR_ADDR_PMPADDR14       = 12'h3BE,
    RRV64_CSR_ADDR_PMPADDR15       = 12'h3BF,
    RRV64_CSR_ADDR_MCOUNTINHIBIT   = 12'h320,
    RRV64_CSR_ADDR_MHPMEVENT3      = 12'h323,
    RRV64_CSR_ADDR_MHPMEVENT4      = 12'h324,
    RRV64_CSR_ADDR_MHPMEVENT5      = 12'h325,
    RRV64_CSR_ADDR_MHPMEVENT6      = 12'h326,
    RRV64_CSR_ADDR_MHPMEVENT7      = 12'h327,
    RRV64_CSR_ADDR_MHPMEVENT8      = 12'h328,
    RRV64_CSR_ADDR_MHPMEVENT9      = 12'h329,
    RRV64_CSR_ADDR_MHPMEVENT10     = 12'h32A,
    RRV64_CSR_ADDR_MHPMEVENT11     = 12'h32B,
    RRV64_CSR_ADDR_MHPMEVENT12     = 12'h32C,
    RRV64_CSR_ADDR_MHPMEVENT13     = 12'h32D,
    RRV64_CSR_ADDR_MHPMEVENT14     = 12'h32E,
    RRV64_CSR_ADDR_MHPMEVENT15     = 12'h32F,
    RRV64_CSR_ADDR_MHPMEVENT16     = 12'h330,
    RRV64_CSR_ADDR_MHPMEVENT17     = 12'h331,
    RRV64_CSR_ADDR_MHPMEVENT18     = 12'h332,
    RRV64_CSR_ADDR_MHPMEVENT19     = 12'h333,
    RRV64_CSR_ADDR_MHPMEVENT20     = 12'h334,
    RRV64_CSR_ADDR_MHPMEVENT21     = 12'h335,
    RRV64_CSR_ADDR_MHPMEVENT22     = 12'h336,
    RRV64_CSR_ADDR_MHPMEVENT23     = 12'h337,
    RRV64_CSR_ADDR_MHPMEVENT24     = 12'h338,
    RRV64_CSR_ADDR_MHPMEVENT25     = 12'h339,
    RRV64_CSR_ADDR_MHPMEVENT26     = 12'h33A,
    RRV64_CSR_ADDR_MHPMEVENT27     = 12'h33B,
    RRV64_CSR_ADDR_MHPMEVENT28     = 12'h33C,
    RRV64_CSR_ADDR_MHPMEVENT29     = 12'h33D,
    RRV64_CSR_ADDR_MHPMEVENT30     = 12'h33E,
    RRV64_CSR_ADDR_MHPMEVENT31     = 12'h33F,
    RRV64_CSR_ADDR_TSELECT         = 12'h7A0,
    RRV64_CSR_ADDR_TDATA1          = 12'h7A1,
    RRV64_CSR_ADDR_TDATA2          = 12'h7A2,
    RRV64_CSR_ADDR_TDATA3          = 12'h7A3,
    RRV64_CSR_ADDR_DCSR            = 12'h7B0,
    RRV64_CSR_ADDR_DPC             = 12'h7B1,
    RRV64_CSR_ADDR_DSCRATCH        = 12'h7B2,
    RRV64_CSR_ADDR_MCYCLE          = 12'hB00,
    RRV64_CSR_ADDR_MINSTRET        = 12'hB02,
    RRV64_CSR_ADDR_MHPMCOUNTER3    = 12'hB03,
    RRV64_CSR_ADDR_MHPMCOUNTER4    = 12'hB04,
    RRV64_CSR_ADDR_MHPMCOUNTER5    = 12'hB05,
    RRV64_CSR_ADDR_MHPMCOUNTER6    = 12'hB06,
    RRV64_CSR_ADDR_MHPMCOUNTER7    = 12'hB07,
    RRV64_CSR_ADDR_MHPMCOUNTER8    = 12'hB08,
    RRV64_CSR_ADDR_MHPMCOUNTER9    = 12'hB09,
    RRV64_CSR_ADDR_MHPMCOUNTER10   = 12'hB0A,
    RRV64_CSR_ADDR_MHPMCOUNTER11   = 12'hB0B,
    RRV64_CSR_ADDR_MHPMCOUNTER12   = 12'hB0C,
    RRV64_CSR_ADDR_MHPMCOUNTER13   = 12'hB0D,
    RRV64_CSR_ADDR_MHPMCOUNTER14   = 12'hB0E,
    RRV64_CSR_ADDR_MHPMCOUNTER15   = 12'hB0F,
    RRV64_CSR_ADDR_MHPMCOUNTER16   = 12'hB10,
    RRV64_CSR_ADDR_MHPMCOUNTER17   = 12'hB11,
    RRV64_CSR_ADDR_MHPMCOUNTER18   = 12'hB12,
    RRV64_CSR_ADDR_MHPMCOUNTER19   = 12'hB13,
    RRV64_CSR_ADDR_MHPMCOUNTER20   = 12'hB14,
    RRV64_CSR_ADDR_MHPMCOUNTER21   = 12'hB15,
    RRV64_CSR_ADDR_MHPMCOUNTER22   = 12'hB16,
    RRV64_CSR_ADDR_MHPMCOUNTER23   = 12'hB17,
    RRV64_CSR_ADDR_MHPMCOUNTER24   = 12'hB18,
    RRV64_CSR_ADDR_MHPMCOUNTER25   = 12'hB19,
    RRV64_CSR_ADDR_MHPMCOUNTER26   = 12'hB1A,
    RRV64_CSR_ADDR_MHPMCOUNTER27   = 12'hB1B,
    RRV64_CSR_ADDR_MHPMCOUNTER28   = 12'hB1C,
    RRV64_CSR_ADDR_MHPMCOUNTER29   = 12'hB1D,
    RRV64_CSR_ADDR_MHPMCOUNTER30   = 12'hB1E,
    RRV64_CSR_ADDR_MHPMCOUNTER31   = 12'hB1F,
    RRV64_CSR_ADDR_MVENDORID       = 12'hF11,
    RRV64_CSR_ADDR_MARCHID         = 12'hF12,
    RRV64_CSR_ADDR_MIMPID          = 12'hF13,
    RRV64_CSR_ADDR_MHARTID         = 12'hF14,

    // S mode accessible csrs
    RRV64_CSR_ADDR_SSTATUS         = 12'h100,
    RRV64_CSR_ADDR_SEDELEG         = 12'h102,
    RRV64_CSR_ADDR_SIDELEG         = 12'h103,
    RRV64_CSR_ADDR_SIE             = 12'h104,
    RRV64_CSR_ADDR_STVEC           = 12'h105,
    RRV64_CSR_ADDR_SCOUNTEREN      = 12'h106,
    RRV64_CSR_ADDR_SSCRATCH        = 12'h140,
    RRV64_CSR_ADDR_SEPC            = 12'h141,
    RRV64_CSR_ADDR_SCAUSE          = 12'h142,
    RRV64_CSR_ADDR_STVAL           = 12'h143,
    RRV64_CSR_ADDR_SIP             = 12'h144,
    RRV64_CSR_ADDR_SATP            = 12'h180,

    // U mode accessible csrs
    RRV64_CSR_ADDR_USTATUS         = 12'h000,
    RRV64_CSR_ADDR_UIE             = 12'h004,
    RRV64_CSR_ADDR_UTVEC           = 12'h005,
    RRV64_CSR_ADDR_USCRATCH        = 12'h040,
    RRV64_CSR_ADDR_UEPC            = 12'h041,
    RRV64_CSR_ADDR_UCAUSE          = 12'h042,
    RRV64_CSR_ADDR_UTVAL           = 12'h043,
    RRV64_CSR_ADDR_UIP             = 12'h044,
    RRV64_CSR_ADDR_FFLAGS          = 12'h001,
    RRV64_CSR_ADDR_FRM             = 12'h002,
    RRV64_CSR_ADDR_FCSR            = 12'h003,
    RRV64_CSR_ADDR_CYCLE           = 12'hC00,
    RRV64_CSR_ADDR_TIME            = 12'hC01,
    RRV64_CSR_ADDR_INSTRET         = 12'hC02,
    RRV64_CSR_ADDR_HPMCOUNTER3     = 12'hC03,
    RRV64_CSR_ADDR_HPMCOUNTER4     = 12'hC04,
    RRV64_CSR_ADDR_HPMCOUNTER5     = 12'hC05,
    RRV64_CSR_ADDR_HPMCOUNTER6     = 12'hC06,
    RRV64_CSR_ADDR_HPMCOUNTER7     = 12'hC07,
    RRV64_CSR_ADDR_HPMCOUNTER8     = 12'hC08,
    RRV64_CSR_ADDR_HPMCOUNTER9     = 12'hC09,
    RRV64_CSR_ADDR_HPMCOUNTER10    = 12'hC0A,
    RRV64_CSR_ADDR_HPMCOUNTER11    = 12'hC0B,
    RRV64_CSR_ADDR_HPMCOUNTER12    = 12'hC0C,
    RRV64_CSR_ADDR_HPMCOUNTER13    = 12'hC0D,
    RRV64_CSR_ADDR_HPMCOUNTER14    = 12'hC0E,
    RRV64_CSR_ADDR_HPMCOUNTER15    = 12'hC0F,
    RRV64_CSR_ADDR_HPMCOUNTER16    = 12'hC10,
    RRV64_CSR_ADDR_HPMCOUNTER17    = 12'hC11,
    RRV64_CSR_ADDR_HPMCOUNTER18    = 12'hC12,
    RRV64_CSR_ADDR_HPMCOUNTER19    = 12'hC13,
    RRV64_CSR_ADDR_HPMCOUNTER20    = 12'hC14,
    RRV64_CSR_ADDR_HPMCOUNTER21    = 12'hC15,
    RRV64_CSR_ADDR_HPMCOUNTER22    = 12'hC16,
    RRV64_CSR_ADDR_HPMCOUNTER23    = 12'hC17,
    RRV64_CSR_ADDR_HPMCOUNTER24    = 12'hC18,
    RRV64_CSR_ADDR_HPMCOUNTER25    = 12'hC19,
    RRV64_CSR_ADDR_HPMCOUNTER26    = 12'hC1A,
    RRV64_CSR_ADDR_HPMCOUNTER27    = 12'hC1B,
    RRV64_CSR_ADDR_HPMCOUNTER28    = 12'hC1C,
    RRV64_CSR_ADDR_HPMCOUNTER29    = 12'hC1D,
    RRV64_CSR_ADDR_HPMCOUNTER30    = 12'hC1E,
    RRV64_CSR_ADDR_HPMCOUNTER31    = 12'hC1F // }}}
  } rrv64_csr_addr_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
    RRV64_CSR_GRP0_USTATUS         = 8'h00,
    RRV64_CSR_GRP0_UIE             = 8'h04,
    RRV64_CSR_GRP0_UTVEC           = 8'h05,
    RRV64_CSR_GRP0_USCRATCH        = 8'h40,
    RRV64_CSR_GRP0_UEPC            = 8'h41,
    RRV64_CSR_GRP0_UCAUSE          = 8'h42,
    RRV64_CSR_GRP0_UTVAL           = 8'h43,
    RRV64_CSR_GRP0_UIP             = 8'h44,
    RRV64_CSR_GRP0_FFLAGS          = 8'h01,
    RRV64_CSR_GRP0_FRM             = 8'h02,
    RRV64_CSR_GRP0_FCSR            = 8'h03
  } rrv64_csr_grp0_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
    RRV64_CSR_GRP1_SSTATUS         = 8'h00,
    RRV64_CSR_GRP1_SEDELEG         = 8'h02,
    RRV64_CSR_GRP1_SIDELEG         = 8'h03,
    RRV64_CSR_GRP1_SIE             = 8'h04,
    RRV64_CSR_GRP1_STVEC           = 8'h05,
    RRV64_CSR_GRP1_SCOUNTEREN      = 8'h06,
    RRV64_CSR_GRP1_SSCRATCH        = 8'h40,
    RRV64_CSR_GRP1_SEPC            = 8'h41,
    RRV64_CSR_GRP1_SCAUSE          = 8'h42,
    RRV64_CSR_GRP1_STVAL           = 8'h43,
    RRV64_CSR_GRP1_SIP             = 8'h44,
    RRV64_CSR_GRP1_SATP            = 8'h80
  } rrv64_csr_grp1_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
    RRV64_CSR_GRP3_MSTATUS         = 8'h00,
    RRV64_CSR_GRP3_MISA            = 8'h01,
    RRV64_CSR_GRP3_MEDELEG         = 8'h02,
    RRV64_CSR_GRP3_MIDELEG         = 8'h03,
    RRV64_CSR_GRP3_MIE             = 8'h04,
    RRV64_CSR_GRP3_MTVEC           = 8'h05,
    RRV64_CSR_GRP3_MCOUNTEREN      = 8'h06,
    RRV64_CSR_GRP3_MSCRATCH        = 8'h40,
    RRV64_CSR_GRP3_MEPC            = 8'h41,
    RRV64_CSR_GRP3_MCAUSE          = 8'h42,
    RRV64_CSR_GRP3_MTVAL           = 8'h43,
    RRV64_CSR_GRP3_MIP             = 8'h44,
    RRV64_CSR_GRP3_PMPCFG0         = 8'hA0,
    RRV64_CSR_GRP3_PMPCFG1         = 8'hA1,
    RRV64_CSR_GRP3_PMPCFG2         = 8'hA2,
    RRV64_CSR_GRP3_PMPCFG3         = 8'hA3,
    RRV64_CSR_GRP3_PMPADDR0        = 8'hB0,
    RRV64_CSR_GRP3_PMPADDR1        = 8'hB1,
    RRV64_CSR_GRP3_PMPADDR2        = 8'hB2,
    RRV64_CSR_GRP3_PMPADDR3        = 8'hB3,
    RRV64_CSR_GRP3_PMPADDR4        = 8'hB4,
    RRV64_CSR_GRP3_PMPADDR5        = 8'hB5,
    RRV64_CSR_GRP3_PMPADDR6        = 8'hB6,
    RRV64_CSR_GRP3_PMPADDR7        = 8'hB7,
    RRV64_CSR_GRP3_PMPADDR8        = 8'hB8,
    RRV64_CSR_GRP3_PMPADDR9        = 8'hB9,
    RRV64_CSR_GRP3_PMPADDR10       = 8'hBA,
    RRV64_CSR_GRP3_PMPADDR11       = 8'hBB,
    RRV64_CSR_GRP3_PMPADDR12       = 8'hBC,
    RRV64_CSR_GRP3_PMPADDR13       = 8'hBD,
    RRV64_CSR_GRP3_PMPADDR14       = 8'hBE,
    RRV64_CSR_GRP3_PMPADDR15       = 8'hBF,
    RRV64_CSR_GRP3_MCOUNTINHIBIT   = 8'h20,
    RRV64_CSR_GRP3_MHPMEVENT3      = 8'h23,
    RRV64_CSR_GRP3_MHPMEVENT4      = 8'h24,
    RRV64_CSR_GRP3_MHPMEVENT5      = 8'h25,
    RRV64_CSR_GRP3_MHPMEVENT6      = 8'h26,
    RRV64_CSR_GRP3_MHPMEVENT7      = 8'h27,
    RRV64_CSR_GRP3_MHPMEVENT8      = 8'h28,
    RRV64_CSR_GRP3_MHPMEVENT9      = 8'h29,
    RRV64_CSR_GRP3_MHPMEVENT10     = 8'h2A,
    RRV64_CSR_GRP3_MHPMEVENT11     = 8'h2B,
    RRV64_CSR_GRP3_MHPMEVENT12     = 8'h2C,
    RRV64_CSR_GRP3_MHPMEVENT13     = 8'h2D,
    RRV64_CSR_GRP3_MHPMEVENT14     = 8'h2E,
    RRV64_CSR_GRP3_MHPMEVENT15     = 8'h2F,
    RRV64_CSR_GRP3_MHPMEVENT16     = 8'h30,
    RRV64_CSR_GRP3_MHPMEVENT17     = 8'h31,
    RRV64_CSR_GRP3_MHPMEVENT18     = 8'h32,
    RRV64_CSR_GRP3_MHPMEVENT19     = 8'h33,
    RRV64_CSR_GRP3_MHPMEVENT20     = 8'h34,
    RRV64_CSR_GRP3_MHPMEVENT21     = 8'h35,
    RRV64_CSR_GRP3_MHPMEVENT22     = 8'h36,
    RRV64_CSR_GRP3_MHPMEVENT23     = 8'h37,
    RRV64_CSR_GRP3_MHPMEVENT24     = 8'h38,
    RRV64_CSR_GRP3_MHPMEVENT25     = 8'h39,
    RRV64_CSR_GRP3_MHPMEVENT26     = 8'h3A,
    RRV64_CSR_GRP3_MHPMEVENT27     = 8'h3B,
    RRV64_CSR_GRP3_MHPMEVENT28     = 8'h3C,
    RRV64_CSR_GRP3_MHPMEVENT29     = 8'h3D,
    RRV64_CSR_GRP3_MHPMEVENT30     = 8'h3E,
    RRV64_CSR_GRP3_MHPMEVENT31     = 8'h3F
  } rrv64_csr_grp3_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
`ifdef RRV64_SUPPORT_DMA
    RRV64_CSR_GRP6_DMA_DONE         = 8'h02,
`endif

`ifdef RRV64_SUPPORT_UART
    RRV64_CSR_GRP6_TO_UART          = 8'h10,
    RRV64_CSR_GRP6_FROM_UART        = 8'h11,
`endif
    RRV64_CSR_GRP6_CFG              = 8'h00,
    RRV64_CSR_GRP6_TIMER            = 8'h01
  } rrv64_csr_grp6_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
    RRV64_CSR_GRPB_MCYCLE          = 8'h00,
    RRV64_CSR_GRPB_MINSTRET        = 8'h02,
    RRV64_CSR_GRPB_MHPMCOUNTER3    = 8'h03,
    RRV64_CSR_GRPB_MHPMCOUNTER4    = 8'h04,
    RRV64_CSR_GRPB_MHPMCOUNTER5    = 8'h05,
    RRV64_CSR_GRPB_MHPMCOUNTER6    = 8'h06,
    RRV64_CSR_GRPB_MHPMCOUNTER7    = 8'h07,
    RRV64_CSR_GRPB_MHPMCOUNTER8    = 8'h08,
    RRV64_CSR_GRPB_MHPMCOUNTER9    = 8'h09,
    RRV64_CSR_GRPB_MHPMCOUNTER10   = 8'h0A,
    RRV64_CSR_GRPB_MHPMCOUNTER11   = 8'h0B,
    RRV64_CSR_GRPB_MHPMCOUNTER12   = 8'h0C,
    RRV64_CSR_GRPB_MHPMCOUNTER13   = 8'h0D,
    RRV64_CSR_GRPB_MHPMCOUNTER14   = 8'h0E,
    RRV64_CSR_GRPB_MHPMCOUNTER15   = 8'h0F,
    RRV64_CSR_GRPB_MHPMCOUNTER16   = 8'h10,
    RRV64_CSR_GRPB_MHPMCOUNTER17   = 8'h11,
    RRV64_CSR_GRPB_MHPMCOUNTER18   = 8'h12,
    RRV64_CSR_GRPB_MHPMCOUNTER19   = 8'h13,
    RRV64_CSR_GRPB_MHPMCOUNTER20   = 8'h14,
    RRV64_CSR_GRPB_MHPMCOUNTER21   = 8'h15,
    RRV64_CSR_GRPB_MHPMCOUNTER22   = 8'h16,
    RRV64_CSR_GRPB_MHPMCOUNTER23   = 8'h17,
    RRV64_CSR_GRPB_MHPMCOUNTER24   = 8'h18,
    RRV64_CSR_GRPB_MHPMCOUNTER25   = 8'h19,
    RRV64_CSR_GRPB_MHPMCOUNTER26   = 8'h1A,
    RRV64_CSR_GRPB_MHPMCOUNTER27   = 8'h1B,
    RRV64_CSR_GRPB_MHPMCOUNTER28   = 8'h1C,
    RRV64_CSR_GRPB_MHPMCOUNTER29   = 8'h1D,
    RRV64_CSR_GRPB_MHPMCOUNTER30   = 8'h1E,
    RRV64_CSR_GRPB_MHPMCOUNTER31   = 8'h1F
  } rrv64_csr_grpb_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
    RRV64_CSR_GRPC_CYCLE           = 8'h00,
    RRV64_CSR_GRPC_TIME            = 8'h01,
    RRV64_CSR_GRPC_INSTRET         = 8'h02,
    RRV64_CSR_GRPC_HPMCOUNTER3     = 8'h03,
    RRV64_CSR_GRPC_HPMCOUNTER4     = 8'h04,
    RRV64_CSR_GRPC_HPMCOUNTER5     = 8'h05,
    RRV64_CSR_GRPC_HPMCOUNTER6     = 8'h06,
    RRV64_CSR_GRPC_HPMCOUNTER7     = 8'h07,
    RRV64_CSR_GRPC_HPMCOUNTER8     = 8'h08,
    RRV64_CSR_GRPC_HPMCOUNTER9     = 8'h09,
    RRV64_CSR_GRPC_HPMCOUNTER10    = 8'h0A,
    RRV64_CSR_GRPC_HPMCOUNTER11    = 8'h0B,
    RRV64_CSR_GRPC_HPMCOUNTER12    = 8'h0C,
    RRV64_CSR_GRPC_HPMCOUNTER13    = 8'h0D,
    RRV64_CSR_GRPC_HPMCOUNTER14    = 8'h0E,
    RRV64_CSR_GRPC_HPMCOUNTER15    = 8'h0F,
    RRV64_CSR_GRPC_HPMCOUNTER16    = 8'h10,
    RRV64_CSR_GRPC_HPMCOUNTER17    = 8'h11,
    RRV64_CSR_GRPC_HPMCOUNTER18    = 8'h12,
    RRV64_CSR_GRPC_HPMCOUNTER19    = 8'h13,
    RRV64_CSR_GRPC_HPMCOUNTER20    = 8'h14,
    RRV64_CSR_GRPC_HPMCOUNTER21    = 8'h15,
    RRV64_CSR_GRPC_HPMCOUNTER22    = 8'h16,
    RRV64_CSR_GRPC_HPMCOUNTER23    = 8'h17,
    RRV64_CSR_GRPC_HPMCOUNTER24    = 8'h18,
    RRV64_CSR_GRPC_HPMCOUNTER25    = 8'h19,
    RRV64_CSR_GRPC_HPMCOUNTER26    = 8'h1A,
    RRV64_CSR_GRPC_HPMCOUNTER27    = 8'h1B,
    RRV64_CSR_GRPC_HPMCOUNTER28    = 8'h1C,
    RRV64_CSR_GRPC_HPMCOUNTER29    = 8'h1D,
    RRV64_CSR_GRPC_HPMCOUNTER30    = 8'h1E,
    RRV64_CSR_GRPC_HPMCOUNTER31    = 8'h1F // }}}
  } rrv64_csr_grpc_t;
  typedef enum logic [RRV64_CSR_ADDR_CUT_WIDTH-1:0] {
    RRV64_CSR_GRPF_MVENDORID       = 8'h11,
    RRV64_CSR_GRPF_MARCHID         = 8'h12,
    RRV64_CSR_GRPF_MIMPID          = 8'h13,
    RRV64_CSR_GRPF_MHARTID         = 8'h14
  } rrv64_csr_grpf_t;
  typedef enum logic [1:0] { // {{{
    RRV64_PRV_U = 2'b00,
    RRV64_PRV_S = 2'b01,
    RRV64_PRV_M = 2'b11 // }}}
  } rrv64_prv_t;
  typedef enum logic [1:0] { // {{{
    RRV64_CSR_OP_RW = 2'b01,
    RRV64_CSR_OP_RS = 2'b10,
    RRV64_CSR_OP_RC = 2'b11,
    RRV64_CSR_OP_NONE = 2'b00 // }}}
  } rrv64_csr_op_t;
  typedef struct packed { // {{{
    logic Z;
    logic Y;
    logic X;
    logic W;
    logic V;
    logic U;
    logic T;
    logic S;
    logic R;
    logic Q;
    logic P;
    logic O;
    logic N;
    logic M;
    logic L;
    logic K;
    logic J;
    logic I;
    logic H;
    logic G;
    logic F;
    logic E;
    logic D;
    logic C;
    logic B;
    logic A;// }}}
  } rrv64_misa_ext_t;
  typedef struct packed { // {{{
    logic [ 1:0]  MXL; // [63:62]
    // logic [35:0]  reserved_61_to_26;
    rrv64_misa_ext_t  EXTENSIONS; // [25:0] }}}
  } rrv64_csr_misa_t;
  typedef struct packed { // {{{
    logic         SD;
    logic         BYPASS_IC;
    logic [25:0]  reserved_61_to_36;
    logic [ 1:0]  SXL;
    logic [ 1:0]  UXL;
    logic [ 8:0]  reserved_31_to_23;
    logic         TSR;
    logic         TW;
    logic         TVM;
    logic         MXR;
    logic         SUM;
    logic         MPRV;
    logic [ 1:0]  XS;
    logic [ 1:0]  FS;
    rrv64_prv_t   MPP;
    logic [ 1:0]  reserved_10_to_9;
    logic         SPP;
    logic         MPIE;
    logic         reserved_6;
    logic         SPIE;
    logic         UPIE;
    logic         MIE;
    logic         reserved_2;
    logic         SIE;
    logic         UIE; // }}}
  } rrv64_csr_mstatus_t;
  typedef enum logic [1:0] { // {{{
    RRV64_CSR_TVEC_DIRECT = 2'b00,
    RRV64_CSR_TVEC_VECTORED = 2'b01,
    RRV64_CSR_TVEC_RSVD0 = 2'b10,
    RRV64_CSR_TVEC_RSVD1 = 2'b11 // }}}
  } rrv64_csr_tvec_mode_t;
  typedef struct packed { // {{{
    logic [61:0]  base;
    rrv64_csr_tvec_mode_t mode; // }}}
  } rrv64_csr_tvec_t;
  typedef enum logic [63:0] { // {{{
    RRV64_TRAP_CAUSE_U_SW_INT = {1'b1, 63'('d0)},
    RRV64_TRAP_CAUSE_S_SW_INT = {1'b1, 63'('d1)},
    RRV64_TRAP_CAUSE_M_SW_INT = {1'b1, 63'('d3)},
    RRV64_TRAP_CAUSE_U_TIME_INT = {1'b1, 63'('d4)},
    RRV64_TRAP_CAUSE_S_TIME_INT = {1'b1, 63'('d5)},
    RRV64_TRAP_CAUSE_M_TIME_INT = {1'b1, 63'('d7)},
    RRV64_TRAP_CAUSE_U_EXT_INT = {1'b1, 63'('d8)},
    RRV64_TRAP_CAUSE_S_EXT_INT = {1'b1, 63'('d9)},
    RRV64_TRAP_CAUSE_M_EXT_INT = {1'b1, 63'('d11)},
    RRV64_TRAP_CAUSE_INST_ADDR_MISALIGNED = {1'b0, 63'('d0)},
    RRV64_TRAP_CAUSE_INST_ACCESS_FAULT = {1'b0, 63'('d1)},
    RRV64_TRAP_CAUSE_ILLEGAL_INST = {1'b0, 63'('d2)},
    RRV64_TRAP_CAUSE_BREAKPOINT = {1'b0, 63'('d3)},
    RRV64_TRAP_CAUSE_LOAD_ADDR_MISALIGNED = {1'b0, 63'('d4)},
    RRV64_TRAP_CAUSE_LOAD_ACCESS_FAULT = {1'b0, 63'('d5)},
    RRV64_TRAP_CAUSE_STORE_ADDR_MISALIGNED = {1'b0, 63'('d6)},
    RRV64_TRAP_CAUSE_STORE_ACCESS_FAULT = {1'b0, 63'('d7)},
    RRV64_TRAP_CAUSE_ECALL_FROM_U = {1'b0, 63'('d8)},
    RRV64_TRAP_CAUSE_ECALL_FROM_S = {1'b0, 63'('d9)},
    RRV64_TRAP_CAUSE_ECALL_FROM_M = {1'b0, 63'('d11)},
    RRV64_TRAP_CAUSE_INST_PAGE_FAULT = {1'b0, 63'('d12)},
    RRV64_TRAP_CAUSE_LOAD_PAGE_FAULT = {1'b0, 63'('d13)},
    RRV64_TRAP_CAUSE_STORE_PAGE_FAULT = {1'b0, 63'('d15)}
    // }}}
  } rrv64_csr_trap_cause_t;
  typedef enum logic [3:0] {
    RRV64_INT_U_SW    = 'd0,
    RRV64_INT_S_SW    = 'd1,
    RRV64_INT_M_SW    = 'd3,
    RRV64_INT_U_TIME  = 'd4,
    RRV64_INT_S_TIME  = 'd5,
    RRV64_INT_M_TIME  = 'd7,
    RRV64_INT_U_EXT   = 'd8,
    RRV64_INT_S_EXT   = 'd9,
    RRV64_INT_M_EXT   = 'd11
  } rrv64_int_cause_t;
  typedef struct packed { // {{{
    logic                   valid;
    rrv64_csr_trap_cause_t  cause; // }}}
  } rrv64_trap_t;
  typedef struct packed {
    logic en_hpmcounter;
  } rrv64_csr_cfg_t;
  // next virtual PC
  typedef struct packed { // {{{
    logic         valid;
    rrv64_vaddr_t       pc; // }}}
  } rrv64_npc_t;
  typedef struct packed { // {{{
    logic         HPM31;
    logic         HPM30;
    logic         HPM29;
    logic         HPM28;
    logic         HPM27;
    logic         HPM26;
    logic         HPM25;
    logic         HPM24;
    logic         HPM23;
    logic         HPM22;
    logic         HPM21;
    logic         HPM20;
    logic         HPM19;
    logic         HPM18;
    logic         HPM17;
    logic         HPM16;
    logic         HPM15;
    logic         HPM14;
    logic         HPM13;
    logic         HPM12;
    logic         HPM11;
    logic         HPM10;
    logic         HPM9;
    logic         HPM8;
    logic         HPM7;
    logic         HPM6;
    logic         HPM5;
    logic         HPM4;
    logic         HPM3;
    logic         IR;
    logic         TM;
    logic         CY; // }}}
  } rrv64_csr_counteren_t;
  typedef struct packed { // {{{
    logic         HPM31;
    logic         HPM30;
    logic         HPM29;
    logic         HPM28;
    logic         HPM27;
    logic         HPM26;
    logic         HPM25;
    logic         HPM24;
    logic         HPM23;
    logic         HPM22;
    logic         HPM21;
    logic         HPM20;
    logic         HPM19;
    logic         HPM18;
    logic         HPM17;
    logic         HPM16;
    logic         HPM15;
    logic         HPM14;
    logic         HPM13;
    logic         HPM12;
    logic         HPM11;
    logic         HPM10;
    logic         HPM9;
    logic         HPM8;
    logic         HPM7;
    logic         HPM6;
    logic         HPM5;
    logic         HPM4;
    logic         HPM3;
    logic         IR;
    logic         O;
    logic         CY; // }}}
  } rrv64_csr_counter_inhibit_t;
  typedef struct packed { // {{{
    logic [51:0]  reserved_63_to_12;
    logic         MEIP;
    logic         reserved_10;
    logic         SEIP;
    logic         UEIP;
    logic         MTIP;
    logic         reserved_6;
    logic         STIP;
    logic         UTIP;
    logic         MSIP;
    logic         reserved_2;
    logic         SSIP;
    logic         USIP; // }}}
  } rrv64_csr_ip_t;
  typedef struct packed { // {{{
    logic [51:0]  reserved_63_to_12;
    logic         MEIE;
    logic         reserved_10;
    logic         SEIE;
    logic         UEIE;
    logic         MTIE;
    logic         reserved_6;
    logic         STIE;
    logic         UTIE;
    logic         MSIE;
    logic         reserved_2;
    logic         SSIE;
    logic         USIE; // }}}
  } rrv64_csr_ie_t;
  typedef struct packed { // {{{
    logic [47:0] reserved_63_to_16;
    logic        store_page_fault;
    logic        reserved_14;
    logic        load_page_fault;
    logic        inst_page_fault;
    logic        ecall_from_m;
    logic        reserved_10;
    logic        ecall_from_s;
    logic        ecall_from_u;
    logic        store_access_fault;
    logic        store_addr_misaligned;
    logic        load_access_fault;
    logic        load_addr_misaligned;
    logic        breakpoint;
    logic        illegal_inst;
    logic        inst_access_fault;
    logic        inst_addr_misaligned; // }}}
  } rrv64_csr_edeleg_t;
  typedef struct packed {
    logic [51:0]  reserved_63_to_12;
    logic         meip;
    logic         reserved_10;
    logic         seip;
    logic         ueip;
    logic         mtip;
    logic         reserved_6;
    logic         stip;
    logic         utip;
    logic         msip;
    logic         reserved_2;
    logic         ssip;
    logic         usip;
  } rrv64_csr_ideleg_t;
  // bypass
  typedef struct packed { // {{{
    logic             valid_data;
    logic       is_rd_fp;
    rrv64_reg_addr_t  rd_addr;
    logic             valid_addr;
    rrv64_data_t      rd; // }}}
  } rrv64_bps_t;

  // Supervisor csrs
  typedef struct packed { // {{{
    logic           SD;
    logic [28:0]    reserved_62_to_34;
    logic [1:0]     UXL;
    logic [11:0]    reserved_31_to_20;
    logic           MXR;
    logic           SUM;
    logic           reserved_17;
    logic [1:0]     XS;
    logic [1:0]     FS;
    logic [3:0]     reserved_12_to_9;
    logic           SPP;
    logic [1:0]     reserved_7_to_6;
    logic           SPIE;
    logic           UPIE;
    logic [1:0]     reserved_3_to_2;
    logic           SIE;
    logic           UIE; // }}}
  } rrv64_csr_sstatus_t;

  // User csrs
  typedef struct packed { // {{{
    logic [58:0]    reserved_63_to_5;
    logic           UPIE;
    logic [2:0]     reserved_3_to_1;
    logic           UIE; // }}}
  } rrv64_csr_ustatus_t;

  //------------------------------------------------------
  // floating point
  typedef enum logic [2:0] { // {{{
    RRV64_FRM_RNE   = 3'b000,
    RRV64_FRM_RTZ   = 3'b001,
    RRV64_FRM_RDN   = 3'b010,
    RRV64_FRM_RUP   = 3'b011,
    RRV64_FRM_RMM   = 3'b100,
    RRV64_FRM_RSVD0 = 3'b101,
    RRV64_FRM_RSVD1 = 3'b110,
    RRV64_FRM_DYN   = 3'b111 // }}}
  } rrv64_frm_t; // rounding mode for floating-point
  typedef enum logic [2:0] { // {{{
    RRV64_FRM_DW_RNE   = 3'b000,
    RRV64_FRM_DW_RTZ   = 3'b001,
    RRV64_FRM_DW_RDN   = 3'b011,
    RRV64_FRM_DW_RUP   = 3'b010,
    RRV64_FRM_DW_RMM   = 3'b100,
    RRV64_FRM_DW_RSVD0 = 3'b101,
    RRV64_FRM_DW_RSVD1 = 3'b110,
    RRV64_FRM_DW_DYN   = 3'b111 // }}}
  } rrv64_frm_dw_t; // rounding mode for floating-point
  typedef struct packed { // {{{
    logic nv;
    logic dz;
    logic of;
    logic uf;
    logic nx; // }}}
  } rrv64_fflags_t; // floating-point flags
  typedef struct packed { // {{{
    logic compspecific;
    logic hugeint;
    logic inexact;
    logic huge;
    logic tiny;
    logic invalid;
    logic infinity;
    logic zero; // }}}
  } rrv64_fstatus_dw_t; // status output from DW
  typedef enum logic [3:0] { // {{{
    RRV64_FP_OP_ADD_S,
    RRV64_FP_OP_MAC_S,
    RRV64_FP_OP_DIV_S,
    RRV64_FP_OP_SQRT_S,
    RRV64_FP_OP_ADD_D,
    RRV64_FP_OP_MAC_D,
    RRV64_FP_OP_DIV_D,
    RRV64_FP_OP_SQRT_D,
    RRV64_FP_OP_MISC,
    RRV64_FP_OP_NONE // }}}
  } rrv64_fp_op_t;
  typedef enum logic { // {{{
    RRV64_FP_RS1_SEL_RS1,
    RRV64_FP_RS1_SEL_RS1_INV // }}}
  } rrv64_fp_rs1_sel_t;
  typedef enum logic { // {{{
    RRV64_FP_RS2_SEL_RS2,
    RRV64_FP_RS2_SEL_RS2_INV // }}}
  } rrv64_fp_rs2_sel_t;
  typedef enum logic [1:0] { // {{{
    RRV64_FP_RS3_SEL_ZERO,
    RRV64_FP_RS3_SEL_RS3,
    RRV64_FP_RS3_SEL_RS3_INV // }}}
  } rrv64_fp_rs3_sel_t;
  typedef enum logic [1:0] { // {{{
    RRV64_FP_SGNJ_SEL_J,
    RRV64_FP_SGNJ_SEL_JN,
    RRV64_FP_SGNJ_SEL_JX // }}}
  } rrv64_fp_sgnj_sel_t;
  typedef enum logic [2:0] { // {{{
    RRV64_FP_CMP_SEL_MAX,
    RRV64_FP_CMP_SEL_MIN,
    RRV64_FP_CMP_SEL_EQ,
    RRV64_FP_CMP_SEL_LT,
    RRV64_FP_CMP_SEL_LE,
    RRV64_FP_CMP_SEL_NONE // }}}
  } rrv64_fp_cmp_sel_t;
  typedef enum logic [1:0] { // {{{
    RRV64_FP_CVT_SEL_W,
    RRV64_FP_CVT_SEL_WU,
    RRV64_FP_CVT_SEL_L,
    RRV64_FP_CVT_SEL_LU // }}}
  } rrv64_fp_cvt_sel_t;
  typedef struct packed { // {{{
    logic q_nan;      // quiet NaN
    logic s_nan;      // signaling NaN
    logic pos_inf;    // +infinity
    logic pos;        // +normal
    logic pos_sub;    // +subnormal
    logic pos_zero;   // +0
    logic neg_zero;   // -0
    logic neg_sub;    // -subnormal
    logic neg;        // -normal
    logic neg_inf;    // -infinity }}}
  } rrv64_fp_cls_t;
  typedef enum logic [4:0] { // {{{
    RRV64_FP_OUT_SEL_ADD_S,
    RRV64_FP_OUT_SEL_CMP_S,
    RRV64_FP_OUT_SEL_MAC_S,
    RRV64_FP_OUT_SEL_DIV_S,
    RRV64_FP_OUT_SEL_SQRT_S,
    RRV64_FP_OUT_SEL_ADD_D,
    RRV64_FP_OUT_SEL_CMP_D,
    RRV64_FP_OUT_SEL_MAC_D,
    RRV64_FP_OUT_SEL_DIV_D,
    RRV64_FP_OUT_SEL_SQRT_D,
    RRV64_FP_OUT_SEL_SGNJ,
    RRV64_FP_OUT_SEL_CVT_I2S,
    RRV64_FP_OUT_SEL_CVT_I2D,
    RRV64_FP_OUT_SEL_CVT_S2I,
    RRV64_FP_OUT_SEL_CVT_D2I,
    RRV64_FP_OUT_SEL_CVT_S2D,
    RRV64_FP_OUT_SEL_CVT_D2S,
    RRV64_FP_OUT_SEL_CLS,
    RRV64_FP_OUT_SEL_NONE // }}}
  } rrv64_fp_out_sel_t;

  //------------------------------------------------------
  // exception
  typedef enum logic [3:0] { // {{{
    RRV64_EXCP_CAUSE_INST_ADDR_MISALIGNED = 4'('d0),
    RRV64_EXCP_CAUSE_INST_ACCESS_FAULT = 4'('d1),
    RRV64_EXCP_CAUSE_ILLEGAL_INST = 4'('d2),
    RRV64_EXCP_CAUSE_BREAKPOINT = 4'('d3),
    RRV64_EXCP_CAUSE_LOAD_ADDR_MISALIGNED = 4'('d4),
    RRV64_EXCP_CAUSE_LOAD_ACCESS_FAULT = 4'('d5),
    RRV64_EXCP_CAUSE_STORE_ADDR_MISALIGNED = 4'('d6),
    RRV64_EXCP_CAUSE_STORE_ACCESS_FAULT = 4'('d7),
    RRV64_EXCP_CAUSE_ECALL_FROM_U = 4'('d8),
    RRV64_EXCP_CAUSE_ECALL_FROM_S = 4'('d9),
    RRV64_EXCP_CAUSE_ECALL_FROM_M = 4'('d11),
    RRV64_EXCP_CAUSE_INST_PAGE_FAULT = 4'('d12),
    RRV64_EXCP_CAUSE_LOAD_PAGE_FAULT = 4'('d13),
    RRV64_EXCP_CAUSE_STORE_PAGE_FAULT = 4'('d15),
    RRV64_EXCP_CAUSE_NONE = 4'('d10)
    // }}}
  } rrv64_excp_cause_t;

  typedef struct packed { // {{{
    logic         valid;
    rrv64_inst_t  inst;
    rrv64_excp_cause_t  cause; // }}}
    logic         is_half1;
  } rrv64_excp_t;

  typedef enum logic [1:0] { // {{{
    RRV64_FENCE_TYPE_FENCE,
    RRV64_FENCE_TYPE_FENCE_I,
    RRV64_FENCE_TYPE_SFENCE,
    RRV64_FENCE_TYPE_NONE // }}}
  } rrv64_fence_type_t;

  //==========================================================
  // RVC
  typedef enum logic [1:0] {
    RVC_C0  = 2'b00,
    RVC_C1  = 2'b01,
    RVC_C2  = 2'b10
  } rrv64_rvc_opcode_t;

  //==========================================================
  // pipeline registers
  //------------------------------------------------------
  // data
  typedef struct packed {
    rrv64_vaddr_t       pred_target; 
    logic               pred_taken; 
    logic [1:0]         pred_bimodal;
  } rrv64_pred_info_t;

  typedef struct packed {
      logic         valid;
      rrv64_vaddr_t branch_addr;
      logic         branch_taken;
      logic [1:0]   branch_bimodal;
      rrv64_vaddr_t branch_target;
      logic         branch_indirect;
  } rrv64_pred_update_t;

  typedef struct packed { // {{{
    rrv64_vaddr_t       pc;
    rrv64_inst_t        inst; 
    rrv64_pred_info_t   bpred;
    logic               is_rvc; // }}}
  } rrv64_if2id_t;

  typedef struct packed { // (by Anh)
    logic [2:0]                 thread_id;
    logic [31:0]                instr;
  } rrv64_if2vid_t;

  typedef struct packed { // {{{
    rrv64_vaddr_t       pc;
    rrv64_inst_t        inst;
    rrv64_data_t        rs1, rs2;
    rrv64_reg_addr_t    rd_addr;
    rrv64_pred_info_t   bpred;
    logic               is_rs1_final;
    logic               is_rs2_final;
    logic               is_rvc; // }}}
  } rrv64_id2ex_t;
  typedef struct packed { // {{{
    logic         en;
    rrv64_data_t        rs1, rs2;
    logic         is_same; // }}}
  } rrv64_id2muldiv_t;
  typedef struct packed { // {{{
    rrv64_vaddr_t     pc;
    rrv64_inst_t        inst;
    // rrv64_data_t      rs1, rs2;
    rrv64_data_t      ex_out;
    rrv64_reg_addr_t  rd_addr;
    rrv64_csr_addr_t  csr_addr;
    rrv64_fflags_t    fflags;
    logic             is_rvc;
    // }}}
  } rrv64_ex2ma_t;
  typedef struct packed { // {{{
    rrv64_vaddr_t     pc;
    rrv64_inst_t      inst;
    rrv64_vaddr_t     mem_addr;
    rrv64_csr_addr_t  csr_addr;
    rrv64_reg_addr_t  rs1_addr;
    rrv64_data_t      csr_wdata;
    rrv64_fflags_t    fflags;
    logic             is_rvc;
    // }}}
  } rrv64_ma2cs_t;
  typedef struct packed { // {{{
    rrv64_data_t      csr; // }}}
  } rrv64_cs2ma_t;
  typedef struct packed { // {{{
    rrv64_data_t rd;
    rrv64_reg_addr_t rd_addr; // }}}
    logic is_rvc;
    logic is_csr_op_on_mmu_frm;
  } rrv64_ma2wb_t;
  //------------------------------------------------------
  // ctrl
  typedef struct packed { // {{{
    logic         is_rs1_fp, is_rs2_fp,
                  rs1_re, rs2_re, rs3_re,
                  is_rs1_x0, is_rs2_x0; // }}}
  } rrv64_if2id_ctrl_t;
  typedef struct packed { // {{{
    // copy from IF
    logic         is_rs1_fp, is_rs2_fp;
    // new in ID
    rrv64_alu_op_t      alu_op;
    logic         alu_en;
    logic         is_32;
    rrv64_op1_sel_t     op1_sel;
    rrv64_op2_sel_t     op2_sel;
    rrv64_imm_sel_t     imm_sel;
    rrv64_rd_avail_t    rd_avail;
    logic         rd_we;
    logic         is_rd_fp;
    rrv64_ex_pc_sel_t   ex_pc_sel;
    rrv64_ex_out_sel_t  ex_out_sel;
    // new in ID for MULDIV
    rrv64_mul_type_t    mul_type;
    rrv64_div_type_t    div_type;
    // new in ID for FP
    rrv64_fp_op_t       fp_op;
    rrv64_fp_rs1_sel_t  fp_rs1_sel;
    rrv64_fp_rs2_sel_t  fp_rs2_sel;
    rrv64_fp_rs3_sel_t  fp_rs3_sel;
    rrv64_fp_sgnj_sel_t fp_sgnj_sel;
    rrv64_fp_cmp_sel_t  fp_cmp_sel;
    rrv64_fp_cvt_sel_t  fp_cvt_sel;
    rrv64_fp_out_sel_t  fp_out_sel;
    // atomic
    logic is_aq_rl;
    logic is_amo;
    logic is_amo_mv;
    logic is_amo_load;
    logic is_amo_op;
    logic is_amo_store;
    logic is_amo_done;
    logic is_lr;
    logic is_sc;
    logic is_fp_32;
    `ifndef SYNTHESIS
    logic [63:0]  inst_code;
    `endif // }}}
  } rrv64_id2ex_ctrl_t;
  typedef struct packed { // {{{
    // copy from ID
    logic         is_rd_fp,
                  rd_we;
    rrv64_rd_avail_t    rd_avail;
    // new in EX to MA
    logic         is_load, is_store;
    rrv64_fence_type_t  fence_type;
    rrv64_rd_sel_t      rd_sel;
    rrv64_ma_byte_sel_t ma_byte_sel;
    // new in EX to CS
    rrv64_csr_op_t      csr_op;
    rrv64_ret_type_t    ret_type;
    logic         is_wfi;
    logic         is_wfe;
    logic is_amo_load;
    logic is_amo_op;
    logic is_amo_store;
    logic is_lr;
    logic is_sc;
    logic is_amo;
    logic is_amo_done;
    logic is_aq_rl;
    logic is_fp_32;
    `ifndef SYNTHESIS
    logic [63:0]  inst_code;
    `endif // }}}
  } rrv64_ex2ma_ctrl_t;
  typedef struct packed { // {{{
    rrv64_csr_op_t    csr_op;
    rrv64_ret_type_t  ret_type;
    logic       is_wfi; // }}}
  } rrv64_ma2cs_ctrl_t;
  typedef struct packed { // {{{
    logic rd_we, is_rd_fp; // }}}
  } rrv64_ma2wb_ctrl_t;

  //------------------------------------------------------
  // AMO
  typedef enum logic [3:0] {
    AMO_IDLE	 = 4'b0000,
    AMO_LOAD	 = 4'b0001,
    AMO_OP       = 4'b0010,
    AMO_STORE    = 4'b0011,
    AMO_MEM_BAR  = 4'b0100,
    AMO_DONE     = 4'b0101,
    AMO_MV       = 4'b0110,
    AMO_LRSC     = 4'b0111,
    AMO_GAP      = 4'b1000
  } amo_fsm_t;

  //------------------------------------------------------
  // regfile
  typedef struct packed { // {{{
    rrv64_reg_addr_t  rs1_addr, rs2_addr;
    logic       rs1_re, rs2_re; // }}}
  } rrv64_if2irf_t;
  typedef struct packed { // {{{
    rrv64_reg_addr_t  rs1_addr, rs2_addr, rs3_addr;
    logic       rs1_re, rs2_re, rs3_re; // }}}
  } rrv64_if2fprf_t;
  typedef struct packed { // {{{
    rrv64_data_t  rs1, rs2; // }}}
  } rrv64_irf2id_t;
  typedef struct packed { // {{{
    rrv64_data_t  rs1, rs2, rs3; // }}}
  } rrv64_fprf2id_t;
  typedef struct packed { // {{{
    rrv64_data_t      rd;
    rrv64_reg_addr_t  rd_addr;
    logic       rd_we; // }}}
  } rrv64_ma2rf_t;
  //------------------------------------------------------
  // cpunoc i/f

  typedef enum logic [CPUNOC_TID_SRCID_SIZE-1:0] {
    RRV64_IPTW_SRC_ID = CPUNOC_TID_SRCID_SIZE'(4'h0),
    RRV64_DPTW_SRC_ID = CPUNOC_TID_SRCID_SIZE'(4'h1),
    RRV64_VPTW_SRC_ID = CPUNOC_TID_SRCID_SIZE'(4'h2),
    RRV64_IC_SRC_ID   = CPUNOC_TID_SRCID_SIZE'(4'h3),
    RRV64_DC_SRC_ID   = CPUNOC_TID_SRCID_SIZE'(4'h4)
  } rrv64_src_id_t;
  
  //------------------------------------------------------
  // icache
  typedef logic [RRV64_ICACHE_TAG_WIDTH-1:0]    rrv64_ic_tag_t;
  typedef logic [RRV64_ICACHE_INDEX_WIDTH-1:0]  rrv64_ic_idx_t;
  typedef logic [RRV64_ICACHE_WYID_WIDTH-1:0]   rrv64_ic_wyid_t;
  typedef struct packed { // {{{
    rrv64_vaddr_t     pc;
    logic             en; // }}}
  } rrv64_ib2icmem_t;
  typedef struct packed { // {{{
    cache_line_t       rdata;
    rrv64_paddr_t      pc_paddr;
    logic              excp_valid;
    rrv64_excp_cause_t excp_cause;
    logic              valid; // }}}
  } rrv64_icmem2ib_t;
  typedef struct packed { // {{{
    rrv64_vaddr_t     pc;
    logic       en; // }}}
  } rrv64_if2ic_t;
  typedef struct packed { // {{{
    rrv64_paddr_t     pc;
    logic       en; // }}}
  } rrv64_if2ic_phys_t;
  typedef struct packed { // {{{
    rrv64_paddr_t     pc;
    logic       en; // }}}
  } rrv64_ic2sys_t;
  typedef struct packed { // {{{
    cache_line_t       rdata;
    logic              valid; // }}}
  } rrv64_sys2ic_t;
  typedef struct packed { // {{{
    rrv64_inst_t       inst;
    rrv64_inst_t       rvc_inst;
    logic              excp_valid;
    rrv64_excp_cause_t excp_cause;
    logic              is_half1_excp;
    logic       is_rvc;
    logic       valid; // }}}
    rrv64_vaddr_t vaddr;
  } rrv64_ic2if_t;
  typedef struct packed {
    logic                             valid;
    logic [RRV64_ICACHE_RAM_TAG_WIDTH-1:0] tag;
  } rrv64_ic_tag_entry_t;
  typedef struct packed { // {{{
    logic       line_idx;
    logic       en; // }}}
  } rrv64_da2ib_t;
  typedef struct packed { // {{{
    cache_line_t       line_data;
    rrv64_paddr_t      line_paddr;
    logic              line_excp_valid;
    rrv64_excp_cause_t line_excp_cause;
    logic [4:0]        buf_cnt; // TODO: parameterize
    logic [4:0]        rd_ptr; // }}}
  } rrv64_ib2da_t;
  //------------------------------------------------------
  // dcache
  typedef struct packed { // {{{
    logic       re, we;
    logic		lr, sc, amo_store, amo_load, aq_rl;
    rrv64_vaddr_t     addr;
    rrv64_data_byte_mask_t mask;
    rrv64_data_t      wdata;
    cpu_byte_mask_t   width; // }}}
  } rrv64_ex2dc_t;
  typedef struct packed { // {{{
    logic       re, we;
    rrv64_paddr_t     addr;
    rrv64_data_byte_mask_t mask;
    rrv64_data_t      wdata; // }}}
  } rrv64_ex2dc_phys_t;
  typedef struct packed { // {{{
    rrv64_data_t      rdata;
    logic              excp_valid;
    rrv64_excp_cause_t excp_cause;
    logic       valid; // }}}
  } rrv64_dc2ma_t;
  typedef struct packed {
    logic                                     valid;
    rrv64_data_t  [CACHE_LINE_BYTE*8/64-1:0]        data;
    logic   [CACHE_LINE_BYTE*8/64-1:0] [7:0]  dirty; // every byte has 1 dirty bit to indicate that it's been written
    logic   [RRV64_DCACHE_TAG_WIDTH+RRV64_DCACHE_INDEX_WIDTH-1:0] tag;
  } rrv64_store_buf_t;
  //------------------------------------------------------
  // magic mem
  typedef struct packed {
    logic         re, we;
    logic [ 3:0]  ra, wa;
    logic [ 7:0]  wm;
    rrv64_data_t        wd;
  } rrv64_mgc_i_t;
  typedef struct packed {
    rrv64_data_t        rd;
    logic         rmiss, wmiss;
  } rrv64_mgc_o_t;
  //------------------------------------------------------
  // floating point
  typedef struct packed { // {{{
    rrv64_data_t        rs1, rs2;
    rrv64_frm_dw_t      frm_dw; // }}}
  } rrv64_id2fp_add_t;
  typedef struct packed { // {{{
    rrv64_data_t        rs1, rs2, rs3;
    logic               is_mul;
    rrv64_frm_dw_t      frm_dw; // }}}
  } rrv64_id2fp_mac_t;
  typedef struct packed { // {{{
    rrv64_data_t        rs1, rs2;
    rrv64_frm_dw_t      frm_dw; // }}}
  } rrv64_id2fp_div_t;
  typedef struct packed { // {{{
    rrv64_data_t        rs1;
    rrv64_frm_dw_t      frm_dw; // }}}
  } rrv64_id2fp_sqrt_t;
  typedef struct packed { // {{{
    rrv64_data_t        rs1, rs2;
    rrv64_frm_dw_t      frm_dw; // }}}
  } rrv64_id2fp_misc_t;
  typedef struct packed { // {{{
    rrv64_frm_t     frm;
    rrv64_fflags_t  fflags; // }}}
  } rrv64_csr_fcsr_t;
  typedef struct packed { // {{{
    logic ovl_32;
    logic ovl_32u;
    logic ovl_64;
    logic ovl_64u; // }}}
  } rrv64_fp2i_ovl_t;
  //------------------------------------------------------
  // to DMA
  typedef struct packed { // {{{
    // logic [$clog2(RRV64_N_DMA_CHNL) - 1 : 0] dma_chnl;
    logic dma_rw;
    logic [39:0] start_addr;
    // logic [RRV64_N_DMA_SIZE_BIT - 1 : 0] dma_size; // }}}
  } rrv64_mp2dma_cmd_t;

  //------------------------------------------------------
  // Debug access

  typedef struct packed {
    logic is_l1da;
    logic is_batch;
    logic [1:0] reserved;
    logic is_tag;
    logic way_id;
    logic [RRV64_ICACHE_DATA_BANK_ID_WIDTH-1:0] data_bank_id; // 3-bit
    logic [RRV64_ICACHE_INDEX_WIDTH-1:0] index; // 7-bit
  } rrv64_l1da_addr_t;

  typedef struct packed {
    rrv64_data_t              mcycle;
    rrv64_data_t              minstret;
    rrv64_csr_mstatus_t       mstatus;
    rrv64_vaddr_t             mepc;
    rrv64_csr_trap_cause_t    mcause;
    rrv64_csr_misa_t          misa;
    rrv64_csr_ip_t            mip;
    rrv64_csr_ie_t            mie;
    rrv64_csr_edeleg_t        medeleg;
    rrv64_csr_ideleg_t        mideleg;
    rrv64_csr_tvec_t          mtvec;
    rrv64_data_t              mtval;
    rrv64_vaddr_t             satp;
    rrv64_vaddr_t             sepc;
    rrv64_csr_trap_cause_t    scause;
    rrv64_csr_edeleg_t        sedeleg;
    rrv64_csr_ideleg_t        sideleg;
    rrv64_csr_tvec_t          stvec;
    rrv64_data_t              stval;
    rrv64_vaddr_t             uepc;
    rrv64_csr_trap_cause_t    ucause;
    rrv64_csr_edeleg_t        uedeleg;
    rrv64_csr_ideleg_t        uideleg;
    rrv64_csr_tvec_t          utvec;
    rrv64_data_t              utval;
    rrv64_cntr_t    hpmcounter3,
                    hpmcounter4,
                    hpmcounter5,
                    hpmcounter6,
                    hpmcounter7,
                    hpmcounter8,
                    hpmcounter9,
                    hpmcounter10,
                    hpmcounter11,
                    hpmcounter12,
                    hpmcounter13,
                    hpmcounter14,
                    hpmcounter15,
                    hpmcounter16;
  } rrv64_cs2da_t;

  typedef struct packed {
    amo_fsm_t if_amo_st;
    logic wait_for_branch;
  } rrv64_if2da_t;
  typedef struct packed {
    logic id_wait_for_reg;
    logic wait_for_fence;
  } rrv64_id2da_t;
  typedef struct packed {
    logic wait_for_reg;
    logic wait_for_mul;
    logic wait_for_div;
    logic wait_for_fp;
    logic hold_ex2if_kill;
  } rrv64_ex2da_t;
  typedef struct packed {
    logic cs2ma_stall;
    logic wait_for_dc;
  } rrv64_ma2da_t;
  typedef struct packed {
    rrv64_reg_addr_t    reg_addr;
  } rrv64_da2rf_t;
  typedef struct packed {
    rrv64_data_t        reg_data;
  } rrv64_rf2da_t;

  //------------------------------------------------------
  // MMU

  typedef logic [RRV64_ASID_WIDTH-1:0] rrv64_asid_t;
  typedef logic [RRV64_VPN_WIDTH-1:0] rrv64_vpn_t;
  typedef logic [RRV64_PPN_WIDTH-1:0] rrv64_ppn_t;
  typedef logic [RRV64_PPN_PART_WIDTH-1:0] rrv64_ppn_part_t;
  typedef logic [RRV64_PAGE_OFFSET_WIDTH-1:0] rrv64_page_ofs_t;
  typedef logic [RRV64_PTE_IDX_WIDTH-1:0] rrv64_pte_idx_t;
  typedef logic [RRV64_PTW_LVL_CNT_WIDTH-1:0] rrv64_ptw_lvl_t;

  typedef enum logic [1:0] {
    RRV64_SFENCE_ASID       = 2'b00,
    RRV64_SFENCE_VPN        = 2'b01,
    RRV64_SFENCE_ASID_VPN   = 2'b10,
    RRV64_SFENCE_ALL        = 2'b11
  } rrv64_sfence_type_t;

  typedef enum logic [1:0] {
    RRV64_ACCESS_FETCH = 2'b00,
    RRV64_ACCESS_LOAD  = 2'b01,
    RRV64_ACCESS_STORE = 2'b10,
    RRV64_ACCESS_AMO   = 2'b11
  } rrv64_access_type_t;

  typedef struct packed {
    logic       [9:0] reserved1;
    rrv64_ppn_t       ppn;
    logic       [1:0] reserved0;
    logic             dirty;
    logic             accessed;
    logic             global_map;
    logic             user;
    logic             perm_x;
    logic             perm_w;
    logic             perm_r;
    logic             valid;
  } rrv64_pte_t; // page table entry

  typedef struct packed {
    rrv64_asid_t    asid;
    rrv64_vpn_t     vpn;
    rrv64_pte_t     pte;
    rrv64_ptw_lvl_t page_lvl;
  } rrv64_tlb_entry_t;

  typedef struct packed {
    logic       en;
    logic [2:0] idx;
  } rrv64_da2tlb_t;

  typedef struct packed {
    logic         tlb_valid;
    rrv64_vpn_t   tlb_vpn;
    rrv64_pte_t   tlb_pte;
    rrv64_asid_t  tlb_asid;
  } rrv64_tlb2da_t;

  typedef enum logic [3:0] {
    RRV64_BARE = 4'd0,
    RRV64_SV39 = 4'd8,
    RRV64_SV48 = 4'd9,
    RRV64_SV57 = 4'd10,
    RRV64_SV64 = 4'd11
  } rrv64_satp_mode_t;

  typedef struct packed { // {{{
    rrv64_satp_mode_t   mode;
    rrv64_asid_t        asid;
    rrv64_ppn_t         ppn;  // }}}
  } rrv64_csr_satp_t;

  typedef struct packed {
//    logic [9:0]     reserved;
    logic [53:0]    addr;
  } rrv64_csr_pmpaddr_t; // for RV64

  typedef enum logic [1:0] { // {{{
    RRV64_OFF   = 2'b00,
    RRV64_TOR   = 2'b01,
    RRV64_NA4   = 2'b10,
    RRV64_NAPOT = 2'b11 // }}}
  } rrv64_pmp_access_type;

  typedef struct packed {
    logic           l;
    logic [1:0]     reserved;
    rrv64_pmp_access_type a;
    logic           x;
    logic           w;
    logic           r;
  } rrv64_csr_pmpcfg_part_t;

  typedef struct packed {
    rrv64_csr_pmpcfg_part_t [RRV64_N_FIELDS_PMPCFG-1:0] pmpcfg;
  } rrv64_csr_pmpcfg_t; // for RV64

  typedef struct packed {
    logic                 valid;
    logic                 is_mp;
    logic                 is_broadcast;
    logic [3:0]           vcore_id;
    logic                 rw;
    logic                 bus_type; // 0: req; 1: resp
    logic [16-1:0]        addr;
//     rrv64_dbg_addr_t              addr;
    logic [32-1:0]        data;
  } rrv64_cpu_dbg_bus_t;

  typedef struct packed {
    logic is_l1da;      // if not l1, then is local proc registers
    logic is_local_sp; // if is not a local Scalar Proc reg, then is local Vector Proc reg
    logic is_msb;   // access the MSB part of a register in case its width is more than 32-bit
    logic       reserved;
    logic [11:0] reg_addr;  // 12-bit register addr
  } rrv64_dbg_addr_t;
  //==========================================================
  // Memory breakpoint

  typedef enum logic [1:0]  {
    RRV64_BP_OFF    = 2'b00,
    RRV64_BP_READ   = 2'b01,
    RRV64_BP_WRITE  = 2'b10,
    RRV64_BP_RW     = 2'b11
  } rrv64_bp_mem_cfg_t;

  //==========================================================
  // instruction trace buffer

  typedef logic [RRV64_ITB_ADDR_WIDTH-1:0]  rrv64_itb_addr_t;
  typedef logic [RRV64_ITB_ADDR_WIDTH:0]    rrv64_itb_size_t;
  typedef struct packed {
    rrv64_vaddr_t   vpc;
  } rrv64_itb_data_t;
  typedef enum logic [2:0] {
    RRV64_ITB_SEL_IF    = 3'h0,
    RRV64_ITB_SEL_ID    = 3'h1,
    RRV64_ITB_SEL_EX    = 3'h2,
    RRV64_ITB_SEL_MA    = 3'h3,
    RRV64_ITB_SEL_WB    = 3'h4,
    RRV64_ITB_SEL_STACK = 3'h5
  } rrv64_itb_sel_t;
  typedef struct packed {
    logic              push_to_stack;
    rrv64_itb_data_t   data;
  } rrv64_itb_packet_t;
  typedef struct packed {
    logic             en, rw;
    rrv64_itb_addr_t  addr;
    rrv64_itb_data_t  din;
  } rrv64_da2itb_t;

  typedef struct packed {
    rrv64_itb_data_t  dout;
  } rrv64_itb2da_t;

endpackage
