// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*************************************************************************
 * Multiplier wrapper 
 **************************************************************************/

module rrv64_mul
  import rrv64_typedef_pkg::*;
  import rrv64_param_pkg::*;
(
  output  rrv64_data_t      rdh, rdl,
  output  logic       complete,
  input   rrv64_data_t      rs1, rs2,
  input   rrv64_mul_type_t  mul_type,
  input   logic       start_pulse,
  input   logic       rst, clk
);
  localparam a_width = 65;
  localparam b_width = 65;
  localparam tc_mode = 1;   // signed
  // localparam num_cyc = 13;
  localparam num_cyc = RRV64_N_CYCLE_INT_MUL;
  localparam rst_mode = 1;  // sync reset
  localparam input_mode = 0;  // flop outside
  localparam output_mode = 0; // flop outside
  localparam early_start = 0; // 0 for input_mode = output_mode = 0

  logic is_rs1_signed, is_rs2_signed;
  always_comb
    case (mul_type)
      RRV64_MUL_TYPE_HUU: begin
        is_rs1_signed = 1'b0;
        is_rs2_signed = 1'b0;
      end
      RRV64_MUL_TYPE_HSU: begin
        is_rs1_signed = 1'b1;
        is_rs2_signed = 1'b0;
      end
      default: begin
        is_rs1_signed = 1'b1;
        is_rs2_signed = 1'b1;
      end
    endcase

  logic [ 64:0] a, b;
  logic [129:0] p;

  assign a = (is_rs1_signed) ? {rs1[63], rs1} : {1'b0, rs1};
  assign b = (is_rs2_signed) ? {rs2[63], rs2} : {1'b0, rs2};

  logic en;
  logic ce_uu, ce_su, ce_ss;
  logic [127:0] p_uu, p_su, p_ss;
  logic int_complete;


  logic start_pulse_ff;
  always @(posedge clk)
    if (rst)
        start_pulse_ff <= 1'b0;
    else
        start_pulse_ff <= start_pulse;
      
  always @(posedge clk)
    if (rst)
        en <= 1'b0;
    else if ((~start_pulse) && start_pulse_ff)
        en <= 1'b1;
    else if (complete)
        en <= 1'b0;
    
  always_comb begin
    ce_uu = '0; ce_su = '0; ce_ss = '0;
//    p = '0;

    case (mul_type)
      RRV64_MUL_TYPE_NONE: begin
        ce_uu = '0; ce_su = '0; ce_ss = '0;
 //       p[127:0] = p_ss;
      end
      RRV64_MUL_TYPE_HUU: begin
        ce_uu = '1; ce_su = '0; ce_ss = '0;
 //       p[127:0] = p_uu;
      end
      RRV64_MUL_TYPE_HSU: begin
        ce_uu = '0; ce_su = '1; ce_ss = '0;
 //       p[127:0] = p_su;
      end
      default: begin
        ce_uu = '0; ce_su = '0; ce_ss = '1;
//        p[127:0] = p_ss;
      end
    endcase
  end

  logic int_complete_ff;
  always_latch
      if (rst)
          int_complete_ff = 1'b0;
      else if (start_pulse)
          int_complete_ff = 1'b0;
      else if (int_complete)
          int_complete_ff = 1'b1;
          

  assign complete = int_complete_ff & ~start_pulse;

`ifndef FPGA

  multiplier_fast MUL(
		.clk(clk),
		.rst(rst),
		//control
		.mulit_en_i(en),
		.mul_finish_o(int_complete),
		//operand
		.mul_a_i(rs1), // Multiplicand<< // Dividend
		.mul_b_i(rs2), // multiplier>>   // divisor
		
    // unsigned / signed
    .is_signed_a(ce_su | ce_ss),
    .is_signed_b(ce_ss),

		// regfile interface
		.result_o(p)
	);

`else
  //==========================================================
  // Xilinx FPGA   

  mult_uu XMULT_UU( .CLK(clk), .A(rs1), .B(rs2), .CE(ce_uu), .P(p_uu) );
  mult_su XMULT_SU( .CLK(clk), .A(rs1), .B(rs2), .CE(ce_su), .P(p_su) );
  mult_ss XMULT_SS( .CLK(clk), .A(rs1), .B(rs2), .CE(ce_ss), .P(p_ss) );
`endif

  assign rdl = (mul_type == RRV64_MUL_TYPE_W) ? { {32{p[31]}}, p[31:0] } : p[63:0];
  assign rdh = p[127:64];

endmodule
