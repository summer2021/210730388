// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

`ifndef __SD_PPLN_UNIT_RSTN_V__
`define __SD_PPLN_UNIT_RSTN_V__

module sd_ppln_unit_rstn
    #(  parameter   width = 32
    )
    (
    input clk,
    input rstn,

    //--------- config
    input   cfg_is_clk_gated, 

    //--------- I/O
    input               c_srdy,
    output              c_drdy,
    input [width-1:0]   c_data,

    output logic                p_srdy,
    input                       p_drdy,
    output logic [width-1:0]    p_data
    );

    logic   rstn_d;
    logic   move_en;

    //================== BODY ========================
	assign c_drdy = (~rstn)?1'b0:(p_drdy | ~p_srdy);
	assign move_en = c_srdy & (p_drdy | ~p_srdy);
	
	always @(posedge clk ) begin
        if (~rstn)
            p_srdy <= 1'b0;
        else if (p_drdy | ~p_srdy)
            p_srdy <= move_en;

        if ((p_drdy | ~p_srdy) & move_en) begin
            p_data <= c_data;
        end
	end
endmodule
`endif  // __SD_PPLN_UNIT_RSTN_V__
