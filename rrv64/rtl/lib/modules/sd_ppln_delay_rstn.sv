// Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

`ifndef __SD_PPLN_DELAY_RSTN_V__
`define __SD_PPLN_DELAY_RSTN_V__

module sd_ppln_delay_rstn
    #(parameter width = 64,
      parameter latency = 3
    )
    (

    input   clk,
    input   rstn,
    
    input   cfg_is_clk_gated,

    input               in_srdy,
    output              in_drdy,
    input [width-1:0]   in_data,
    
    output logic                out_srdy,
    input logic                 out_drdy,
    output logic [width-1:0]    out_data
    
    );

    logic [latency:0]               tmp_srdy;
    logic [latency:0]               tmp_drdy;
    logic [latency:0] [width-1:0]   tmp_data;
    
    //================== BODY ==================
genvar ii,jj;  

generate
if (latency == 0) begin: latency_zero
    assign out_srdy = in_srdy;
    assign in_drdy = out_drdy;
    assign out_data = in_data;
end
else begin: latency_non_zero
    assign tmp_srdy[0] = in_srdy;
    assign in_drdy = tmp_drdy[0];
    assign tmp_data[0] = in_data;
    

    for (ii=0; ii<latency; ii=ii+1) begin: unit_ii
        sd_ppln_unit_rstn 
        #(.width (width)
        )
        ppln_unit_rstn_u
        (
            .clk    (clk),   
            .rstn    (rstn),
    
            .cfg_is_clk_gated   (cfg_is_clk_gated),
    
            .c_srdy     (tmp_srdy[ii]),
            .c_drdy     (tmp_drdy[ii]),
            .c_data     (tmp_data[ii]),
    
            .p_srdy    (tmp_srdy[ii+1]),
            .p_drdy    (tmp_drdy[ii+1]),
            .p_data    (tmp_data[ii+1])
        );
    end

    assign out_srdy = tmp_srdy[latency];
    assign tmp_drdy[latency] = out_drdy;
    assign out_data = tmp_data[latency];
end    
endgenerate

endmodule
`endif //__SD_PPLN_DELAY_RSTN_V__
