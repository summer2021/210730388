# Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

::legacy::set_attribute common_ui false /

set design rrv64 ;# cpu_10bit|cpu_12bit

set d_rrv64 ~/rios
set d_rtl  "$d_rrv64/$design/rtl $d_rrv64/$design/rtl/lib $d_rrv64/$design/rtl/common $d_rrv64/$design/rtl/vcore_gen2 $d_rrv64/$design/rtl/oursring $d_rrv64/$design/rtl/rrv64"
set d_jtcl $env(RTLSCORE_ROOT)/tcl_scripts

# set SDC  $d_rrv64/$design/rtl/oursring/constraints/oursring.sdc
set SDC  $d_rrv64/$design/joules_db/libraries/rrv64.sdc
# set CPF  $d_rtl/cpu.cpf

# Read Library and Create Library Domains
# set d_lib  /home/zfu/rios/rrv64/joules_db/libraries 
# set_attribute lib_search_path ". $d_lib"
# set Lib_rrv64_list "CDK_S256x12.lib and_or.lib class.lib class_fpga.lib demo.lib  lsi_10k.lib  lsi_9k.lib lsi_7k.lib  lsi_lsc15.lib  nonlinear.lib  power_sample.lib power2_sample.lib" ;
# read_libs -domain lib_rrv64 -libs $Lib_rrv64_list
# infer_memory_cells -tag_as_memory

set d_lib $env(RTLSCORE_ROOT)/tutorial/libraries
set cadence_lib /opt/cad/cadence/PDK/gsclib045_all_v4.4/gsclib045
set_attribute lib_search_path ". $d_lib/LIB $d_lib/LEF $cadence_lib/timing"

set Lib_1p00v_list         "slow_vdd1v0_basicCells.lib" ;# 1.00v libs
set Lib_1p08v_list         "pwr_mgmt.lib" ;# 1.08v libs
set Lib_1p20v_list         "fast_vdd1v2_basicCells.lib CDK_S64x10.lib" ;# 1.20v libs

read_libs -domain lib_1p00v         -libs $Lib_1p00v_list
read_libs -domain lib_1p08v         -libs $Lib_1p08v_list
read_libs -domain lib_1p20v         -libs $Lib_1p20v_list

# set Lib_1p08v_list "slow.lib pwr_mgmt.lib" ;# 1.08v libs
# set Lib_1p20v_list "typical.lib CDK_S64x10.lib" ;# 1.20v libs
# suspend 
# read_libs -domain lib_1p08v -libs $Lib_1p08v_list
# read_libs -domain lib_1p20v -libs $Lib_1p20v_list

infer_memory_cells -tag_as_memory

suspend

set_attribute init_hdl_search_path $d_rtl /


read_hdl -sv -define JOULES -define SYNTHESIS common/soc_cfg.sv common/soc_typedef.sv common/soc_func.sv rrv64/rrv_cfg.sv rrv64/rrv_typedef.sv rrv64/rrv64_param_pkg.sv rrv64/rrv64_typedef_pkg.sv rrv64/rrv64_func_pkg.sv rrv64/rrv64_decode_func_pkg.sv vcore_gen2/vcore_cfg.sv vcore_gen2/vcore_pkg.sv common/soc_intf_typedef.sv common/station_vp.sv common/def.sv \
rrv64/rrv64.sv lib/clock/icg.sv rrv64/rrv64_stall.sv rrv64/rrv64_rvc_inst_build.sv rrv64/rrv64_ptw_core.sv rrv64/rrv64_ptw.sv rrv64/rrv64_pmp_match.sv rrv64/rrv64_pmp_checker.sv rrv64/rrv64_pc_breakpoint.sv rrv64/rrv64_ring_if_arbiter.sv rrv64/rrv64_napot_addr.sv rrv64/rrv64_mul.sv rrv64/rrv64_mem_breakpoint.sv rrv64/rrv64_mem_access.sv rrv64/rrv64_inst_buffer.sv rrv64/rrv64_ideleg_checker.sv rrv64/rrv64_icache_top.sv rrv64/rrv64_icache_sysbus.sv rrv64/rrv64_icache.sv rrv64/rrv64_fetch.sv rrv64/rrv64_execute.sv rrv64/rrv64_edeleg_checker.sv rrv64/rrv64_div.sv rrv64/rrv64_decode.sv rrv64/rrv64_debug_access.sv rrv64/rrv64_dcache_bypass.sv rrv64/rrv64_csr.sv rrv64/rrv64_clk_gating.sv rrv64/rrv64_cache_noc.sv rrv64/rrv64_alu.sv rrv64/rrv64_mult_slow.sv rrv64/rrv64_regfile.sv rrv64/rrv64_int_regfile.sv rrv64/rrv64_tlb.sv rrv64/rrv64_perm_checker.sv lib/modules/ours_fifo.sv oursring/ring_station.sv lib/module/sd_ppln_delay_rstn.sv lib/modules/ours_vld_rdy_rr_arb_buf.sv lib/modules/ours_vld_rdy_rr_arb.sv lib/modules/ours_vld_rdy_buf.sv lib/modules/ours_multi_lyr_axi4_aw_w_rr_arb.sv lib/sd_ppln.sv oursring/ring_resp_ppln.sv oursring/ring_resp_arbiter.sv oursring/ring_resp.sv oursring/ring_req_ppln.sv oursring/ring_req_arbiter.sv oursring/ring_req.sv oursring/ring_junction.sv rrv64/rrv64_divi.sv lib/modules/ours_axi4_aw_w_rr_arb_buf.sv lib/module/sd_ppln_unit_rstn.sv lib/sd_hold_valid.sv 


set_attribute lp_insert_clock_gating true /

elaborate $design
# suspend

# save elab DB
write_db -all -to_file rrv64.joules.flow.elab.db

read_sdc $SDC

set_attribute lp_clock_gating_min_flops  2 /$design
set_attribute lp_clock_gating_max_flops  6 /$design ;# try 6|9|12|18

# Read the stimulus cpu_10bit_pgm_gcf.vcd 
read_stimulus -file  /home/zfu/rios/rrv64/tb/rrv64.vcd -dut_instance /testbench/top0 -dut_instance /testbench/top0 
# suspend
write_sdb -out rrv64.flow.sdb


power_map -effort medium  # low medium high
read_stimulus -file rrv64.flow.sdb 

# compute_power
compute_power
report_power
write_db -all -to_file rrv64.joules.flow.proto.db

