# Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

set DC_HOME $env(DC_HOME)
set RRV64_SRC_PATH $env(RRV64_SRC_PATH)

lappend search_path . 
lappend search_path $DC_HOME/libraries/syn 
lappend search_path $DC_HOME/dw/syn_ver
lappend search_path $RRV64_SRC_PATH/rtl/common
lappend search_path $RRV64_SRC_PATH/rtl/lib
lappend search_path $RRV64_SRC_PATH/rtl/lib/clock
lappend search_path $RRV64_SRC_PATH/rtl/lib/modules
lappend search_path $RRV64_SRC_PATH/rtl/rrv64

set synthetic_library "dw_foundation.sldb standard.sldb"
set target_library "class.db and_or.db"
set link_library "* class.db and_or.db dw_foundation.sldb"
set symbol_library "class.sdb generic.sdb"

analyze -format sverilog {soc_cfg.sv soc_typedef.sv soc_func.sv rrv_cfg.sv rrv_typedef.sv rrv64_param_pkg.sv rrv64_typedef_pkg.sv rrv64_func_pkg.sv rrv64_decode_func_pkg.sv soc_intf_typedef.sv station_vp.sv def.sv }

analyze -format sverilog {rrv64.sv icg.sv rrv64_stall.sv rrv64_rvc_inst_build.sv rrv64_ptw_core.sv rrv64_ptw.sv rrv64_pmp_match.sv rrv64_pmp_checker.sv rrv64_pc_breakpoint.sv rrv64_ring_if_arbiter.sv rrv64_napot_addr.sv rrv64_mul.sv rrv64_mem_breakpoint.sv rrv64_mem_access.sv rrv64_int_regfile.sv rrv64_inst_buffer.sv rrv64_ideleg_checker.sv rrv64_icache_top.sv rrv64_icache_sysbus.sv rrv64_icache.sv rrv64_fetch.sv rrv64_execute.sv rrv64_edeleg_checker.sv rrv64_div.sv rrv64_decode.sv rrv64_debug_access.sv rrv64_dcache_bypass.sv rrv64_csr.sv rrv64_clk_gating.sv rrv64_cache_noc.sv rrv64_alu.sv rrv64_mult_slow.sv rrv64_regfile.sv rrv64_tlb.sv rrv64_perm_checker.sv ours_fifo.sv ring_station.sv sd_ppln_delay_rstn.sv ours_vld_rdy_rr_arb_buf.sv ours_vld_rdy_rr_arb.sv ours_vld_rdy_buf.sv ours_multi_lyr_axi4_aw_w_rr_arb.sv sd_ppln.sv ring_resp_ppln.sv ring_resp_arbiter.sv ring_resp.sv ring_req_ppln.sv ring_req_arbiter.sv ring_req.sv ring_junction.sv rrv64_divi.sv ours_axi4_aw_w_rr_arb_buf.sv sd_ppln_unit_rstn.sv sd_hold_valid.sv  ring_utils.sv }

elaborate rrv64 

link

create_clock -period 200 [get_ports clk]
set dont_touch_network [get_clocks clk]
set dont_touch_network [get_ports s2b_rst]
set dont_touch_network [get_ports s2b_early_rst]


compile -map_effort medium -incremental_mapping

write  -f  ddc  -hier  -out  RRV64.ddc
