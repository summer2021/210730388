# Copyright 2021 RISC-V International Open Source Laboratory (RIOS Lab). All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

set design rrv64 ;
set d_rrv64 ~/rios
set d_rtl  "$d_rrv64/$design/rtl $d_rrv64/$design/rtl/lib $d_rrv64/$design/rtl/common $d_rrv64/$design/rtl/oursring $d_rrv64/$design/rtl/rrv64"
set_db init_hdl_search_path $d_rtl 

set SDC  $d_rrv64/$design/joules_db/libraries/rrv64.sdc

set d_lib /opt/cad/cadence/installs/JLS191/etc/rtls/tutorial/libraries
set cadence_lib /opt/cad/cadence/PDK/gsclib045_all_v4.4/gsclib045
set lib_search_path ". $d_lib/LIB $d_lib/LEF $cadence_lib/timing"
set_db init_lib_search_path ". $lib_search_path"
# set Lib_1p08v_list "slow.lib pwr_mgmt.lib" ;# 1.08v libs
# set Lib_1p20v_list "typical.lib CDK_S64x10.lib" ;# 1.20v libs
set Lib_1p00v_list         "slow_vdd1v0_basicCells.lib" ;# 1.00v libs
set Lib_1p08v_list         "pwr_mgmt.lib" ;# 1.08v libs
set Lib_1p20v_list         "fast_vdd1v2_basicCells.lib CDK_S64x10.lib" ;# 1.20v libs


set_db library {slow.lib pwr_mgmt.lib typical.lib CDK_S64x10.lib}




read_hdl -sv -define JOULES -define SYNTHESIS common/soc_cfg.sv common/soc_typedef.sv common/soc_func.sv rrv64/rrv_cfg.sv rrv64/rrv_typedef.sv rrv64/rrv64_param_pkg.sv rrv64/rrv64_typedef_pkg.sv rrv64/rrv64_func_pkg.sv rrv64/rrv64_decode_func_pkg.sv common/soc_intf_typedef.sv common/station_vp.sv common/def.sv \
rrv64/rrv64.sv lib/clock/icg.sv rrv64/rrv64_stall.sv rrv64/rrv64_rvc_inst_build.sv rrv64/rrv64_ptw_core.sv rrv64/rrv64_ptw.sv rrv64/rrv64_pmp_match.sv rrv64/rrv64_pmp_checker.sv rrv64/rrv64_pc_breakpoint.sv rrv64/rrv64_ring_if_arbiter.sv rrv64/rrv64_napot_addr.sv rrv64/rrv64_mul.sv rrv64/rrv64_mem_breakpoint.sv rrv64/rrv64_mem_access.sv rrv64/rrv64_inst_buffer.sv rrv64/rrv64_ideleg_checker.sv rrv64/rrv64_icache_top.sv rrv64/rrv64_icache_sysbus.sv rrv64/rrv64_icache.sv rrv64/rrv64_fetch.sv rrv64/rrv64_execute.sv rrv64/rrv64_edeleg_checker.sv rrv64/rrv64_div.sv rrv64/rrv64_decode.sv rrv64/rrv64_debug_access.sv rrv64/rrv64_dcache_bypass.sv rrv64/rrv64_csr.sv rrv64/rrv64_clk_gating.sv rrv64/rrv64_cache_noc.sv rrv64/rrv64_alu.sv rrv64/rrv64_mult_slow.sv rrv64/rrv64_regfile.sv rrv64/rrv64_int_regfile.sv rrv64/rrv64_tlb.sv rrv64/rrv64_perm_checker.sv lib/ours_ip/ours_fifo.sv oursring/ring_station.sv lib/module/sd_ppln_delay_rstn.sv lib/ours_ip/ours_vld_rdy_rr_arb_buf.sv lib/ours_ip/ours_vld_rdy_rr_arb.sv lib/ours_ip/ours_vld_rdy_buf.sv lib/ours_ip/ours_multi_lyr_axi4_aw_w_rr_arb.sv lib/sd_ppln.sv oursring/ring_resp_ppln.sv oursring/ring_resp_arbiter.sv oursring/ring_resp.sv oursring/ring_req_ppln.sv oursring/ring_req_arbiter.sv oursring/ring_req.sv oursring/ring_junction.sv rrv64/rrv64_divi.sv lib/ours_ip/ours_axi4_aw_w_rr_arb_buf.sv lib/module/sd_ppln_unit_rstn.sv lib/sd_hold_valid.sv 

elaborate $design

read_sdc $SDC

syn_generic              
# syn_generic -effort low  # low medium high
syn_map

report_timing > rrv64_timing.rpt
report_area > rrv64_area.rpt
write_hdl > rrv64_net.v
write_script > rrv64_script.con
write_db -to_file rrv64_net.db
