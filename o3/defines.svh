`include "../defs.sv"
`ifndef DEFINES
`define DEFINES
`define REG_WIDTH 32
`define PRF_NUM_WIDTH 6
`define ARF_NUM_WIDTH 6
`define PRF_NUM 64
`define IQ_ALU_LENGTH 8
`define ALU_OP_WIDTH 4
`define RS_IDX_WIDTH 2

`define CMPQ_SEL_DIN1    2'b00
`define CMPQ_SEL_DIN2    2'b01
`define CMPQ_SEL_UP1    2'b10
`define CMPQ_SEL_UP2    2'b11

`define UOP_WIDTH   7:0
`define ROB_SIZE    32
`define ROB_ID_W    1+`ROB_ADDR_W
`define ROB_ADDR_W  4:0
`define TRUE        1'b1
`define FALSE       1'b0

`define ALU_QUEUE_LEN 8
`define ALU_QUEUE_LEN_MINUS1 7
`define ALU_QUEUE_LEN_MINUS2 6
`define ALU_QUEUE_IDX_LEN 4

`define MDU_QUEUE_LEN 8
`define MDU_QUEUE_LEN_MINUS1 7
`define MDU_QUEUE_LEN_MINUS2 6
`define MDU_QUEUE_IDX_LEN 4

`define LSU_QUEUE_LEN 8
`define LSU_QUEUE_LEN_MINUS1 7
`define LSU_QUEUE_LEN_MINUS2 6
`define LSU_QUEUE_IDX_LEN 4

`define MDU_MUL_CYCLE 2
`define MDU_DIV_CYCLE 16

typedef logic [5:0] PRFNum; 
typedef logic [5:0] ARFNum; 

// BranchPred
`define GHLEN 20
`define BHRLEN 4
`define PHTIDXLEN_G 12 
typedef logic [`GHLEN-1:0] GlobalHist;           // Index used for indexing BHR Table
typedef logic [1:0] PHTEntry;           // One entry in PHT Table
typedef logic [`PHTIDXLEN_G-1:0] PHTIndex_G;           // PHT Index
typedef struct packed {
    PHTIndex_G pht_index_g;
} GlobalHistPred;

`define BHTIDXLEN 8
`define BHRLEN 4
`define PHTIDXLEN 10 
`define CPHT_ENTRY 1024
typedef logic [`BHTIDXLEN-1:0] BHTIndex;           // Index used for indexing BHR Table
typedef logic [`BHRLEN-1:0] BHREntry;           // One entry in BHR Table
typedef logic [`PHTIDXLEN-1:0] PHTIndex;           // PHT Index
typedef logic [9:0] CPHTIndex;
typedef logic [1:0] CPHT_Entry;
typedef struct packed {
    BHTIndex bht_index;
    PHTIndex pht_index;
} LocalHistPred;

`define BTBIDXLEN 8
typedef logic [`BTBIDXLEN-1:0] BTBIndex;

typedef struct packed {
    BHTIndex bht_index;
    PHTIndex pht_index;
    PHTIndex_G pht_index_g;
    CPHTIndex cpht_index;
    BTBIndex btb_index;
    logic use_global;
} PredInfo;

typedef logic [9:0] TAGEIndex;
typedef logic [2:0] TAGECtr;
typedef logic [7:0] TAGETag;
typedef logic [1:0] TAGEUseful;

typedef struct packed {
    logic hit;                                            // 预测命中
    TAGEIndex hit_index;                                  // 预测的index，以防历史记录改�?
    TAGEIndex on_mispred_index;                           // 预测错误时，要分配的新项的index
    logic [1:0] on_mispred_bank;                          // 预测错误时，�?要分配哪个Bank
    TAGETag hit_tag;            // 命中的Tag
    TAGETag on_mispred_tag;     // 错误预测时，更新的新项的Tag
    logic has_free_to_alloc;    // 预测错误时，有新项可以分�?
    TAGECtr ctr;                // 三位饱和计数�?
    logic [1:0] provider;                             // 
    logic has_alter;
} TAGEPred;

typedef struct packed {
    logic   [31:0]  inst;
    logic   [31:0]  pc;
    logic           isBr;
    logic           isDs;
    logic           isJ;
    logic           valid;
    NLPPredInfo     nlpInfo;
    PredInfo        bpdInfo;
    logic           predTaken;
    logic   [31:0]  predAddr;
    logic           jBadAddr;
} InstBundle;

typedef enum bit [1:0] {
    RS_ALU, RS_MDU, RS_LSU
} RS_Type;

typedef logic [63:0] PRF_Vec;
typedef logic [31:0] Word;
typedef logic [`ALU_OP_WIDTH-1:0] ALUOP;
typedef logic [`RS_IDX_WIDTH-1:0] RSNum;

typedef enum bit[1:0] { typeJ, typeBR, typeJR, typeNormal } BranchType;

typedef struct packed {
    PRFNum prf_rs1;
    PRFNum prf_rs2;
    PRFNum prf_rd_stale;
} rename_table_output;          // 重命名表的三个输出

typedef struct packed {
    ARFNum ars1, ars2, ard;         // 指令的两个源寄存器，逻辑寄存器
    logic wen;                      // 指令写寄存器
} rename_req;

typedef struct packed {
    // From the instruction
    rename_req req;
    // 从Free List来的
    PRFNum prf_rd_new;
} rename_table_input ;

typedef struct packed {
    ARFNum committed_arf;        // 被提交的ARF信息
    PRFNum committed_prf;        // 被提交的PRF信息
    PRFNum stale_prf;           // 旧的PRF，需要被释放�?
    logic wr_reg_commit;         // the instruction actually write the register
} commit_info;

typedef struct packed {
    PRFNum prf_rs1;
    PRFNum prf_rs2;
    PRFNum prf_rd_stale;
    PRFNum new_prd;
} rename_resp;

typedef struct packed {
    logic wen_0, wen_1, wen_2, wen_3;
    PRFNum wb_num0_i, wb_num1_i, wb_num2_i, wb_num3_i;  //唤醒信息
} Wake_Info;

typedef struct packed {
    logic prs1_rdy, prs2_rdy;
} Arbitration_Info;
// *_rdy = waked_up | busy list[i] = not busy

typedef struct packed {
    logic cmp_en;
    logic enq_en;
    logic enq_sel;
    logic cmp_sel;
    logic freeze;
} Queue_Ctrl_Meta;

typedef struct packed {
    PRFNum rs0;
    PRFNum rs1;
} PRFrNums;

typedef struct packed { 
    PRFNum rd;
    logic wen;
    Word wdata;
} PRFwInfo;

typedef struct packed { // TODO
    Word rs0_data;
    Word rs1_data;
} PRFrData;

typedef struct packed {
    // logic   [`UOP_WIDTH]    uOP;
    logic   [31:0]          pc;
    logic   [`ROB_ID_W]     id;
    uOP                     uOP;
    
    ALUType                 aluType;
    logic   [4:0]           cacheOP;
    RS_Type                 rs_type;

    ARFNum                  op0LAddr;   // logical
    PRFNum                  op0PAddr;   // physical
    logic                   op0re;

    ARFNum                  op1LAddr;
    PRFNum                  op1PAddr;
    logic                   op1re;

    ARFNum                  dstLAddr;
    PRFNum                  dstPAddr;
    PRFNum                  dstPStale;
    logic                   dstwe;

    logic   [31:0]          imm;

    BranchType              branchType;
    logic                   branchTaken;
    logic   [31:0]          branchAddr;

    logic                   predTaken;
    logic   [31:0]          predAddr;
    PredInfo                predInfo;
    logic   [1:0]           nlpBimState;

    logic                   causeExc;
    ExceptionType           exception;
    logic   [31:0]          BadVAddr;



    logic                   isDS;
    logic                   isPriv;
    logic   [4:0]           cp0Addr;
    logic   [31:0]          cp0Data;
    logic   [2:0]           cp0Sel;
    logic                   busy;
    logic                   valid;
    logic                   committed;  // is it committed before?
    // TODO
} UOPBundle;

interface Dispatch_ROB;
    UOPBundle   uOP0;
    UOPBundle   uOP1;
    logic       ready;
    logic       valid;
    logic       empty;
    logic [`ROB_ID_W] robID;

    modport dispatch(output uOP0, uOP1, valid, input ready, empty, robID);
    modport rob(input uOP0, uOP1, valid, output ready, empty, robID);

    task automatic sendUOP(logic [31:0] pc0, logic valid0, logic [31:0] pc1, logic valid1, ref logic clk);
        #1
        uOP0.pc     = pc0;
        uOP0.valid  = valid0;
        uOP0.busy   = `TRUE;

        uOP1.pc     = pc1;
        uOP1.valid  = valid1;
        uOP1.busy   = `TRUE;

        valid       = `TRUE;
        do @(posedge clk);
        while(!ready);
        #1
        valid       = `FALSE;
    endtask //automatic
endinterface //Dispatch_ROB

typedef struct packed {
    UOPBundle ops;
    Arbitration_Info rdys;
} ALU_Queue_Meta;

typedef struct packed {
    UOPBundle ops_hi;
    UOPBundle ops_lo;
    Arbitration_Info rdys;
    logic isMul;
} MDU_Queue_Meta;

typedef struct packed {
    UOPBundle ops;
    logic isStore;
    Arbitration_Info rdys;
} LSU_Queue_Meta;

`endif 