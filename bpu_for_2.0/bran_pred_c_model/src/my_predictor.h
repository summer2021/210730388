// my_predictor.h
// This file contains a sample my_predictor class.
// It is a simple 32,768-entry gshare with a history length of 15.
// Note that this predictor doesn't use the whole 32 kilobytes available
// for the CBP-2 contest; it is just an example.

#include <cstdint>
#include <stdint.h>
#include <vector>
#include <bitset>
#include <iostream>
#include <stdio.h>
#include <cassert>
// #define DEBUG_ON

#ifdef DEBUG_ON
#define Log(format, ...) \
    printf("[%s,%d,%s] " format "\n", \
        __FILE__, __LINE__, __func__, ## __VA_ARGS__)
#else
#define Log(format, ...) 
#endif

typedef uint64_t ADDRINT;


// ================ Helper Types ====================
//
template <size_t N, uint64_t init = (1 << N)/2 - 1>   // N < 64
class SaturatingCnt
{
    uint64_t val;
    public:
        SaturatingCnt() { reset(); }

        void increase() { if (val < (1 << N) - 1) val++; }
        void decrease() { if (val > 0) val--; }

        void reset() { val = init; }
        uint64_t getVal() { return val; }
        void setVal(uint64_t v) { val = v; }

        bool isTaken() { return (val > (1 << N)/2 - 1); }
};

// ================ Helper functions ==================
//


// This function is for ghr folding
template <uint64_t w>
    uint64_t foldBitVector(std::bitset<w> vec, int srclen, int dstwidth)
{
    uint64_t u_vecMask = ((1ULL << srclen) - 1);
    uint64_t u_retMask = ((1ULL << dstwidth) - 1);
    std::bitset<w> vecMask(u_vecMask);
    std::bitset<w> retMask(u_retMask);
    vec = vec & vecMask;
    std::bitset<w> ret = 0;
    for (int i = 0; i < srclen; i += dstwidth)
    {
        ret = ret ^ vec;
        vec >>= dstwidth;
    }
    ret = (ret ^ vec) & retMask;
    return ret.to_ulong();
}

// This function is for address folding
uint64_t foldBitVector(ADDRINT vec, int srclen, int dstwidth)
{
    uint64_t vecMask = ((1ULL << srclen) - 1);
    uint64_t retMask = ((1ULL << dstwidth) - 1);
    vec = vec & vecMask;
    uint64_t ret = 0;
    for (int i = 0; i < srclen; i += dstwidth)
    {
        ret = ret ^ vec;
        vec >>= dstwidth;
    }
    ret = (ret ^ vec) & retMask;
    return ret;
}

template<int indexLen> 
class BTB {
    private:
        std::vector<uint64_t> targetCache;
    public:
        BTB() {
            targetCache.resize(1ULL << indexLen);
        }
        uint64_t predict(uint64_t pc) {
            uint64_t idx = pc & ((1ULL << indexLen) - 1);
            return targetCache[idx];
        }
        void update(uint64_t br_pc, uint64_t target) {
            uint64_t idx = br_pc & ((1ULL << indexLen) - 1);
            targetCache[idx] = target;
        }
};

template<int indexLen> 
class BiModal {
    private:
        std::vector<SaturatingCnt<2>> biModalTable;
    public:
        BiModal() {
            biModalTable.resize(1ULL << indexLen);
        }
        bool predict(uint64_t pc) {
            uint64_t idx = pc & ((1ULL << indexLen) - 1);
            return biModalTable[idx].isTaken();
        }
        void update(uint64_t br_pc, bool taken) {
            uint64_t idx = br_pc & ((1ULL << indexLen) - 1);
            if(taken) biModalTable[idx].increase();
            else biModalTable[idx].decrease();
        }
};

class TakenPredInfo : public branch_update {
    public:
        bool pred_taken;

        bool provider_valid;
        uint8_t provider;
        uint64_t provider_index;
        bool provider_taken;

        bool altpred_valid;
        uint8_t altpred;
        uint64_t altpred_index;
        bool altpred_taken;
};

class TargetPredInfo : public branch_update {
    public:
        ADDRINT pred_target;

        bool provider_valid;
        uint8_t provider;
        uint64_t provider_index;
        ADDRINT provider_target;

        bool altpred_valid;
        uint8_t altpred;
        uint64_t altpred_index;
        ADDRINT altpred_target;
};

class my_update : public branch_update {
public:
	unsigned int index;
    TakenPredInfo takenPred;
    TargetPredInfo targetPred;
};

template <uint64_t ghrNumBits, uint64_t numIdxBits, int numBanks, int tagNumBits>
class TAGE
{
private:
    uint64_t gtimer; // Timer for grace reset
    uint64_t tageTagMask;
    uint64_t tageIdxMask;
    struct IPredEntry
    {
        IPredEntry() : tag(0) {}
        ADDRINT tag;
        SaturatingCnt<3> counter;
        SaturatingCnt<2> useful;
    };
    std::bitset<ghrNumBits> ghr; // Global history
    std::vector<std::vector<IPredEntry> > histTable;
    BiModal<numIdxBits> base;
    // BTB<numIdxBits> base;

public:

    TAGE()
    {
        histTable.resize(numBanks);

        for (int i = 0; i < numBanks; i++)
        {
            histTable[i].resize(1ULL << numIdxBits);
        }
        tageTagMask = (1ULL << tagNumBits) - 1;
        tageIdxMask = (1ULL << numIdxBits) - 1;
    }
    TakenPredInfo predict(ADDRINT addr)
    {
        Log();
		TakenPredInfo ret;
        uint64_t idxAddrFold = foldBitVector(addr, 20, 10);
        bool toFindAltPred = false;
		ret.provider_valid = true;
		ret.altpred_valid = false;
		ret.provider = numBanks; // Last number: default provider
        for (int i = numBanks - 1; i >= 0; i--)
        { // From the longest history
            // Compute the index
            uint64_t useGhrLen = bankGhrLen(i);
            uint64_t idxFoldedHistory = foldBitVector(ghr, useGhrLen, numIdxBits);
            uint64_t index = ( idxFoldedHistory ^ idxAddrFold ) & tageIdxMask;
            // Compute the tag
            uint64_t tagFoldedHistory1 = foldBitVector(ghr, useGhrLen, tagNumBits);
            uint64_t tagFoldedHistory2 = foldBitVector(ghr, useGhrLen, tagNumBits - 1);
            uint64_t tag = (addr & tageTagMask) ^ tagFoldedHistory1 ^ (tagFoldedHistory2 << 1);
            Log();
			// cout << "index: " << index << endl;
			// cout << "tag: " << tag << endl;
            // Compare the tag
            assert( index < (1ULL << numIdxBits) );
            if (histTable[i][index].tag == tag)
            {
                if (!toFindAltPred)
                {
					ret.pred_taken = histTable[i][index].counter.isTaken();
                    toFindAltPred = true;
                    // Record the providor and the providor index
					ret.provider_valid = true;
					ret.provider = i;
					ret.provider_index = index;
					ret.provider_taken = histTable[i][index].counter.isTaken();
                }
                if (toFindAltPred)
                {
                    // Record the altpred info
					ret.altpred_valid = true;
					ret.altpred = i;
					ret.altpred_index = index;
					ret.altpred_taken = histTable[i][index].counter.isTaken();
                    break;
                }
            }
            Log();
        }
        if(!toFindAltPred) { // not hit, using btb
            ret.provider = numBanks;
            ret.provider_valid = true;
            ret.pred_taken = base.predict(addr);
            ret.pred_taken = false;  // TODO
            ret.provider_taken = ret.provider_taken; 
        }
        return ret;
    }
	// Should only be called when a "control flow" occurs
	void update_ghr(bool taken) 
	{
        std::bitset<ghrNumBits> one(1);
        if(taken) ghr = (ghr << 1) | one;
        else ghr = (ghr << 1);
	}

    void update(ADDRINT br_pc, bool real_taken, TakenPredInfo pred_info)
	{
            // Update the Bimodal table (TODO)
            base.update(br_pc, real_taken);
			uint8_t provider = pred_info.provider;
			uint64_t provider_index = pred_info.provider_index;
			bool provider_taken = pred_info.provider_taken;
			//uint8_t altpred = pred_info.altpred;
			//uint64_t altpred_index = pred_info.altpred_index;
			ADDRINT altpred_taken = pred_info.altpred_taken;
			bool pred_correct = (provider_taken == real_taken);
			bool altpred_correct = (altpred_taken == real_taken);
			// Update the useful bit
            if(pred_info.altpred_valid) {
                if( pred_correct && !altpred_correct) {
                    assert(provider < numBanks && provider_index < (1ULL << numIdxBits));
                    Log("Increasing useful on %d, %d", provider, provider_index);
                    histTable[provider][provider_index].useful.increase();
                }
                if( !pred_correct && altpred_correct) {
                    assert(provider < numBanks && provider_index < (1ULL << numIdxBits));
                    Log("Decreasing useful on %d, %d", provider, provider_index);
                    histTable[provider][provider_index].useful.decrease();
                }
            }
			// Update the provider's bimodal counter
            if((int)provider < numBanks) {
                assert(provider < numBanks && provider_index < (1ULL << numIdxBits));
                if(real_taken) {
                    histTable[provider][provider_index].counter.increase();
                } else {
                    histTable[provider][provider_index].counter.decrease();
                }
            }
            Log();

            // Allocate new entry in the component
            if(!pred_correct) {
                if(provider == numBanks) { // the history length 0
                    provider = 0;
                }

                // Allocate a new entry with longer history
                // scan the useful bits
                // Can be implemented using a priority encoder
                bool find_1_allocatable = false;
                bool find_2_allocatable = false;
                int allocatable_bank_1 = 0, allocatable_bank_2 = 0;
                for(int i = provider; i < numBanks; i++) {
                    uint64_t idxAddrFold = foldBitVector(br_pc, 20, 10);
                    uint64_t useGhrLen = bankGhrLen(i);
                    uint64_t idxFoldedHistory = foldBitVector(ghr, useGhrLen, numIdxBits);
                    uint64_t index = (idxFoldedHistory ^ idxAddrFold) & tageIdxMask;
                    uint8_t useful_bits = histTable[i][index].useful.getVal();
                    // Find the 1st allocatable
                    if(useful_bits == 0 && !find_1_allocatable) {
                        find_1_allocatable = true;
                        allocatable_bank_1= i;
                    } 
                    // Find the 2nd allocatable, stop finding
                    if(useful_bits == 0 && find_1_allocatable) {
                        find_2_allocatable = true;
                        allocatable_bank_2= i;
                    }
                }

                // Avoid ping-phenonmenon
                int rnd = rand() % 300;
                Log("rnd = %d",rnd);
                int final_allocate_bank;
                // Find 2 allocatable banks
                if(find_1_allocatable && find_2_allocatable) {
                    if(rnd < 200) {
                        final_allocate_bank = allocatable_bank_1;
                    } else {
                        final_allocate_bank = allocatable_bank_2;
                    }
                } else { // Find only 1 allocatable banks
                    final_allocate_bank = allocatable_bank_1;
                }

                if(find_1_allocatable) { // At least 1 allocatable bank
                    uint64_t idxAddrFold = foldBitVector(br_pc, 20, 10);
                    uint64_t useGhrLen = bankGhrLen(final_allocate_bank);
                    uint64_t idxFoldedHistory = foldBitVector(ghr, useGhrLen, numIdxBits);
                    uint64_t tagFoldedHistory1 = foldBitVector(ghr, useGhrLen, tagNumBits);
                    uint64_t tagFoldedHistory2 = foldBitVector(ghr, useGhrLen, tagNumBits - 1);
                    uint64_t tag = (br_pc & tageTagMask) ^ tagFoldedHistory1 ^ (tagFoldedHistory2 << 1);
                    uint64_t index = (idxFoldedHistory ^ idxAddrFold) & tageIdxMask;
                    histTable[final_allocate_bank][index].tag = tag;
                    histTable[final_allocate_bank][index].useful.setVal(0);
                    histTable[final_allocate_bank][index].counter.reset();
                } else { // Not any allocatable banks found
                    // Decrease all useful bits
                    for(int i = provider; i < numBanks; i++) {
                        uint64_t idxAddrFold = foldBitVector(br_pc, 20, 10);
                        uint64_t useGhrLen = bankGhrLen(i);
                        uint64_t idxFoldedHistory = foldBitVector(ghr, useGhrLen, numIdxBits);
                        uint64_t index = (idxFoldedHistory ^ idxAddrFold) & tageIdxMask;
                        histTable[i][index].useful.decrease();
                    }
                }
            }
			update_ghr(real_taken);
	}

	// Gracefully reset the useful counter
	void tick()
	{
			gtimer++;
	}


    inline int bankGhrLen(int X)
    {
        return numIdxBits << X;
    }
};


template <uint64_t ghrNumBits, uint64_t numIdxBits, int numBanks, int tagNumBits>
class ITTAGE
{
private:
    uint64_t gtimer; // Timer for grace reset
    uint64_t tageTagMask;
    uint64_t tageIdxMask;
    struct IPredEntry
    {
        IPredEntry() : tag(0), target(0) {}
        ADDRINT tag;
        ADDRINT target;
        SaturatingCnt<2> confidence;
        bool useful;
    };
    std::bitset<ghrNumBits> ghr; // Global history
    std::vector<std::vector<IPredEntry> > targetCache;
    BTB<numIdxBits> base;

public:

    ITTAGE()
    {
        targetCache.resize(numBanks);

        for (int i = 0; i < numBanks; i++)
        {
            targetCache[i].resize(1ULL << numIdxBits);
        }
        tageTagMask = (1ULL << tagNumBits) - 1;
        tageIdxMask = (1ULL << numIdxBits) - 1;
    }
    TargetPredInfo predict(ADDRINT addr)
    {
		TargetPredInfo ret;
        uint64_t idxAddrFold = foldBitVector(addr, 20, 10);
        bool toFindAltPred = false;
		ret.provider_valid = true;
		ret.altpred_valid = false;
		ret.provider = numBanks; // Last number: default provider
        for (int i = numBanks - 1; i >= 0; i--)
        { // From the longest history
            // Compute the index
            uint64_t useGhrLen = bankGhrLen(i);
            uint64_t idxFoldedHistory = foldBitVector(ghr, useGhrLen, numIdxBits);
            uint64_t index = ( idxFoldedHistory ^ idxAddrFold ) & tageIdxMask;
            // Compute the tag
            uint64_t tagFoldedHistory1 = foldBitVector(ghr, useGhrLen, tagNumBits);
            uint64_t tagFoldedHistory2 = foldBitVector(ghr, useGhrLen, tagNumBits - 1);
            uint64_t tag = (addr & tageTagMask) ^ tagFoldedHistory1 ^ (tagFoldedHistory2 << 1);
			// cout << "index: " << index << endl;
			// cout << "tag: " << tag << endl;
            // Compare the tag
            assert( index < (1ULL << numIdxBits) );
            if (targetCache[i][index].tag == tag)
            {
                if (!toFindAltPred)
                {
					ret.pred_target = targetCache[i][index].target;
                    toFindAltPred = true;
                    // Record the providor and the providor index
					ret.provider_valid = true;
					ret.provider = i;
					ret.provider_index = index;
					ret.provider_target = targetCache[i][index].target;
                }
                if (toFindAltPred)
                {
                    // Record the altpred info
					ret.altpred_valid = true;
					ret.altpred = i;
					ret.altpred_index = index;
					ret.altpred_target = targetCache[i][index].target;
                    break;
                }
            }
        }
        if(!toFindAltPred) { // not hit, using btb
            ret.provider = numBanks;
            ret.provider_valid = true;
            ret.pred_target = base.predict(addr);
            ret.provider_target = ret.pred_target;
        }
        return ret;
    }
	// Should only be called when a "control flow" occurs
	void update_ghr(bool taken) 
	{
        std::bitset<ghrNumBits> one(1);
        if(taken) ghr = (ghr << 1) | one;
        else ghr = (ghr << 1);
	}

    void update(ADDRINT br_pc, ADDRINT real_target, TargetPredInfo pred_info)
	{
            // Update the BTB
            base.update(br_pc, real_target);
			uint8_t provider = pred_info.provider;
			uint64_t provider_index = pred_info.provider_index;
			ADDRINT provider_target = pred_info.provider_target;
			//uint8_t altpred = pred_info.altpred;
			//uint64_t altpred_index = pred_info.altpred_index;
			ADDRINT altpred_target = pred_info.altpred_target;
			bool pred_correct = (provider_target == real_target);
			bool altpred_correct = (altpred_target == real_target);
			// Update the useful bit
			if(pred_info.altpred_valid && pred_correct && !altpred_correct) {
                assert(provider < numBanks && provider_index < (1ULL << numIdxBits));
                Log("Updating useful on %d, %d", provider, provider_index);
                targetCache[provider][provider_index].useful = true;
			}
			// Update the provider's confidence counter on successful prediction
            if(pred_correct && provider < numBanks) { // Not using the default prediction
                //cout << "pred_correct" << endl;
                assert(provider < numBanks && provider_index < (1ULL << numIdxBits));
                Log("Pred correct, updating confidence on %d, %d", provider, provider_index);
                targetCache[provider][provider_index].confidence.increase();
            } 

            if(!pred_correct) {
                //cout << "pred_incorrect" << endl;
                //cout << "provider: " << int(provider) << endl;
                //cout << "provider_index: " << provider_index << endl;
                // Update the existing provider entry
                // If the confidence counter is non-null, we decrement it
                if((int)provider < numBanks) { // Tage Hit
                    assert(provider < numBanks && provider_index < (1ULL << numIdxBits));
                    unsigned int confidence_val = targetCache[provider][provider_index].confidence.getVal();
                    if(confidence_val == 0) {
                        // Replace the target with the effective target
                        targetCache[provider][provider_index].target = real_target;
                    } else {
                        targetCache[provider][provider_index].confidence.decrease();
                    }
                }

                if(provider == numBanks) {
                    provider = 0;
                }

                // Allocate a new entry
                for(int i = provider; i < numBanks; i++) {
							uint64_t idxAddrFold = foldBitVector(br_pc, 20, 10);
							uint64_t useGhrLen = bankGhrLen(i);
							uint64_t idxFoldedHistory = foldBitVector(ghr, useGhrLen, numIdxBits);
							uint64_t index = (idxFoldedHistory ^ idxAddrFold) & tageIdxMask;
							bool useful = targetCache[i][index].useful;
							if(!useful) {
									// cout << "allocating " << int(i) << ", " << index << endl;
									// Compute the tag
									uint64_t tagFoldedHistory1 = foldBitVector(ghr, useGhrLen, tagNumBits);
									uint64_t tagFoldedHistory2 = foldBitVector(ghr, useGhrLen, tagNumBits - 1);
									uint64_t tag = (br_pc & tageTagMask) ^ tagFoldedHistory1 ^ (tagFoldedHistory2 << 1);
									targetCache[i][index].target = real_target;
									targetCache[i][index].tag = tag;
									targetCache[i][index].useful = false;
									targetCache[i][index].confidence.reset();
							}
					}

			}
			// cout << "here " << endl;
			update_ghr(true);
	}

	// Gracefully reset the useful counter
	void tick()
	{
			gtimer++;
	}


    inline int bankGhrLen(int X)
    {
        return numIdxBits << X;
    }
};


class my_predictor : public branch_predictor {
public:
#define HISTORY_LENGTH	15
#define TABLE_BITS	15
	my_update u;
	branch_info bi;
	unsigned int history;
	unsigned char tab[1<<TABLE_BITS];
    ITTAGE<140, 9, 4, 8> ittage; // ITTAGE target Predictor
    TAGE<140, 12, 8, 11> tage;  // TAGE predictor
    BTB<30> btb;              // Baseline btb target predictor

	my_predictor (void) : history(0) { 
		memset (tab, 0, sizeof (tab));
	}

	branch_update *predict (branch_info & b) {
		bi = b;
		if (b.br_flags & BR_CONDITIONAL) {
            // the default GShare Predictor ( Baseline )
            u.index = (history << (TABLE_BITS - HISTORY_LENGTH)) ^ (b.address & ((1<<TABLE_BITS)-1));
			// u.direction_prediction (tab[u.index] >> 1);
            u.takenPred = tage.predict(b.address);
            u.direction_prediction(u.takenPred.provider_taken);
		} 
        if (b.br_flags & BR_INDIRECT) {
            // Set prediction target
            u.targetPred = ittage.predict(b.address);
            u.target_prediction (u.targetPred.pred_target);
            // baseline below
            // u.target_prediction (btb.predict(b.address));
        }
		return &u;
	}

	void update (branch_update *u, bool taken, unsigned int target) {
        // Log();
		if (bi.br_flags & BR_CONDITIONAL) {
            // the default GShare Predictor
			unsigned char *c = &tab[((my_update*)u)->index];
			if (taken) {
				if (*c < 3) (*c)++;
			} else {
				if (*c > 0) (*c)--;
			}
			history <<= 1;
			history |= taken;
			history &= (1<<HISTORY_LENGTH)-1;
            // Update for tage
            ittage.update_ghr(taken);
            TakenPredInfo t_update = ((my_update*)u)->takenPred;
            tage.update(bi.address, taken, t_update);
		}
        if (bi.br_flags & BR_INDIRECT) {
            TargetPredInfo i_update = ((my_update*)u)->targetPred;
            ittage.update(bi.address, target, i_update);
            btb.update(bi.address, target);
        }
	}
};
