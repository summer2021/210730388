#include <cstdint>
#include <stdint.h>
#include <bitset>
typedef uint64_t ADDRINT;
template <size_t N, uint64_t init = (1 << N)/2 - 1>   // N < 64
class SaturatingCnt
{
    uint64_t val;
    public:
        SaturatingCnt() { reset(); }

        void increase() { if (val < (1 << N) - 1) val++; }
        void decrease() { if (val > 0) val--; }

        void reset() { val = init; }
        uint64_t getVal() { return val; }

        bool isTaken() { return (val > (1 << N)/2 - 1); }
};
template <uint64_t w>
    uint64_t foldBitVector(std::bitset<w> vec, int srclen, int dstwidth)
{
    uint64_t u_vecMask = ((1ULL << srclen) - 1);
    uint64_t u_retMask = ((1ULL << dstwidth) - 1);
    std::bitset<w> vecMask(u_vecMask);
    std::bitset<w> retMask(u_retMask);
    vec = vec & vecMask;
    std::bitset<w> ret = 0;
    for (int i = 0; i < srclen; i += dstwidth)
    {
        ret = ret ^ vec;
        vec >>= dstwidth;
    }
    ret = (ret ^ vec) & retMask;
    return ret.to_ulong();
}

// This function is for address folding
uint64_t foldBitVector(ADDRINT vec, int srclen, int dstwidth)
{
    uint64_t vecMask = ((1ULL << srclen) - 1);
    uint64_t retMask = ((1ULL << dstwidth) - 1);
    vec = vec & vecMask;
    uint64_t ret = 0;
    for (int i = 0; i < srclen; i += dstwidth)
    {
        ret = ret ^ vec;
        vec >>= dstwidth;
    }
    ret = (ret ^ vec) & retMask;
    return ret;
}
